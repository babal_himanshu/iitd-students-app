package in.ac.iitd.bsw.iitdapp.Startup.SignIn;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.ac.iitd.bsw.iitdapp.FirstTimeSetup.FirstSetupActivity;
import in.ac.iitd.bsw.iitdapp.R;

public class LoginFragment extends Fragment implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener{
    @BindView(R.id.kerebros_login_userName_editText)
    EditText userNameEditText;
    @BindView(R.id.kerebros_login_password_editText)
    EditText passEditText;
    @BindView(R.id.kerebros_login_submitButton)
    Button submitButton;
    @BindView(R.id.kerebros_login_google_signIn_button)
    SignInButton googleSignInButton;

    private static final String URL_LOGIN = "https://hostelcomp.iitd.ac.in/authe.php?";
    private static final String URL_ADD_KERBEROS_USER = "https://hostelcomp.iitd.ac.in/adduser.php?";
    private static final String URL_ADD_GOOGLE_USER = "https://hostelcomp.iitd.ac.in/addgoogle.php?";

    private static final int RC_SIGN_IN = 9001;
    private static final String TAG = "SignIn";

    public static final String LOGIN_SHARED_PREF = "Login Preferences";
    public static final String IS_KERBEROS_LOGIN = "Kerberos_login";
    public static final String IS_GOOGLE_LOGIN = "Google_login";

    public static final String KERBEROS_USERNAME = "kerberos_name";
    public static final String KERBEROS_USERID = "kerberos_uid";
    public static final String KERBEROS_DEPARTMENT = "kerberos_dep";
    public static final String KERBEROS_CATEGORY = "kerberos_category";
    public static final String KERBEROS_ENTRYNO = "kerberos_entryNo";
    public static final String KERBEROS_HOSTEL = "kerberos_hostel";

    public static final String GOOGLE_NAME = "google_name";
    public static final String GOOGLE_ID = "google_uid";
    public static final String GOOGLE_PHOTO = "google_photo";

    public static final String CLUB_ID = "club_code";
    public static final String CLUB_NAME = "club_name";

    public static final String FIREBASE_TOKEN = "firebase_token";

    private RequestQueue requestQueue;
    private GoogleApiClient apiClient;
    private SharedPreferences preferences;
    private String refreshedToken;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.kerebros_login, container, false);
        ButterKnife.bind(this, view);
        submitButton.setOnClickListener(this);
        googleSignInButton.setOnClickListener(this);
        requestQueue = Volley.newRequestQueue(getContext());
        preferences = getActivity().getApplicationContext().getSharedPreferences(LOGIN_SHARED_PREF, Context.MODE_PRIVATE);

        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        apiClient = new GoogleApiClient.Builder(getContext())
                .enableAutoManage(getActivity(), this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions)
                .build();

        SharedPreferences.Editor sharedEditor = preferences.edit();
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sharedEditor.putString(FIREBASE_TOKEN, refreshedToken);
        sharedEditor.apply();

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        apiClient.stopAutoManage(getActivity());
        apiClient.disconnect();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            SharedPreferences.Editor sharedEditor = preferences.edit();

            GoogleSignInAccount acct = result.getSignInAccount();
            assert acct != null;

            Log.i(TAG, acct.getId());
            Log.i(TAG, acct.getDisplayName());
            Log.i(TAG, String.valueOf(acct.getPhotoUrl()));

            sharedEditor.putBoolean(IS_GOOGLE_LOGIN, true);
            sharedEditor.putString(GOOGLE_NAME, acct.getDisplayName());
            sharedEditor.putString(GOOGLE_ID, acct.getId());
            sharedEditor.putString(GOOGLE_PHOTO, String.valueOf(acct.getPhotoUrl()));
            sharedEditor.apply();

            addGoogleUser(acct.getDisplayName(), acct.getId());

            firstTimeSetup("google");

        } else {
            Log.i(TAG, "signIn-unsuccessful");
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(apiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.kerebros_login_submitButton) {
            authUser(userNameEditText.getText().toString(), passEditText.getText().toString());

        } else if (v.getId() == R.id.kerebros_login_google_signIn_button) {
            signIn();
        }
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(apiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.i(TAG, "signed-out");
                    }
                });
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(apiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.i(TAG, "access-revoked");
                    }
                });
    }

    private void authUser(final String userID, String password) {
        String url = URL_LOGIN + "uid=" + userID + "&pwd=" + password;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    parseJSON(response, userID);
                    Log.i(TAG, response);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });

        requestQueue.add(stringRequest);
    }

    private void parseJSON(String response, String userID) throws JSONException, UnsupportedEncodingException {
        SharedPreferences.Editor sharedEditor = preferences.edit();
        JSONObject object = new JSONObject(response);
        int success = object.getInt("success");
        Boolean isAuth = false;
        if (success > 0) {
            isAuth = true;
            sharedEditor.putBoolean(IS_KERBEROS_LOGIN, true);
        }

        if (isAuth) {
            Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();

            if (object.getString("category").equals("club")){
                sharedEditor.putString(CLUB_ID, object.getString("entry"));
                sharedEditor.putString(CLUB_NAME, object.getString("name"));
                sharedEditor.apply();

                firstTimeSetup("club");

            } else {
                sharedEditor.putString(KERBEROS_USERID, userID);
                sharedEditor.putString(KERBEROS_USERNAME, object.getString("name"));
                sharedEditor.putString(KERBEROS_DEPARTMENT, object.getString("dep"));
                sharedEditor.putString(KERBEROS_CATEGORY, object.getString("category"));
                sharedEditor.putString(KERBEROS_ENTRYNO, object.getString("entry"));
                sharedEditor.apply();

                addKerberosUser(object.getString("name"), userID, object.getString("entry"));
                firstTimeSetup("kerberos");
            }
        } else {
            Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void firstTimeSetup(String provider){
        Intent intent = new Intent(getActivity(), FirstSetupActivity.class);
        intent.putExtra("provider", provider);
        startActivity(intent);
    }

    private void addGoogleUser(String name, String google_uid){
        String url = encodeURL(URL_ADD_GOOGLE_USER + "Name=" +  name + "&Google_uid=" + google_uid + "&FireBaseToken_id=" + refreshedToken);
        Log.i(TAG, url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "Add Google User to DB : " + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
        requestQueue.add(stringRequest);
    }

    private void addKerberosUser(String name,String ker_uid, String entryNo) throws UnsupportedEncodingException {
        Log.i(TAG, name);
        Log.i(TAG, ker_uid);
        Log.i(TAG, entryNo);
        Log.i(TAG, refreshedToken + "");
        String url = encodeURL(URL_ADD_KERBEROS_USER + "Name=" + name + "&Kerebros_uid=" + ker_uid + "&Entry_No=" + entryNo + "&FireBaseToken_id=" + refreshedToken) ;
        Log.i(TAG, url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "Add IITD User to DB : " + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
        requestQueue.add(stringRequest);
    }

    private String encodeURL(String url) {
        return url.replace(" ", "%20");
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//
//        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(apiClient);
//        if (opr.isDone()) {
//            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
//            // and the GoogleSignInResult will be available instantly.
//            Log.d(TAG, "Got cached sign-in");
//            GoogleSignInResult result = opr.get();
//            handleSignInResult(result);
//        } else {
//            // If the user has not previously signed in on this device or the sign-in has expired,
//            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
//            // single sign-on will occur in this branch.
//            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
//                @Override
//                public void onResult(GoogleSignInResult googleSignInResult) {
//                    handleSignInResult(googleSignInResult);
//                }
//            });
//        }
//    }
}
