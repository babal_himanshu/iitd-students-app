package in.ac.iitd.bsw.iitdapp.Map;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import in.ac.iitd.bsw.iitdapp.Dashboard.DashBoardActivity;
import in.ac.iitd.bsw.iitdapp.Dashboard.DrawerActivity;
import in.ac.iitd.bsw.iitdapp.R;

public class Map extends DrawerActivity implements OnMapReadyCallback, GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMapClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    private final int[] TYPESOfMAPS = {GoogleMap.MAP_TYPE_SATELLITE, GoogleMap.MAP_TYPE_NORMAL,
            GoogleMap.MAP_TYPE_HYBRID, GoogleMap.MAP_TYPE_TERRAIN, GoogleMap.MAP_TYPE_NONE};
    private final int REQUEST_LOCATION = 200;
    private final int REQUEST_CHECK_SETTINGS = 300;
    private final int REQUEST_GOOGLE_PLAY_SERVICE = 400;

    private GoogleApiClient apiClient;
    private GoogleMap googleMap;
    private Circle circle;
    private LocationRequest mLocationRequest;
    private PendingResult<LocationSettingsResult> result;
    private LocationSettingsRequest.Builder builder;
    private Location mLastLocation;
    private Toolbar toolbar;

    private ArrayList<IITDplace> placeList = new ArrayList<>();
    int size = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_map);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.drawer_frame_container);
        View contentView = inflater.inflate(R.layout.activity_map, frameLayout, false);
        drawerLayout.addView(contentView, 0);

        toolbar = (Toolbar) findViewById (R.id.toolbar_map);
        toolbar.setTitle("");
        //toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar (toolbar);
        getSupportActionBar().setTitle("Map");


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

//        SupportMapFragment supportMapFragment;
//        if (Build.VERSION.SDK_INT < 21) {
//            supportMapFragment = (SupportMapFragment) this.getSupportFragmentManager().findFragmentById(R.id.map);
//        } else {
//            supportMapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
//        }
//        supportMapFragment.getMapAsync(this);



        if (apiClient == null) {
            apiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        // Academics
        placeList.add(new IITDplace("Mechanical Department", 28.545777, 77.192531, R.drawable.academics));       //1
        placeList.add(new IITDplace("Chemical Department", 28.546096, 77.193547, R.drawable.academics));         //2
        placeList.add(new IITDplace("Computer Science Department", 28.545059, 77.190507,R.drawable.academics)); //3
        placeList.add(new IITDplace("Electrical Department", 28.545495, 77.191464,R.drawable.academics));                            //4
        placeList.add(new IITDplace("Civil Department", 28.546254, 77.191335,R.drawable.academics));                                 //5
        placeList.add(new IITDplace("Mathematics Department", 28.544828, 77.192121,R.drawable.academics));                           //6
        placeList.add(new IITDplace("Textile Department", 28.544489, 77.193937,R.drawable.academics));                               //7
        placeList.add(new IITDplace("BioTechnology Department", 28.545674, 77.193342,R.drawable.academics));
        placeList.add(new IITDplace("Physics Department", 28.545095, 77.192441,R.drawable.academics));
        // Venues
        placeList.add(new IITDplace("Computer Services Center(CSC)", 28.544923, 77.192491,R.drawable.computer));
        placeList.add(new IITDplace("Library", 28.544612, 77.191359,R.drawable.library));
        placeList.add(new IITDplace("Dogra Hall", 28.545087, 77.192485,R.drawable.random));
        placeList.add(new IITDplace("Seminar Hall", 28.545168, 77.192467,R.drawable.random));
        placeList.add(new IITDplace("Exhibition Hall", 28.545151, 77.192354,R.drawable.random));
        placeList.add(new IITDplace("Central Workshop", 28.543752, 77.191957,R.drawable.random));
        placeList.add(new IITDplace("Diro's Lobby", 28.545144, 77.192418,R.drawable.random));
        placeList.add(new IITDplace("New Exhibition Hall", 28.546614, 77.191223,R.drawable.random));
        placeList.add(new IITDplace("WindT", 28.545241, 77.192263,R.drawable.random));
        placeList.add(new IITDplace("Lecture Hall Complex", 28.543007, 77.193250,R.drawable.random));
        placeList.add(new IITDplace("Biotech Lawns", 28.545222, 77.193583,R.drawable.random));
        placeList.add(new IITDplace("Synergy Building", 28.543518, 77.191131,R.drawable.random));
        placeList.add(new IITDplace("OAT", 28.545084, 77.185255,R.drawable.random));
        placeList.add(new IITDplace("Nalanda Ground", 28.546608, 77.183962,R.drawable.random));
        // Administrative
        placeList.add(new IITDplace("UG Section", 28.544826, 77.192689,R.drawable.administration));
        placeList.add(new IITDplace("State Bank of India", 28.543931, 77.193228,R.drawable.bank));
        placeList.add(new IITDplace("Hospital", 28.545838, 77.188168,R.drawable.hospital));
        placeList.add(new IITDplace("Post Office", 28.542771, 77.199229,R.drawable.postoffice));
        placeList.add(new IITDplace("SBI ATM", 28.543813, 77.192795,R.drawable.bank));
        placeList.add(new IITDplace("Canara Bank ATM", 28.544714, 77.191910,R.drawable.bank));
        placeList.add(new IITDplace("Pharmacy", 28.545773, 77.187948,R.drawable.pharmacy));
        placeList.add(new IITDplace("Indicash ATM", 28.547345, 77.183839,R.drawable.bank));
        placeList.add(new IITDplace("HDFC ATM", 28.545856, 77.198153,R.drawable.bank));
        placeList.add(new IITDplace("HDFC Bank", 28.545856, 77.198153,R.drawable.bank));
        placeList.add(new IITDplace("SBI ATM", 28.542687, 77.199005,R.drawable.bank));
        placeList.add(new IITDplace("SBI ATM", 28.545725, 77.183320,R.drawable.bank));
        placeList.add(new IITDplace("ATM", 28.547736, 77.189707,R.drawable.bank));
        // Residential
        placeList.add(new IITDplace("Kailash Hostel", 28.544371, 77.195962,R.drawable.hostel));
        placeList.add(new IITDplace("Himadri Hostel", 28.545173, 77.197299,R.drawable.hostel));
        placeList.add(new IITDplace("Udaigiri Hostel", 28.547763, 77.189241,R.drawable.hostel));
        placeList.add(new IITDplace("Girnar Hostel", 28.547724, 77.188634,R.drawable.hostel));
        placeList.add(new IITDplace("Satpura Hostel", 28.548164, 77.187818,R.drawable.hostel));
        placeList.add(new IITDplace("Zanskar Hostel", 28.546888, 77.186261,R.drawable.hostel));
        placeList.add(new IITDplace("Jwalamukhi Hostel", 28.549256, 77.184243,R.drawable.hostel));
        placeList.add(new IITDplace("Nilgiri Hostel", 28.546186, 77.182561,R.drawable.hostel));
        placeList.add(new IITDplace("Aravali Hostel", 28.548249, 77.183959,R.drawable.hostel));
        placeList.add(new IITDplace("Karakoram Hostel", 28.547202, 77.183150,R.drawable.hostel));
        placeList.add(new IITDplace("Vindhyachal Hostel", 28.548766, 77.186134,R.drawable.hostel));
        placeList.add(new IITDplace("Kumaon Hostel", 28.549296, 77.185126,R.drawable.hostel));
        placeList.add(new IITDplace("Shivalik Hostel", 28.547524, 77.185997,R.drawable.hostel));
        placeList.add(new IITDplace("Nalanda Apartments", 28.545856, 77.183428,R.drawable.apartments));
        placeList.add(new IITDplace("Vaishali Apartments", 28.545978, 77.180815,R.drawable.apartments));
        placeList.add(new IITDplace("Vikramshila Apartments", 28.543872, 77.181011,R.drawable.apartments));
        placeList.add(new IITDplace("Indraprastha Apartments", 28.546863, 77.181118,R.drawable.apartments));
        // Shops
        placeList.add(new IITDplace("SCOOPS", 28.545426, 77.191813,R.drawable.stationary));
        placeList.add(new IITDplace("Rakesh Cafeteria", 28.548223, 77.184303,R.drawable.food));
        placeList.add(new IITDplace("Southy", 28.548147, 77.184287,R.drawable.food));
        placeList.add(new IITDplace("Nescafe", 28.544621, 77.191761,R.drawable.fastfood));
        placeList.add(new IITDplace("Cafeteria", 28.543518, 77.191131,R.drawable.food));
        placeList.add(new IITDplace("Lipton", 28.544765, 77.191121,R.drawable.fastfood));
        placeList.add(new IITDplace("Amul", 28.544871, 77.191446,R.drawable.fastfood));
        placeList.add(new IITDplace("hpmc", 28.544748, 77.191710,R.drawable.fastfood));
        placeList.add(new IITDplace("Tea Halt", 28.549355, 77.185075,R.drawable.fastfood));
        placeList.add(new IITDplace("Mother Dairy", 28.549378, 77.185017,R.drawable.fastfood));
        placeList.add(new IITDplace("Gupta Store", 28.545410, 77.182697,R.drawable.departmentstore));
        placeList.add(new IITDplace("Jwalamukhi Night Mess", 28.549256, 77.184243,R.drawable.food));
        placeList.add(new IITDplace("Nilgiri Night Mess", 28.546186, 77.182561,R.drawable.food));
        placeList.add(new IITDplace("Kailash Night Mess", 28.544371, 77.195962,R.drawable.food));
        placeList.add(new IITDplace("Vindhyachal Night Mess", 28.548766, 77.186134,R.drawable.food));
        placeList.add(new IITDplace("Aravali Night Mess", 28.548249, 77.183959,R.drawable.food));
        placeList.add(new IITDplace("Shivalik Night Mess", 28.547524, 77.185997,R.drawable.food));
        placeList.add(new IITDplace("Udaigiri Night Mess", 28.547763, 77.189241,R.drawable.food));
        placeList.add(new IITDplace("Sassi", 28.550231, 77.183976,R.drawable.food));
        placeList.add(new IITDplace("Safal", 28.545517, 77.182443,R.drawable.vegetables));
        placeList.add(new IITDplace("Juice Corner", 28.546291, 77.186601,R.drawable.drink));
        placeList.add(new IITDplace("CCD", 28.545547, 77.192231,R.drawable.fastfood));
        placeList.add(new IITDplace("Neelkanth Shopping Complex", 28.545173, 77.197299,R.drawable.departmentstore));
        placeList.add(new IITDplace("Mother Dairy", 28.545615, 77.182495,R.drawable.fastfood));
        placeList.add(new IITDplace("Mother Dairy", 28.542830, 77.200159,R.drawable.fastfood));
        placeList.add(new IITDplace("Tea Halt", 28.547403, 77.183207,R.drawable.fastfood));


        // Sports
        placeList.add(new IITDplace("Swimming Pool", 28.545867, 77.186126,R.drawable.swimming));
        placeList.add(new IITDplace("Badminton Court", 28.545491, 77.185790,R.drawable.badminton));
        placeList.add(new IITDplace("Football Field", 28.544737, 77.187648,R.drawable.football));
        placeList.add(new IITDplace("Athletics Field", 28.543799, 77.189921,R.drawable.athletics));
        placeList.add(new IITDplace("Cricket Field", 28.544322, 77.188891,R.drawable.cricket));
        placeList.add(new IITDplace("Pool", 28.545713, 77.185460,R.drawable.pool));
        placeList.add(new IITDplace("Squash", 28.545707, 77.185315,R.drawable.squash));
        placeList.add(new IITDplace("Lawn Tennis", 28.547532, 77.184838,R.drawable.tennis));
        placeList.add(new IITDplace("Gym", 28.545608, 77.18572,R.drawable.gym));
        placeList.add(new IITDplace("Basketball Court", 28.544159, 77.188010,R.drawable.basketball));
        placeList.add(new IITDplace("Cricket Practice Nets", 28.543797, 77.188376,R.drawable.cricket));
        placeList.add(new IITDplace("Weightlifting", 28.545690, 77.185639,R.drawable.gym));
        placeList.add(new IITDplace("Volleyball Ground", 28.548205, 77.185067,R.drawable.volleyball));
        placeList.add(new IITDplace("Hockey Field", 28.543608, 77.190316,R.drawable.hockey));

        size = placeList.size();
    }

    @Override
    protected void onStart() {
        apiClient.connect();
        super.onStart();
    }
    @Override
    protected void onStop() {
        apiClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        int resultReturned = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getApplicationContext());
        if(resultReturned != ConnectionResult.SUCCESS){
            GoogleApiAvailability.getInstance().getErrorDialog(Map.this,
                    ConnectionResult.SERVICE_MISSING, REQUEST_GOOGLE_PLAY_SERVICE);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = createLocationRequest();
        builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        result = LocationServices.SettingsApi.checkLocationSettings(apiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                final LocationSettingsStates mState = locationSettingsResult.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        if (ActivityCompat.checkSelfPermission(Map.this,
                                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                ActivityCompat.checkSelfPermission(Map.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(Map.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
                        } else {
                            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(apiClient);
                            if (googleMap != null) {
                                LatLng locationMarker = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                                googleMap.addMarker(new MarkerOptions().position(locationMarker).title("Current Location"));
                                googleMap.moveCamera(CameraUpdateFactory.newLatLng(locationMarker));
                            }
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                            status.startResolutionForResult(Map.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                        // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        if (ActivityCompat.checkSelfPermission(Map.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Map.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(Map.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
                        } else {
                            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(apiClient);
                            if (googleMap != null) {
                                LatLng locationMarker = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
//                                int i;
//                                for(i=0;i<size;i++){
//                                    googleMap.addMarker(getMarker(placeList.get(i)));
//                                }
                                googleMap.addMarker(new MarkerOptions().position(locationMarker).title("Current Location"));
//                                googleMap.moveCamera(CameraUpdateFactory.newLatLng(locationMarker));
//                                googleMap.animateCamera(CameraUpdateFactory.zoomTo(18));
                            }
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        // user does not want to update setting. Handle it in a way that it will to affect your app functionality
                        Toast.makeText(Map.this, "User does not update location setting", Toast.LENGTH_LONG).show();
                        break;
                }
                break;
        }
    }

    protected LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapClick(LatLng latLng) {
//        MarkerOptions options = new MarkerOptions().position( latLng );
//        options.title("New Location");
//        options.icon(BitmapDescriptorFactory.defaultMarker());
//        if(googleMap != null){
//            googleMap.addMarker( options );
//        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        displayCircleOnMap(latLng);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setMapType(TYPESOfMAPS[1]);
//        int i;
//        for(i=0;i<size;i++){
//            googleMap.addMarker(getMarker(placeList.get(i)));
//        }
        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(28.545241,77.192263));
        //CameraUpdate zoom = CameraUpdateFactory.zoomTo(18);
        //googleMap.moveCamera(center);
        //googleMap.animateCamera(zoom);
        int i;
        for(i=0;i<size;i++){
            googleMap.addMarker(getMarker(placeList.get(i)));
        }
        //googleMap.addMarker(new MarkerOptions().position(locationMarker).title("Current Location"));
       // LatLngBounds latLngBounds = new LatLngBounds(new LatLng(28.538650, 77.202288), new LatLng(28.550428, 77.177152));
        googleMap.moveCamera(center);
//        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 18));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(21));
        //googleMap.animateCamera(CameraUpdateFactory);

//        CameraPosition position = new CameraPosition.Builder()
//                .target(new LatLng(28.545241,77.192263))
//                .zoom(21)
//                .tilt(30)
//                .bearing(90)
//                .build();

//        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));

        googleMap.setOnMapLongClickListener(Map.this);
        googleMap.setOnMapClickListener(Map.this);
    }

    public MarkerOptions getMarker(IITDplace place){
        return new MarkerOptions()
                .position(new LatLng(place.getLat(),place.getLng()))
                .title(place.getName())
                .icon(BitmapDescriptorFactory.fromResource(place.getDrawableID()));
    }

    private void displayCircleOnMap(LatLng mLatLng){
        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(mLatLng);
        circleOptions.radius(1000);
        circleOptions.fillColor(Color.BLUE);
        circleOptions.strokeColor(Color.RED);
        circleOptions.strokeWidth(10);
        if(googleMap != null){
            googleMap.addCircle(circleOptions);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Map.this, DashBoardActivity.class);
        startActivity(intent);
    }
}
