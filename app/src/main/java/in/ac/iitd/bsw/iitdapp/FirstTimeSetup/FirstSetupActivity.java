package in.ac.iitd.bsw.iitdapp.FirstTimeSetup;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.ac.iitd.bsw.iitdapp.Profile.EditSubscriptions;
import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.Startup.SignIn.LoginFragment;

public class FirstSetupActivity extends AppCompatActivity {
    @BindView(R.id.first_setup_next_fab)
    FloatingActionButton nextFab;
    @BindView(R.id.first_setup_prev_fab)
    FloatingActionButton prevFab;

    public static FloatingActionButton nextFAB, prevFAB;

    public String provider = "";
    public SharedPreferences preferences;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setup_first_activity);
        ButterKnife.bind(this);

        prevFab.setVisibility(View.INVISIBLE);

        nextFAB = nextFab;
        prevFAB = prevFab;

        preferences = getApplicationContext().getSharedPreferences(LoginFragment.LOGIN_SHARED_PREF, Context.MODE_PRIVATE);
//        Boolean isGoogle = preferences.getBoolean(LoginFragment.IS_GOOGLE_LOGIN, false);
//        Boolean isKerberos = preferences.getBoolean(LoginFragment.IS_KERBEROS_LOGIN, false);

//        if (isKerberos) {
//            startFragment(new HostelSetup());
//        } else if (isGoogle) {
//            startFragment(new ClubSelectSetup());
//        }
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getBooleanExtra("editSubs", false)){
                provider = intent.getStringExtra("provider");
                startFragment(new EditSubscriptions());
            }
            else {
                provider = intent.getStringExtra("provider");
                if (!provider.equals("")) {
                    if (provider.equals("kerberos")) {
                        startFragment(new HostelSetup());
                    } else if (provider.equals("google")) {
                        startFragment(new ClubSelectSetup());
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = this.getSupportFragmentManager().findFragmentById(R.id.first_setup_frameLayout);

        if (fragment instanceof HostelSetup) {
            Toast.makeText(this, "Please fill the Required Information", Toast.LENGTH_SHORT).show();

        }
        else if (fragment instanceof ClubSelectSetup || fragment instanceof EditSubscriptions){
            if (doubleBackToExitPressedOnce) {
                finishApp();

            } else {
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Press the back button once again to quit", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2500);
            }
        } else {
            super.onBackPressed();
        }
    }

    private void finishApp(){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void startFragment (Fragment fragment) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.first_setup_frameLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
