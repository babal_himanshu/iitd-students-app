package in.ac.iitd.bsw.iitdapp.StudentResources.PapersAndStudentDiary;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.ac.iitd.bsw.iitdapp.R;

public class SubjectPapersAdapter extends RecyclerView.Adapter<SubjectPapersAdapter.SubjectPaperViewHolder>{
    private List<SubjectPapers> papers;
    private Context context;
    OnItemClickListener onItemClickListener;

    public class SubjectPaperViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView paperName;

        public SubjectPaperViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            paperName = (TextView) itemView.findViewById(R.id.papers_list_row_textView);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null){
                onItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public SubjectPapersAdapter(List<SubjectPapers> subjectPapers, Context context){
        this.papers = subjectPapers;
        this.context = context;
    }

    public interface OnItemClickListener {
        public void onItemClick (View view, int position);
    }

    public void setOnItemClickListener (final OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }


    @Override
    public SubjectPapersAdapter.SubjectPaperViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.papers_list_row, parent, false);
        return new SubjectPaperViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SubjectPapersAdapter.SubjectPaperViewHolder holder, int position) {
        SubjectPapers paper = papers.get(position);
        holder.paperName.setText(paper.getPaperName());
    }

    @Override
    public int getItemCount() {
        return papers.size();
    }
}
