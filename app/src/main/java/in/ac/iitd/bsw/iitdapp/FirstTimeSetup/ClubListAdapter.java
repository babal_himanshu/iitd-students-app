package in.ac.iitd.bsw.iitdapp.FirstTimeSetup;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.ac.iitd.bsw.iitdapp.Profile.Profile;
import in.ac.iitd.bsw.iitdapp.R;

public class ClubListAdapter extends RecyclerView.Adapter<ClubListAdapter.ClubViewHolder> {
    private List<ClubObject> clubObjectList;
    private OnItemClickListener onItemClickListener;
    private Context context;

    public class ClubViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView imageView;
        TextView textView;
        ImageButton button;

        public ClubViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.setup_club_row_imageView);
            textView = (TextView) itemView.findViewById(R.id.setup_club_row_textView);
            button = (ImageButton) itemView.findViewById(R.id.setup_club_row_button);

            button.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null){
                onItemClickListener.onSelectButtonClick(v, getAdapterPosition(), clubObjectList.get(getAdapterPosition()).getSelected());
            }
        }
    }

    public ClubListAdapter (List<ClubObject> clubObjectList, Context context) {
        this.clubObjectList = clubObjectList;
        this.context = context;
    }

    public interface OnItemClickListener {
        void onSelectButtonClick (View view, int position, Boolean isSelected);
    }

    public void setOnItemClickListener (final OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ClubViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.setup_club_select_row, parent, false);
        return new ClubViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ClubViewHolder holder, int position) {
        ClubObject object = clubObjectList.get(position);
        Boolean isSelected = object.getSelected();
        ImageButton selectButton = holder.button;

        if (isSelected){
            selectButton.setImageResource(R.drawable.ic_done_black_24dp);
            //selectButton.setBackgroundColor(Color.parseColor("#FF8000"));
//            selectButton.setText("SELECTED");
            //selectButton.setTextColor(Color.parseColor("#FFFFFF"));

        } else {
            selectButton.setImageResource(R.drawable.ic_clear_black_24dp);
            //selectButton.setBackgroundColor(Color.parseColor("#00ffffff"));
//            selectButton.setText("SELECT");
            //selectButton.setTextColor(Color.parseColor("#000000"));
        }

        holder.textView.setText(object.getName());
        //holder.imageView.setImageBitmap(object.getImage());
        Picasso.with(context).load(Profile.URL_DOWNLOAD_KERBEROS_DP + object.getCode()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return clubObjectList.size();
    }
}
