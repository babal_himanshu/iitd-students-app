package in.ac.iitd.bsw.iitdapp.StudentResources.Directory;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import in.ac.iitd.bsw.iitdapp.R;

/*
An Adapter class is required by RecyclerView
Adapter class uses a ViewHolder class
For more Info, visit -
https://developer.android.com/training/material/lists-cards.html
http://www.androidhive.info/2016/01/android-working-with-recycler-view/
 */

public class DirectoryAdapter extends RecyclerView.Adapter<DirectoryAdapter.DirectoryViewHolder> {
    private List<DirectoryObject> directoryObjectList;  //This list holds Objects to be displayed in recycler view.
    private Context context;                            //Context of the requesting Activity is required to make Alert Dialog.

    //Constructor for Adapter class
    public DirectoryAdapter(List<DirectoryObject> directoryObjectList, Context context) {
        this.directoryObjectList = directoryObjectList;
        this.context = context;
    }

    @Override
    public DirectoryAdapter.DirectoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflating row item layout (Row which will be displayed in recycler view)
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.directory_list_row, parent, false);
        return new DirectoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DirectoryAdapter.DirectoryViewHolder holder, int position) {
        //This method is called after inflating the row item/
        //The elements inside row are given values
        DirectoryObject object = directoryObjectList.get(position);
        holder.textView.setText(object.getName());
    }

    @Override
    public int getItemCount() {
        //Returning count to Recycler view to inform the size of data
        return directoryObjectList.size();
    }

    public class DirectoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;

        public DirectoryViewHolder(View itemView) {
            super(itemView);

            //Setting OnClickListener for itemView
            itemView.setOnClickListener(this);
            textView = (TextView) itemView.findViewById(R.id.directory_list_row_textView);
        }

        @Override
        public void onClick(View v) {
            int itemPosition = getAdapterPosition();

            //Building a new Alert Dialog Box
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            final String phoneNumber = directoryObjectList.get(itemPosition).getNumber().trim();
            builder.setTitle(directoryObjectList.get(itemPosition).getName().trim() + " : " + phoneNumber);
            builder.setMessage("Do You Want to Dial this Number");

            //Setting Positive (YES) Button for Alert Dialog
            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Intent to access Phone's Dialer
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    //Checking Permission (Required for >= Android M, 6.0 )
                    if (ActivityCompat.checkSelfPermission(context,
                            Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(context, "Please permit App to \n make Phone Call in settings", Toast.LENGTH_SHORT).show();
                        //Snackbar snackbar = Snackbar.make(context, "Permission", Snackbar.LENGTH_LONG).show();
                        //DO Something
                    } else {
                        context.startActivity(intent);
                    }

                }
            });
            //Setting Negative (NO) Button for Alert Dialog
            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }
}
