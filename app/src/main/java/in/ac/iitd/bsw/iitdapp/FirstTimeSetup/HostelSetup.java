package in.ac.iitd.bsw.iitdapp.FirstTimeSetup;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.Startup.SignIn.LoginFragment;
import in.ac.iitd.bsw.iitdapp.Utility.GeneralUtility;

public class HostelSetup extends Fragment {
    FloatingActionButton nextFab;
    FloatingActionButton prevFab;

    @BindView(R.id.setup_hostel_autoComp_textView)
    AutoCompleteTextView hostelAutoCompTextView;

    private static final String URL_SET_HOSTEL = "https://hostelcomp.iitd.ac.in/sethostel.php?";

    private RequestQueue requestQueue;
    private String[] hostelArray;

    // TODO: 7/24/16 -> check internet connection before every net required setup

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setup_hostel, container, false);
        ButterKnife.bind(this, view);

        nextFab = FirstSetupActivity.nextFAB;
        prevFab = FirstSetupActivity.prevFAB;
        nextFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextFabClick();
            }
        });

        hostelArray = new String[]{"Aravali", "Girnar", "Jwalamukhi", "Karakoram", "Kumaon", "Nilgiri",
                "Shivalik", "Satpura", "Udaigiri", "Vindhyachal", "Zanskar", "Kailash", "Himadri"};

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, hostelArray);
        hostelAutoCompTextView.setAdapter(arrayAdapter);

        prevFab.setVisibility(View.INVISIBLE);

        requestQueue = Volley.newRequestQueue(getContext());

        return view;
    }

    public void nextFabClick(){
        final String hostel = hostelAutoCompTextView.getText().toString();

        if (!hostel.equals("") && Arrays.asList(hostelArray).contains(hostel)) {
            new MaterialDialog.Builder(getContext())
                    .title("Are you sure : " + hostel + " Hostel")
                    .content("Once entered, Hostel can not be changed \nSo Be careful while entering")
                    .positiveText("Yes")
                    .negativeText("No")
                    .negativeColor(Color.RED)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            submitHostel(hostel);
                        }
                    })
                    .show();

        } else {
            new MaterialDialog.Builder(getContext())
                    .title("Please Choose Valid Hostel")
                    .content("Please choose a valid Hostel to proceed.")
                    .positiveText("Okay")
                    .show();
        }
    }

    private void submitHostel(String hostel){
        SharedPreferences preferences = this.getActivity().getSharedPreferences(LoginFragment.LOGIN_SHARED_PREF, Context.MODE_PRIVATE);
        String Kerebros_uid = preferences.getString(LoginFragment.KERBEROS_USERID, "");

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("Kerebros_uid", Kerebros_uid);
        hashMap.put("Hostel", hostel);

        StringRequest stringRequest = GeneralUtility.sendSimplePostRequest(URL_SET_HOSTEL, hashMap);
        requestQueue.add(stringRequest);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(LoginFragment.KERBEROS_HOSTEL, hostel);
        editor.apply();

        goToNextSettingFragment();
    }

    private void goToNextSettingFragment(){
        ClubSelectSetup clubSelectSetup = new ClubSelectSetup();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.first_setup_frameLayout, clubSelectSetup);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null);
        ft.commit();
    }
}
