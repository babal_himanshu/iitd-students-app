//package in.ac.iitd.bsw.iitdapp.StudentResources.Directory;
//
//import android.Manifest;
//import android.content.pm.PackageManager;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.ActivityCompat;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.DefaultItemAnimator;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import in.ac.iitd.bsw.iitdapp.R;
//
//public class Directory extends AppCompatActivity {
//    private RecyclerView directoryListRecyclerView;
//    private DirectoryAdapter directoryAdapter;
//    private List<DirectoryObject> directoryObjectList = new ArrayList<>();
//    private RequestQueue requestQueue;
//    private static final String URL = "http://bsw.iitd.ac.in/app/default.php";
//    private static final String TAG = "directory";
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.directory_list);
//
//        directoryListRecyclerView = (RecyclerView) findViewById(R.id.directory_list_recyclerView);
//        directoryAdapter = new DirectoryAdapter(directoryObjectList, this);
//        requestQueue = Volley.newRequestQueue(getApplicationContext());
//
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
//        directoryListRecyclerView.setLayoutManager(layoutManager);
//        directoryListRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        directoryListRecyclerView.setAdapter(directoryAdapter);
//
//        prepareDirectory();
//        isCallPermissionGranted();
//
//    }
//
//    public void prepareDirectory(){
//        StringRequest stringRequest = new StringRequest
//                (Request.Method.POST, URL, new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String  response) {
//                        try {
//                            parseJSON(response.trim());
//                            Log.i(TAG, response.trim());
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        error.printStackTrace();
//                    }
//                }){
//            @Override
//            public String getBodyContentType() {
//                return "application/x-www-form-urlencoded; charset=UTF-8";
//            }
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String, String > map = new HashMap<>();
//                map.put("type", "directory");
//                return map;
//            }
//        };
//
//        requestQueue.add(stringRequest);
//    }
//
//    private void parseJSON(String request) throws JSONException {
//        JSONObject object = new JSONObject(request);
//        JSONArray directory_array = object.getJSONArray("directory");
//
//        Log.i(TAG, String.valueOf(directory_array));
//
//        JSONObject impNumbers = directory_array.getJSONObject(0);
//        JSONObject services = directory_array.getJSONObject(2);
//
//        Log.i(TAG, String.valueOf(impNumbers));
//        Log.i(TAG, String.valueOf(services));
//
//        JSONArray impNumArray = impNumbers.getJSONArray("nums");
//        JSONArray servicesArray = services.getJSONArray("nums");
//
//        Log.i(TAG, String.valueOf(impNumArray));
//        Log.i(TAG, String.valueOf(servicesArray));
//
//        for (int i = 0; i < impNumArray.length(); i ++){
//            JSONObject jObject = impNumArray.getJSONObject(i);
//            String dirName = jObject.getString("name");
//            String dirNo = jObject.getString("no");
//            //removing '-' and ' ' from contact number
//            dirNo = dirNo.replace(" ", "");
//            dirNo = dirNo.replace("-", "");
//
//            DirectoryObject directoryObject = new DirectoryObject(dirName, dirNo);
//            directoryObjectList.add(directoryObject);
//        }
//        directoryAdapter.notifyDataSetChanged();
//
//        for (int i = 0; i < servicesArray.length(); i ++){
//            JSONObject jObject = servicesArray.getJSONObject(i);
//            String dirName = jObject.getString("name");
//            String dirNo = jObject.getString("no");
//            dirNo = dirNo.replace(" ", "");
//            dirNo = dirNo.replace("-", "");
//
//            DirectoryObject directoryObject = new DirectoryObject(dirName, dirNo);
//            directoryObjectList.add(directoryObject);
//        }
//        directoryAdapter.notifyDataSetChanged();
//
//    }
//
//    public boolean isCallPermissionGranted() {
//        if (Build.VERSION.SDK_INT >= 23) {
//            if (this.checkSelfPermission(Manifest.permission.CALL_PHONE)
//                    == PackageManager.PERMISSION_GRANTED) {
//                Log.v(TAG, "Permission is granted");
//                return true;
//            }
//            else {
//
//                Log.v(TAG, "Permission is revoked");
//                //ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
//                return false;
//            }
//        } else { //permission is automatically granted on sdk<23 upon installation
//            Log.v(TAG, "Permission is granted");
//            return true;
//        }
//
//
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
//            //resume tasks needing this permission
//        }
//    }
//}
