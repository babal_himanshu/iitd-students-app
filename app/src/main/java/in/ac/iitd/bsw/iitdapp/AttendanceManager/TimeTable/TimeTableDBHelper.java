package in.ac.iitd.bsw.iitdapp.AttendanceManager.TimeTable;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class TimeTableDBHelper extends SQLiteOpenHelper{
    private static final String DATABASE_NAME = "timeTable.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = " , ";
    private static final String NOT_NULL = "NOT NULL";

    private static final String[] ALL_COLUMNS = {TimeTableDBContract.TimeTableEntry._ID,
            TimeTableDBContract.TimeTableEntry.COLUMN_NAME_COURSE_NO,
            TimeTableDBContract.TimeTableEntry.COLUMN_NAME_ROOM_NO,
            TimeTableDBContract.TimeTableEntry.COLUMN_NAME_DAY,
            TimeTableDBContract.TimeTableEntry.COLUMN_NAME_START_TIME,
            TimeTableDBContract.TimeTableEntry.COLUMN_NAME_END_TIME,
            TimeTableDBContract.TimeTableEntry.COLUMN_NAME_FACULTY,
            TimeTableDBContract.TimeTableEntry.COLUMN_NAME_COURSE_NAME,
            TimeTableDBContract.TimeTableEntry.COLUMN_NAME_USER_ID};

    private static final String CREATE_ENTRIES =
            "CREATE TABLE " + TimeTableDBContract.TimeTableEntry.TABLE_NAME + " (" +
                    TimeTableDBContract.TimeTableEntry._ID + " INTEGER PRIMARY KEY," +
                    TimeTableDBContract.TimeTableEntry.COLUMN_NAME_COURSE_NO + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    TimeTableDBContract.TimeTableEntry.COLUMN_NAME_ROOM_NO + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    TimeTableDBContract.TimeTableEntry.COLUMN_NAME_DAY + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    TimeTableDBContract.TimeTableEntry.COLUMN_NAME_START_TIME + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    TimeTableDBContract.TimeTableEntry.COLUMN_NAME_END_TIME + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    TimeTableDBContract.TimeTableEntry.COLUMN_NAME_FACULTY + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    TimeTableDBContract.TimeTableEntry.COLUMN_NAME_COURSE_NAME + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    TimeTableDBContract.TimeTableEntry.COLUMN_NAME_USER_ID + TEXT_TYPE + NOT_NULL +
            " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TimeTableDBContract.TimeTableEntry.TABLE_NAME;

    public TimeTableDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onUpdate() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public boolean insertCourse (String course_no, String room_no, String day, String start_time,
                                 String end_time, String faculty, String user_id, String course_name) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TimeTableDBContract.TimeTableEntry.COLUMN_NAME_COURSE_NO, course_no);
        contentValues.put(TimeTableDBContract.TimeTableEntry.COLUMN_NAME_ROOM_NO, room_no);
        contentValues.put(TimeTableDBContract.TimeTableEntry.COLUMN_NAME_DAY, day);
        contentValues.put(TimeTableDBContract.TimeTableEntry.COLUMN_NAME_START_TIME, start_time);
        contentValues.put(TimeTableDBContract.TimeTableEntry.COLUMN_NAME_END_TIME, end_time);
        contentValues.put(TimeTableDBContract.TimeTableEntry.COLUMN_NAME_FACULTY, faculty);
        contentValues.put(TimeTableDBContract.TimeTableEntry.COLUMN_NAME_USER_ID, user_id);
        contentValues.put(TimeTableDBContract.TimeTableEntry.COLUMN_NAME_COURSE_NAME, course_name);

        long newRowID;
        newRowID = database.insert(TimeTableDBContract.TimeTableEntry.TABLE_NAME, null, contentValues);

        return ( newRowID != -1 );
    }

    public List<CourseTimeTableObject> getAllCourses () {
        List<CourseTimeTableObject> list = new ArrayList<>();

        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(TimeTableDBContract.TimeTableEntry.TABLE_NAME, ALL_COLUMNS,
                null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            CourseTimeTableObject object = cursorToCourseObject(cursor);
            list.add(object);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    private CourseTimeTableObject cursorToCourseObject (Cursor cursor) {
        CourseTimeTableObject object = new CourseTimeTableObject(cursor.getString(1), cursor.getString(2),
                cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(8),
                cursor.getString(7));
        object.set_id(cursor.getLong(0));

        return object;
    }
}
