package in.ac.iitd.bsw.iitdapp.AttendanceManager.TimeTable;

import org.parceler.Parcel;

@Parcel
public class CourseTimeTableObject {
    String course_no;
    String room_no;
    String day;
    String start_time;
    String end_time;
    String user_name;
    String user_id;
    String course_name;
    int attendance_criteria;
    Long _id;

    //Empty Constructor required for Parceler to work
    public CourseTimeTableObject () { }

    public CourseTimeTableObject (String course_no, String room_no, String day, String start_time,
                                  String end_time, String user_name, String user_id, String course_name) {
        this.course_no = course_no;
        this.room_no = room_no;
        this.day = day;
        this.start_time = start_time;
        this.end_time = end_time;
        this.user_name = user_name;
        this.user_id = user_id;
        this.course_name = course_name;
    }

    public CourseTimeTableObject (String course_no, String day, String start_time, String end_time) {
        this.course_no = course_no;
        this.day = day;
        this.start_time = start_time;
        this.end_time = end_time;
    }

    public int getAttendance_criteria() {
        return attendance_criteria;
    }

    public void setAttendance_criteria(int attendance_criteria) {
        this.attendance_criteria = attendance_criteria;
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getCourse_no() {
        return course_no;
    }

    public void setCourse_no(String course_no) {
        this.course_no = course_no;
    }

    public String getRoom_no() {
        return room_no;
    }

    public void setRoom_no(String room_no) {
        this.room_no = room_no;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
