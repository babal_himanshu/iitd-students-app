package in.ac.iitd.bsw.iitdapp.Startup.SignIn;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import in.ac.iitd.bsw.iitdapp.BaseActivity;
import in.ac.iitd.bsw.iitdapp.FirstTimeSetup.HostelSetup;
import in.ac.iitd.bsw.iitdapp.Profile.LinkAccounts;
import in.ac.iitd.bsw.iitdapp.R;

public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attedence_activity);

        if (getIntent() != null) {
            Boolean link = getIntent().getBooleanExtra("link", false);
            if (link) {
                startFragment(new LinkAccounts());
            } else {
                startFragment(new LoginFragment());
            }
        } else {
            startFragment(new LoginFragment());
        }

    }

    @Override
    public void onBackPressed() {
        Fragment fragment = this.getSupportFragmentManager().findFragmentById(R.id.attendance_manager_container_fragment);

        if (fragment instanceof LoginFragment) {
            Toast.makeText(this, "Press the back button once again to quit", Toast.LENGTH_SHORT).show();

        } else {
            super.onBackPressed();
        }
    }

    private void finishApp(){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    private void startFragment (Fragment fragment) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.attendance_manager_container_fragment, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
