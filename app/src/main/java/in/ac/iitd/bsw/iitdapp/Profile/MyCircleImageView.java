package in.ac.iitd.bsw.iitdapp.Profile;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ImageView;

public class MyCircleImageView extends ImageView {
    Bitmap mainImg;
    Bitmap sideImg;
    Context context;

    public MyCircleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public void setMainImg(int mainImg) {
        this.mainImg = BitmapFactory.decodeResource(context.getResources(),
                mainImg);

    }

    public void setSideImg(int sideImg) {
        this.sideImg = BitmapFactory.decodeResource(context.getResources(),
                sideImg);
    }

    public void setMainImg(Bitmap mainImg) {
        this.mainImg = mainImg;
    }

    public void setSideImg(Bitmap sideImg) {
        this.sideImg = sideImg;
    }

    @Override
    protected void onDraw(Canvas canvas) {
////        Drawable sideImage = getDrawable();
////        Drawable mainImage = getBackground();
//        Drawable sideImage = new BitmapDrawable(getResources(), sideImg);
//        Drawable mainImage = new BitmapDrawable(getResources(), mainImg);
////        Drawable sideImage = sideImg;
////        Drawable mainImage = mainImg;
//
//        if (sideImage == null && mainImage == null) {
//            return;
//        }
//
//        if (getWidth() == 0 || getHeight() == 0) {
//            return;
//        }
//
//        assert sideImage != null;
//        assert mainImage != null;
        Bitmap b1 = sideImg;
        Bitmap b2 = mainImg;
        Bitmap sideBitmap = b1.copy(Bitmap.Config.ARGB_8888, true);
        Bitmap mainBitmap = b2.copy(Bitmap.Config.ARGB_8888, true);

        int w = getWidth(), h = getHeight();

        Bitmap finalBitmap = createFinalBitmap(mainBitmap, sideBitmap, w/2);
        Bitmap finalBit = createBit(finalBitmap, sideBitmap, w/2);
        canvas.drawBitmap(finalBit, 0, 0, null);
    }

    private Bitmap createFinalBitmap(Bitmap mainBitmap, Bitmap sideBitmap, int radius){
        Bitmap mainBit;
        Bitmap sideBit;

        if (mainBitmap.getWidth() != 2 * radius || mainBitmap.getHeight() != 2 * radius)
            mainBit = Bitmap.createScaledBitmap(mainBitmap, 2 * radius, 2 * radius, false);
        else
            mainBit = mainBitmap;

        int smallRadii = (int) (radius * 0.292893218);
        if (sideBitmap.getWidth() != 2 * smallRadii || sideBitmap.getHeight() != 2 * smallRadii)
            sideBit = Bitmap.createScaledBitmap(sideBitmap, 2 * smallRadii, 2 * smallRadii, false);
        else
            sideBit = sideBitmap;

        Bitmap output = Bitmap.createBitmap(mainBit.getWidth(), mainBit.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Paint mainPaint = new Paint();
        final Paint sidePaint = new Paint(Paint.FILTER_BITMAP_FLAG);

        final Rect mainRect = new Rect(0, 0, 2 * radius, 2 * radius);
        final Rect sideRect = new Rect(2 * radius - 2 * smallRadii, 2 * radius - 2 * smallRadii, 2 * radius, 2 * radius);

        mainPaint.setAntiAlias(true);
        sidePaint.setAntiAlias(true);
        mainPaint.setFilterBitmap(true);
        sidePaint.setFilterBitmap(true);
        mainPaint.setDither(true);
        sidePaint.setDither(true);

        canvas.drawARGB(0, 0, 0, 0);

        mainPaint.setColor(Color.parseColor("#BAB399"));
        sidePaint.setColor(Color.parseColor("#BAB399"));

        canvas.drawCircle(radius + 0.7f, radius + 0.7f, radius + 0.1f, mainPaint);

        canvas.drawCircle(2 * radius - smallRadii + 0.7f, 2 * radius -smallRadii + 0.7f,
                smallRadii + 0.1f, sidePaint);

        mainPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        sidePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));

        canvas.drawBitmap(mainBit, mainRect, mainRect, mainPaint);
        canvas.drawBitmap(sideBit, sideRect, sideRect, sidePaint);
//        canvas.drawBitmap(mainBit, mainRect, mainRect, mainPaint);

        return output;
    }

    private Bitmap createBit(Bitmap finalBitmap, Bitmap sideBitmap, int radius) {
        Bitmap sideBit;

        int smallRadii = (int) (radius * 0.292893218);
        if (sideBitmap.getWidth() != 2 * smallRadii || sideBitmap.getHeight() != 2 * smallRadii)
            sideBit = Bitmap.createScaledBitmap(sideBitmap, 2 * smallRadii, 2 * smallRadii, false);
        else
            sideBit = sideBitmap;

        Canvas canvas = new Canvas(finalBitmap);

        final Paint sidePaint = new Paint(Paint.FILTER_BITMAP_FLAG);

        final Rect sideRect = new Rect(2 * radius - 2 * smallRadii, 2 * radius - 2 * smallRadii, 2 * radius, 2 * radius);

        sidePaint.setAntiAlias(true);
        sidePaint.setFilterBitmap(true);
        sidePaint.setDither(true);

        canvas.drawARGB(0, 0, 0, 0);

        sidePaint.setColor(Color.parseColor("#BAB399"));

        canvas.drawCircle(2 * radius - smallRadii + 0.7f, 2 * radius -smallRadii + 0.7f,
                smallRadii + 0.1f, sidePaint);

        sidePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));

        canvas.drawBitmap(sideBit, sideRect, sideRect, sidePaint);
//        canvas.drawBitmap(mainBit, mainRect, mainRect, mainPaint);

        return finalBitmap;
    }
}
