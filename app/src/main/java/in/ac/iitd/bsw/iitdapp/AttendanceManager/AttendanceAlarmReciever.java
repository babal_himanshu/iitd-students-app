package in.ac.iitd.bsw.iitdapp.AttendanceManager;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import in.ac.iitd.bsw.iitdapp.R;

public class AttendanceAlarmReciever extends BroadcastReceiver {
    public static NotificationManager manager;
    private String courseCode;
    private String roomNo;
    NotificationManager mNotificationManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("time", "onReceive");
        mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager = mNotificationManager;

        //if (intent.getAction().equals("attendance")) {
            if (intent.getExtras().containsKey("type")) {
                Log.i("time", "containsKey");
                courseCode = intent.getExtras().getString("courseCode");
                roomNo = intent.getExtras().getString("courseRoom");

                String type = intent.getExtras().getString("type");
                if (type.equals("end")) {
                    Log.i("time", "end");
                    classEndNotif(context);

                } else if (type.equals("start")) {
                    Log.i("time", "start");
                    classStartNotif(context);

                } else if (type.equals("test")) {
                    Log.i("time", "test");
                    testNotif(context);

                } else {
                    //Nothing
                }
            }
        //}
    }

    private void testNotif (Context context) {
        Intent notificationIntent = new Intent();
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setContentTitle("You Have " + courseCode + " class at " + roomNo)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setSmallIcon(R.drawable.ic_library_books_white_24dp)
                .setLights(0xff00ff00, 1000, 1000)
                .setAutoCancel(true);

        mNotificationManager.notify(2, mBuilder.build());

        Handler h = new Handler();
        long delayInMilliseconds = 3 * 60 * 1000;
        h.postDelayed(new Runnable() {
            public void run() {
                mNotificationManager.cancel(2);
            }
        }, delayInMilliseconds);
    }

    private void classStartNotif (Context context) {
        Intent notificationIntent = new Intent();
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setContentTitle("You Have " + courseCode + " at " + roomNo)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setSmallIcon(R.drawable.ic_library_books_white_24dp)
                .setLights(0xff00ff00, 1000, 1000)
                .setAutoCancel(true);

        mNotificationManager.notify(1, mBuilder.build());

        Handler h = new Handler();
        long delayInMilliseconds = 3 * 60 * 1000;
        h.postDelayed(new Runnable() {
            public void run() {
                mNotificationManager.cancel(1);
            }
        }, delayInMilliseconds);
    }

    private void classEndNotif (Context context) {
        Intent notificationIntent = new Intent(context, AttendanceActivity.class);
        notificationIntent.putExtra("courseCode", courseCode);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        Intent classAttended = new Intent(context, AttendanceNotificationServiceClass.class);
        classAttended.setAction("action");
        classAttended.putExtra("courseCode", courseCode);
        classAttended.putExtra("class", "attended");
        PendingIntent pClassAttended = PendingIntent.getService(context, 11, classAttended, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent classBunked = new Intent(context, AttendanceNotificationServiceClass.class);
        classBunked.setAction("action");
        classBunked.putExtra("courseCode", courseCode);
        classBunked.putExtra("class", "bunked");
        PendingIntent pClassBunked = PendingIntent.getService(context, 22, classBunked, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent classOff = new Intent(context, AttendanceNotificationServiceClass.class);
        classOff.setAction("action");
        classOff.putExtra("courseCode", courseCode);
        classOff.putExtra("class", "off");
        PendingIntent pClassOff = PendingIntent.getService(context, 33, classOff, PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setSmallIcon(R.drawable.ic_library_books_white_24dp)
                .setContentTitle(courseCode + " Just Ended")
                .setContentText("Did you attend " + courseCode + " ?")
                .addAction(R.drawable.ic_thumb_up_white_24dp, "Attended", pClassAttended)
                .addAction(R.drawable.ic_thumb_down_white_24dp, "Bucked", pClassBunked)
                .addAction(R.drawable.ic_clear_white_24dp, "Class Off", pClassOff)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setPriority(Notification.PRIORITY_HIGH)
                .setLights(0xff00ff00, 1000, 1000)
                .setAutoCancel(true);

        mNotificationManager.notify(0, mBuilder.build());

        Handler h = new Handler();
        long delayInMilliseconds = 4 * 60 * 1000;
        h.postDelayed(new Runnable() {
            public void run() {
                mNotificationManager.cancel(0);
            }
        }, delayInMilliseconds);
    }

//    private void makeNotif (Context context, PendingIntent pendingIntent, long delayInMilliseconds, String title,
//                            String text) {
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
//                .setVisibility(Notification.VISIBILITY_PUBLIC)
//                .setSmallIcon(R.drawable.ic_library_books_white_24dp)
//                .setContentTitle(title)
//                .setContentText(text)
//                .addAction(R.drawable.ic_thumb_up_white_24dp, "Attended", pClassAttended)
//                .addAction(R.drawable.ic_thumb_down_white_24dp, "Bucked", pClassBunked)
//                .addAction(R.drawable.ic_clear_white_24dp, "Class Off", pClassOff)
//                .setContentIntent(pendingIntent)
//                .setDefaults(Notification.DEFAULT_VIBRATE)
//                .setPriority(Notification.PRIORITY_HIGH)
//                .setLights(0xff00ff00, 1000, 1000)
//                .setAutoCancel(true);
//
//        mNotificationManager.notify(0, mBuilder.build());
//
//        Handler h = new Handler();
//        //long delayInMilliseconds = 4 * 60 * 1000;
//        h.postDelayed(new Runnable() {
//            public void run() {
//                mNotificationManager.cancel(0);
//            }
//        }, delayInMilliseconds);
//    }
}
