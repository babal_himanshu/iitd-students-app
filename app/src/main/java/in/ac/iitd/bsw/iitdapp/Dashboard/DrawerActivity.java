package in.ac.iitd.bsw.iitdapp.Dashboard;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.squareup.picasso.Picasso;

import in.ac.iitd.bsw.iitdapp.BaseActivity;
import in.ac.iitd.bsw.iitdapp.FirstTimeSetup.FirstSetupActivity;
import in.ac.iitd.bsw.iitdapp.Map.Map;
import in.ac.iitd.bsw.iitdapp.Profile.Profile;
import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.Startup.SignIn.LoginActivity;
import in.ac.iitd.bsw.iitdapp.Startup.SignIn.LoginFragment;
import in.ac.iitd.bsw.iitdapp.StudentResources.ResourcesActivity;

public class DrawerActivity extends BaseActivity {
    public Toolbar toolbar;
    public DrawerLayout drawerLayout;
    public Drawer result;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        toolbar = (Toolbar) findViewById (R.id.toolbar_drawer);
        setSupportActionBar (toolbar);
//        navigationView = (NavigationView) findViewById(R.id.navigation_view);
//        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
//
//
//        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(MenuItem item) {
//
//                drawerLayout.closeDrawers();
//
//                Intent intent = new Intent(DrawerActivity.this, ResourcesActivity.class);
//                startActivity(intent);
//                return false;
//            }
//        });
//
//        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
//                super.onDrawerClosed(drawerView);
//            }
//
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
//
//                super.onDrawerOpened(drawerView);
//            }
//        };
//
//
//
//        drawerLayout.setDrawerListener(actionBarDrawerToggle);
//
//        //calling sync state is necessay or else your hamburger icon wont show up
//        actionBarDrawerToggle.syncState();
//
//
//        Drawer drawer = new DrawerBuilder()
//                .withActivity(this)
//                .withToolbar(toolbar)
//                .withActionBarDrawerToggleAnimated(true)
//                .build();
        final SharedPreferences preferences = getSharedPreferences(LoginFragment.LOGIN_SHARED_PREF, Context.MODE_PRIVATE);

        String name = "";
        String photo_url = "";

        Boolean isGoogle = preferences.getBoolean(LoginFragment.IS_GOOGLE_LOGIN, false);
        Boolean isKerberos = preferences.getBoolean(LoginFragment.IS_KERBEROS_LOGIN, false);

        if (isKerberos) {
            String Kerebros_uid = preferences.getString(LoginFragment.KERBEROS_USERID, "");
            name = preferences.getString(LoginFragment.KERBEROS_USERNAME, "");
            photo_url = Profile.URL_DOWNLOAD_KERBEROS_DP + Kerebros_uid;

        } else if (isGoogle) {
            name = preferences.getString(LoginFragment.GOOGLE_NAME, "");
            photo_url = preferences.getString(LoginFragment.GOOGLE_PHOTO, "");
        }

        DrawerImageLoader.init(new AbstractDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                Picasso.with(imageView.getContext()).load(uri).placeholder(R.drawable.ic_account_circle_black_24dp).into(imageView);
            }
            @Override
            public void cancel(ImageView imageView) {
                Picasso.with(imageView.getContext()).cancelRequest(imageView);
            }
        });

        // TODO: 7/24/16 -> make background blurry
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withSelectionListEnabledForSingleProfile(false)
                .withSelectionListEnabled(false)
                .withHeaderBackground(R.drawable.book_bkg)
                .addProfiles(new ProfileDrawerItem().withName(name).withIcon(photo_url))
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean current) {
                        return false;
                    }
                })
                .build();

        //create the drawer and remember the `Drawer` result object
        // TODO: 08/08/16 http://stackoverflow.com/questions/38673653/how-to-change-the-humberger-icon-in-toolbar
        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .withActionBarDrawerToggleAnimated(true)
                .build();

        final PrimaryDrawerItem resources = new PrimaryDrawerItem().withName("Student Resources").withSelectable(false);
        final PrimaryDrawerItem map = new PrimaryDrawerItem().withName("Institute Map").withSelectable(false);
        final PrimaryDrawerItem dashboard = new PrimaryDrawerItem().withName("Dashboard").withSelectable(false);
        final PrimaryDrawerItem editSubs = new PrimaryDrawerItem().withName("Edit Subscriptions").withSelectable(false);
//        final PrimaryDrawerItem item5 = new PrimaryDrawerItem().withIdentifier(1).withName("item-5");

        final PrimaryDrawerItem logout = new PrimaryDrawerItem().withName("Logout").withSelectable(false);
        final PrimaryDrawerItem linkAccounts = new PrimaryDrawerItem().withName("Link Kerberos Account").withIcon(R.drawable.ic_link_black_24dp).withSelectable(false);
        if (! isKerberos){
            result.addStickyFooterItem(linkAccounts);
        }
        result.addStickyFooterItem(logout);

        result.addItems(resources, map, new DividerDrawerItem(), dashboard, editSubs, new DividerDrawerItem());

        result.setOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
            @Override
            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                Log.i("drawer", String.valueOf(position));
                if (drawerItem.equals(resources)) {
                    Intent intent = new Intent(DrawerActivity.this, ResourcesActivity.class);
                    startActivity(intent);
                    result.closeDrawer();

                } else if (drawerItem.equals(map)) {
                    Intent intent = new Intent(DrawerActivity.this, Map.class);
                    startActivity(intent);
                    Log.i("drawer", "item-2");
                    result.closeDrawer();

                } else if (drawerItem.equals(dashboard)) {
                    Intent intent = new Intent(DrawerActivity.this, DashBoardActivity.class);
                    startActivity(intent);
                    Log.i("drawer", "item-3");
                    result.closeDrawer();

                } else if (drawerItem.equals(editSubs)) {
                    Boolean isKerberos = preferences.getBoolean(LoginFragment.IS_KERBEROS_LOGIN, false);
                    Intent intent = new Intent(DrawerActivity.this, FirstSetupActivity.class);
                    if (isKerberos) {
                        intent.putExtra("provider", "kerberos");
                    } else {
                        intent.putExtra("provider", "google");
                    }
                    intent.putExtra("editSubs", true);
                    startActivity(intent);
                    result.closeDrawer();

                } else if (drawerItem.equals(logout)) {
                    Log.i("drawer", "item-5");
                    new AlertDialog.Builder(DrawerActivity.this)
                            .setTitle("Are you Sure ?")
                            .setMessage("Are you Sure you want to exit.\nAll saved Settings would be destroyed")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    SharedPreferences preferences = getApplicationContext().getSharedPreferences(LoginFragment.LOGIN_SHARED_PREF, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.clear();
                                    editor.apply();
                                    quitApp();

                                    dialogInterface.dismiss();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .show();

                    result.closeDrawer();

                } else if (drawerItem.equals(linkAccounts)) {
                    Log.i("drawer", "item-link");
                    Intent intent = new Intent(DrawerActivity.this, LoginActivity.class);
                    intent.putExtra("link", true);
                    startActivity(intent);
                    result.closeDrawer();
                }
                return true;
            }
        });

        drawerLayout = result.getDrawerLayout();
    }

    private void quitApp(){
        this.finish();
        System.exit(0);
    }
}
