package in.ac.iitd.bsw.iitdapp.StudentResources.PapersAndStudentDiary;

public class SubjectPapers {
    private String paperName;
    private String paperURL;

    public SubjectPapers(String paperName, String paperURL){
        this.paperName = paperName;
        this.paperURL = paperURL;
    }

    public String getPaperName() {
        return paperName;
    }

    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }

    public String getPaperURL() {
        return paperURL;
    }

    public void setPaperURL(String paperURL) {
        this.paperURL = paperURL;
    }
}
