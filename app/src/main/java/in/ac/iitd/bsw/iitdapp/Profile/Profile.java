package in.ac.iitd.bsw.iitdapp.Profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.ac.iitd.bsw.iitdapp.Dashboard.DashBoardActivity;
import in.ac.iitd.bsw.iitdapp.FirstTimeSetup.FirstSetupActivity;
import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.Startup.SignIn.LoginFragment;
import in.ac.iitd.bsw.iitdapp.Startup.SignIn.MainActivity;

public class Profile extends Fragment implements View.OnClickListener{
//    @BindView(R.id.profile_circularImage)
//    CircleImageView circleImageView;
    @BindView(R.id.profile_circularImage)
    MyCircleImageView circleImageView;
//    @BindView(R.id.profile_camera_button)
//    ImageButton selectFromCameraButton;
//    @BindView(R.id.profile_gallery_button)
//    ImageButton selectFromGalleryButton;
//    @BindView(R.id.profile_upload_button)
//    Button uploadButton;
    @BindView(R.id.profile_changeDP_textView)
    TextView changeDPTextView;
    @BindView(R.id.profile_name_textView)
    TextView nameText;
    @BindView(R.id.profile_entry_textView)
    TextView entryText;
    @BindView(R.id.profile_hostel_textView)
    TextView hostelText;
//    @BindView(R.id.profile_edit_button)
//    ImageButton editProfileButton;
    @BindView(R.id.profile_edit_subs_button)
    Button editSubsButton;

    private static final String TAG = "Profile";
    private static final String URL_UPLOAD_PHOTO = "https://hostelcomp.iitd.ac.in/image_upload.php";
    public static final String URL_DOWNLOAD_KERBEROS_DP = "https://hostelcomp.iitd.ac.in/getPhoto.php?Kerebros_uid=";

    private Bitmap userDP;
    private SharedPreferences preferences;
    private String Kerebros_uid;
    private Boolean isGoogle;
    private Boolean isKerberos;

    private ArrayAdapter<String> changeDPArrayAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment, container, false);
        ButterKnife.bind(this, view);
        preferences = this.getActivity().getSharedPreferences(LoginFragment.LOGIN_SHARED_PREF, Context.MODE_PRIVATE);
        Kerebros_uid = preferences.getString(LoginFragment.KERBEROS_USERID, "");
        isGoogle = preferences.getBoolean(LoginFragment.IS_GOOGLE_LOGIN, false);
        isKerberos = preferences.getBoolean(LoginFragment.IS_KERBEROS_LOGIN, false);

        //((DashBoardActivity)getActivity()).setActionBarTitle("Profile");
        //((DashBoardActivity)getActivity()).toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        //getActivity().setTitle("Profile");

        circleImageView.setMainImg(R.drawable.background_material);
        circleImageView.setSideImg(R.drawable.ic_photo_camera_black_24dp);

        //changeDPTextView.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/marker_felt.ttf"));

//        selectFromCameraButton.setOnClickListener(this);
//        selectFromGalleryButton.setOnClickListener(this);
//        editProfileButton.setOnClickListener(this);
        editSubsButton.setOnClickListener(this);

        changeDPArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, new String[]{"Use Camera", "Choose from Gallery"});

        String name = "";
        if (isKerberos) {
            circleImageView.setOnClickListener(this);

//            Picasso.with(getContext()).load(URL_DOWNLOAD_KERBEROS_DP + Kerebros_uid)
//                    .placeholder(R.drawable.error_image).into(circleImageView);

            String entry = "";
            String hostel = "";

            name += preferences.getString(LoginFragment.KERBEROS_USERNAME, "");
            nameText.setText(name);
            nameText.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/marker_felt.ttf"));
            entry += preferences.getString(LoginFragment.KERBEROS_ENTRYNO, "");
            entryText.setText(entry);
            entryText.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/marker_felt.ttf"));
            hostel += preferences.getString(LoginFragment.KERBEROS_HOSTEL, "");
            hostelText.setText(hostel);
            hostelText.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/marker_felt.ttf"));
        } else if (isGoogle) {
            Picasso.with(getContext()).load(preferences.getString(LoginFragment.GOOGLE_PHOTO, ""))
                    .placeholder(R.drawable.error_image).into(circleImageView);

            name += preferences.getString(LoginFragment.GOOGLE_NAME, "");
            nameText.setText(name);
            nameText.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/marker_felt.ttf"));
            changeDPTextView.setTextSize(15);
            changeDPTextView.setText("Logged in as Guest \nClick on Link Kerberos Account in Navigation Drawer to access full features");
            entryText.setVisibility(View.GONE);
            hostelText.setVisibility(View.GONE);
        }

        return view;
    }

    // TODO: 7/24/16 -> limit size of uploading image
    private void uploadImageToServer(String ker_id){
        Bitmap bitmap = userDP;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        byte [] byte_arr = stream.toByteArray();
        String image_str = Base64.encodeToString(byte_arr, 0);
        final ArrayList<NameValuePair> nameValuePairs = new  ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("image",image_str));
        nameValuePairs.add(new BasicNameValuePair("Kerebros_uid",ker_id));

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(URL_UPLOAD_PHOTO);
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpclient.execute(httppost);
                    Log.i(TAG, convertResponseToString(response));

                }catch(final Exception e){
                    Log.i(TAG, "Error in http connection "+e.toString());
                }
            }
        });
        thread.start();
    }

    public String convertResponseToString(HttpResponse response) throws IllegalStateException, IOException{
        String res = "";
        StringBuffer buffer = new StringBuffer();
        InputStream inputStream = response.getEntity().getContent();
        final int contentLength = (int) response.getEntity().getContentLength(); //getting content length…..

        if (contentLength >= 0) {
            byte[] data = new byte[512];
            int len ;
            try {
                while (-1 != (len = inputStream.read(data)) ) {
                    buffer.append(new String(data, 0, len)); //converting to string and appending  to stringbuffer…..
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            try {
                inputStream.close(); // closing the stream…..
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            res = buffer.toString();     // converting stringbuffer to string…..
        }
        return res;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.profile_circularImage) {
            new MaterialDialog.Builder(getContext())
                    .title("Select an Option")
                    .adapter(changeDPArrayAdapter,
                            new MaterialDialog.ListCallback() {
                                @Override
                                public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                    //Toast.makeText(getContext(), "Clicked item " + which, Toast.LENGTH_SHORT).show();
                                    if (which == 0) { //Camera
                                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                        startActivityForResult(intent, 0);//CAMERA request code is 0.
                                        dialog.dismiss();

                                    } else if (which == 1) { //Gallery
                                        Intent intent = new Intent(
                                                Intent.ACTION_PICK,
                                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        intent.setType("image/*");
                                        startActivityForResult(
                                                Intent.createChooser(intent, "Select File"), 1);//GALLERY request code is 1.
                                        dialog.dismiss();
                                    }
                                }
                            })
                    .show();
        }
//        if(v.getId() == R.id.profile_camera_button){
//            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            startActivityForResult(intent, 0);//CAMERA request code is 0.
//        }
//        else if(v.getId() == R.id.profile_gallery_button){
//            Intent intent = new Intent(
//                    Intent.ACTION_PICK,
//                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//            intent.setType("image/*");
//            startActivityForResult(
//                    Intent.createChooser(intent, "Select File"), 1);//GALLERY request code is 1.
//        }
//        else if(v.getId() == R.id.profile_edit_button) {//Upload Image Button Clicked
//            //uploadImageToServer(Kerebros_uid);
        else if (v.getId() == R.id.profile_edit_subs_button) {
            Intent intent = new Intent(getActivity(), FirstSetupActivity.class);
            if (isKerberos) {
                intent.putExtra("provider", "kerberos");
            } else {
                intent.putExtra("provider", "google");
            }
            intent.putExtra("editSubs", true);
            startActivity(intent);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == MainActivity.RESULT_OK && data != null) {
            if (requestCode == 0) {//CAMERA
                userDP = (Bitmap) data.getExtras().get("data");
                //circleImageView.setImageBitmap(userDP);
                circleImageView.setMainImg(userDP);

                uploadImageToServer(Kerebros_uid);
            }
            else if(requestCode == 1){//Gallery
                Uri selectedImg = data.getData();
                try {
                    userDP = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImg);
                    int nh = (int) ( userDP.getHeight() * (512.0 / userDP.getWidth()) );
                    userDP = Bitmap.createScaledBitmap(userDP, 512, nh, true);
                    circleImageView.setImageBitmap(userDP);

                    uploadImageToServer(Kerebros_uid);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
