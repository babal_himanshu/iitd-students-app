//package in.ac.iitd.bsw.iitdapp.AttendanceManager;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Toast;
//
//import com.daimajia.swipe.SimpleSwipeListener;
//import com.daimajia.swipe.SwipeLayout;
//import com.daimajia.swipe.adapters.BaseSwipeAdapter;
//
//import java.util.List;
//
//import in.ac.iitd.bsw.iitdapp.AttendanceManager.TimeTable.CourseTimeTableObject;
//import in.ac.iitd.bsw.iitdapp.R;
//
//public class SwipeAdapter extends BaseSwipeAdapter {
//    private Context context;
//    private List<CourseTimeTableObject> list;
//
//    public SwipeAdapter (Context context, List<CourseTimeTableObject> list) {
//        this.context = context;
//        this.list = list;
//    }
//
//    @Override
//    public int getSwipeLayoutResourceId(int position) {
//        return R.id.swipe;
//    }
//
//    @Override
//    public View generateView(int position, ViewGroup parent) {
//        View v = LayoutInflater.from(context).inflate(R.layout.attendance_adapter_row, null);
//        SwipeLayout swipeLayout = (SwipeLayout)v.findViewById(getSwipeLayoutResourceId(position));
//        swipeLayout.addSwipeListener(new SimpleSwipeListener() {
//            @Override
//            public void onOpen(SwipeLayout layout) {
//                //YoYo.with(Techniques.Tada).duration(500).delay(100).playOn(layout.findViewById(R.id.swipe_one));
//            }
//
//        });
//        swipeLayout.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
//            @Override
//            public void onDoubleClick(SwipeLayout layout, boolean surface) {
//                Toast.makeText(context, "DoubleClick", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
////        swipeLayout.addRevealListener(R.id.swipe_reveal, new SwipeLayout.OnRevealListener() {
////            @Override
////            public void onReveal(View child, SwipeLayout.DragEdge edge, float fraction, int distance) {
////                Toast.makeText(context, "Reveal", Toast.LENGTH_SHORT).show();
////            }
////        });
//
////        TextView textView = (TextView) v.findViewById(R.id.swipe_reveal3);
////        swipeLayout.addDrag(SwipeLayout.DragEdge.Bottom, textView);
//
////        v.findViewById(R.id.swipe_two).setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                Toast.makeText(context, "click 1", Toast.LENGTH_SHORT).show();
////            }
////        });
////        v.findViewById(R.id.swipe_one).setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                Toast.makeText(context, "click 2", Toast.LENGTH_SHORT).show();
////            }
////        });
//        return v;
//    }
//
//    @Override
//    public void fillValues(int position, View convertView) {
////        TextView t = (TextView)convertView.findViewById(R.id.position_textView);
////        t.setText(list.get(position).getCourse_no());
////
////        TextView textView = (TextView) convertView.findViewById(R.id.swipe_reveal);
////        textView.setText(list.get(position).getCourse_name());
//    }
//
//    @Override
//    public int getCount() {
//        return list.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return null;
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//}
