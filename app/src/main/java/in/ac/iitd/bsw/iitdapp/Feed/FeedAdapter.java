package in.ac.iitd.bsw.iitdapp.Feed;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.ac.iitd.bsw.iitdapp.Dashboard.DashBoardActivity;
import in.ac.iitd.bsw.iitdapp.Profile.Profile;
import in.ac.iitd.bsw.iitdapp.R;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.FeedViewHolder>{
    private List<FeedObject> feedObjectList;
    private OnItemClickListener onItemClickListener;
    private Context context;

    public class FeedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.feed_club_icon_imageView)
        ImageView clubIconImageView;
        @BindView(R.id.feed_post_picture_imageView)
        ImageView feedPostImageView;
        @BindView(R.id.feed_club_name_textView)
        TextView clubNameTextView;
        @BindView(R.id.feed_post_date_textView)
        TextView postDateTextView;
        @BindView(R.id.feed_post_title_textView)
        TextView postTitleTextView;
        @BindView(R.id.feed_desc_expand_text_view)
        ExpandableTextView postDescExpandableTextView;

        public FeedViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null){
                onItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public FeedAdapter(List<FeedObject> feedObjectList, Context context) {
        this.feedObjectList = feedObjectList;
        this.context = context;
    }

    public interface OnItemClickListener {
        void onItemClick (View view, int position);
    }

    public void setOnItemClickListener (final OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public FeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.feed_row, parent, false);
        return new FeedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FeedViewHolder holder, int position) {
        FeedObject object = feedObjectList.get(position);
        String uuid = object.getClubID() + "_" + object.getPostDate().substring(0, 10) + "_" + object.getPostDate().substring(11);
        object.setImageUUID(uuid);

        DisplayMetrics metrics = new DisplayMetrics();
        ((DashBoardActivity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int screenWidth = metrics.widthPixels;
        int imageHeight = (int) (screenWidth / object.getImageRatio());

        if (object.getHasImage()){
//            Bitmap postPic = DBUtility.getBitmapFromURL(Profile.URL_DOWNLOAD_KERBEROS_DP + uuid);
//            assert postPic != null;
//            int widthPic = postPic.getWidth();
//            int heightPic = postPic.getHeight();
//
//            int newWidth = screenWidth;
//            int newHeight = (int)((float) heightPic * ((float) newWidth / (float) widthPic));
//            Log.i("feed", "w : " + newWidth + "   h : " + newHeight);
//            postPic = Bitmap.createScaledBitmap(postPic, newWidth, newHeight, true);
//            holder.feedPostImageView.setImageBitmap(postPic);
            Log.i("feed", String.valueOf(screenWidth));
            Log.i("feed", String.valueOf(imageHeight));
            //Glide.with(context).load(Profile.URL_DOWNLOAD_KERBEROS_DP + uuid).fitCenter().into(holder.feedPostImageView);
            Picasso.with(context).load(Profile.URL_DOWNLOAD_KERBEROS_DP + uuid).resize(screenWidth, imageHeight).into(holder.feedPostImageView);

        } else {
            holder.feedPostImageView.setVisibility(View.GONE);
        }


        Picasso.with(context).load(Profile.URL_DOWNLOAD_KERBEROS_DP + object.getClubID()).into(holder.clubIconImageView);
        //holder.clubIconImageView.setImageResource(object.getClubIcon());
        holder.clubNameTextView.setText(object.getClubName());
        holder.postDateTextView.setText(object.getPostDate());
        holder.postTitleTextView.setText(object.getPostTitle());
        holder.postDescExpandableTextView.setText(object.getPostDesc());
    }

    @Override
    public int getItemCount() {
        return feedObjectList.size();
    }


}
