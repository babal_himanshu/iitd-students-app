package in.ac.iitd.bsw.iitdapp.AttendanceManager.TimeTable;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import in.ac.iitd.bsw.iitdapp.AttendanceManager.AttendanceActivity;
import in.ac.iitd.bsw.iitdapp.BaseActivity;
import in.ac.iitd.bsw.iitdapp.R;

public class TimeTable extends BaseActivity{
    private TimeTableDBHelper timeTableDBHelper;
    private RequestQueue requestQueue;
    private static final String URL = "https://hostelcomp.iitd.ac.in/cgi-bin/finale.cgi";
    private static final String TAG = "time-table";
    RecyclerView recyclerView;
    List<CourseTimeTableObject> list;
    AutoCompleteTextView textView;
    MultiAutoCompleteTextView multiAutoCompleteTextView;
    TimeTableAdapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayAdapter<String> arrayAdapter;
    Button goButton;
    LinearLayout linearLayout;
    FloatingActionButton fab;
    String[] array;
    long[] _idArray;
    List<AutoCompleteTextView> viewList = new ArrayList<>();

    public static List<CourseTimeTableObject> courseObjectsList = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.time_table);


        timeTableDBHelper = new TimeTableDBHelper(this);
        requestQueue = Volley.newRequestQueue(this);
        recyclerView = (RecyclerView) findViewById(R.id.time_table_recyclerView);
        textView = (AutoCompleteTextView) findViewById(R.id.time_table_auto_complete_textView);
        multiAutoCompleteTextView = (MultiAutoCompleteTextView) findViewById(R.id.time_table_multi_auto_complete_textView);
        goButton = (Button) findViewById(R.id.time_table_go_button);
        fab = (FloatingActionButton) findViewById(R.id.time_table_fab);
        linearLayout = (LinearLayout) findViewById(R.id.time_table_scroll_linear_layout);


        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (AutoCompleteTextView autoCompleteTextView : viewList){
                    String selectedText = autoCompleteTextView.getText().toString();

                    Log.i("debug", selectedText);
                    int _id = Arrays.asList(array).indexOf(selectedText);
                    Log.i("debug", String.valueOf(_id));
                    if (_id != -1) {
                        courseObjectsList.add(list.get(_id));
                        Log.i("debug", String.valueOf(courseObjectsList.size()));
                    }
                }

//                Intent i = new Intent(TimeTable.this, AttendanceManager.class);
//                startActivity(i);

            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AutoCompleteTextView autoCompleteTextView = new AutoCompleteTextView(TimeTable.this);
                autoCompleteTextView.setAdapter(arrayAdapter);
                autoCompleteTextView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                linearLayout.addView(autoCompleteTextView);

                viewList.add(autoCompleteTextView);


            }
        });


        prepareTimeTableData();
    }

    private void prepareTimeTableData(){
        StringRequest stringRequest = new StringRequest
                (Request.Method.GET, URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            parseJSON(response.trim());
                            Log.i(TAG, response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        requestQueue.add(stringRequest);
    }

    private void parseJSON(String request) throws JSONException {
        JSONObject object = new JSONObject(request);
        Boolean success = object.getBoolean("Success");

        if (success) {
            JSONArray time_table_array = object.getJSONArray("resp");

            timeTableDBHelper.onUpdate();

            for (int i = 0; i < time_table_array.length(); i++) {
                JSONObject jsonObject = time_table_array.getJSONObject(i);

                String course_no = jsonObject.getString("course_no");
                String room_no = jsonObject.getString("room_no");
                String day = jsonObject.getString("day");
                String start_time = jsonObject.getString("start_time");
                String end_time = jsonObject.getString("end_time");
                String user_name = jsonObject.getString("user_name");
                String user_id = jsonObject.getString("user_id");
                String course_name = jsonObject.getString("course_name");

                timeTableDBHelper.insertCourse(course_no, room_no, day, start_time, end_time, user_name, user_id, course_name);
            }

            Intent intent = new Intent(this, AttendanceActivity.class);
            startActivity(intent);

            list = timeTableDBHelper.getAllCourses();
            adapter = new TimeTableAdapter(list);
            layoutManager = new LinearLayoutManager(this);
            assert recyclerView != null;
            recyclerView.hasFixedSize();
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            array = new String[list.size()];
            _idArray = new long[list.size()];
            for (int i = 0; i < list.size(); i ++){
                array[i] = list.get(i).getCourse_no();
                _idArray[i] = list.get(i).get_id();
            }


            //courseObjectsList = list;

            arrayAdapter = new ArrayAdapter<>(TimeTable.this, android.R.layout.simple_dropdown_item_1line, array);
            textView.setAdapter(arrayAdapter);
            multiAutoCompleteTextView.setAdapter(arrayAdapter);
            multiAutoCompleteTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

            String[] multiString = multiAutoCompleteTextView.getText().toString().split(", ");

            //courseObjectsList = Arrays.asList(multiString);

        }
    }

}
