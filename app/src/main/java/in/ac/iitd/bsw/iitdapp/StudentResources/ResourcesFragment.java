package in.ac.iitd.bsw.iitdapp.StudentResources;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.RequestQueue;

import java.util.ArrayList;
import java.util.List;

import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.StudentResources.Directory.DirectoryFragment;
import in.ac.iitd.bsw.iitdapp.StudentResources.Links.LinksFragment;
import in.ac.iitd.bsw.iitdapp.StudentResources.PapersAndStudentDiary.DownloadPDFFromURL;
import in.ac.iitd.bsw.iitdapp.StudentResources.PapersAndStudentDiary.QuestionBankFragment;

public class ResourcesFragment extends Fragment {
    private RecyclerView resourcesListRecyclerView;
    private ResourcesAdapter resourcesAdapter;
    private List<ResourcesObject> resourcesObjectList = new ArrayList<>();
    private RequestQueue requestQueue;
    private DownloadPDFFromURL download;
    private LinearLayout linearLayout;
    private static final String URL = "http://bsw.iitd.ac.in/stuff/diary.pdf";
    private static final String TAG = "@@-resources";
    private static final String LINKS = "Important Links";
    private static final String DIRECTORY = "Telephone Directory";
    private static final String DIARY = "Student Diary";
    private static final String PAPERS = "Question Papers";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.resources_list, container, false);

        setRetainInstance(true);

        //requestQueue = Volley.newRequestQueue(getContext());
        //resourcesListRecyclerView = (RecyclerView) rootView.findViewById(R.id.resources_list_recyclerView);

        requestQueue = ((ResourcesActivity)getActivity()).requestQueue;
        //linearLayout = (LinearLayout) rootView.findViewById(R.id.fragment_resource_linearLayout_viewGroup);
        linearLayout = ((ResourcesActivity)getActivity()).linearLayout;
        //resourcesListRecyclerView = ((ResourcesActivity)getActivity()).resourcesRecyclerView;
        //RecyclerView.LayoutManager layoutManager = ((ResourcesActivity)getActivity()).layoutManager;
        resourcesListRecyclerView = (RecyclerView) rootView.findViewById(R.id.resources_list_recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());

        resourcesAdapter = new ResourcesAdapter(resourcesObjectList, getContext());
        resourcesListRecyclerView.setLayoutManager(layoutManager);
        resourcesListRecyclerView.setAdapter(resourcesAdapter);
        resourcesListRecyclerView.setItemAnimator(new DefaultItemAnimator());

        download = new DownloadPDFFromURL(getActivity(), URL, "Student_Diary.pdf", "", rootView, "diary");

        prepareResourcesData();

        if (getArguments() != null) {
            if (getArguments().getString("diary") != null) {
                download.view("/BSW-IITD/Student_Diary.pdf");
            }
        }


        resourcesAdapter.setOnItemClickListener(new ResourcesAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position == 0) {
                    //LINKS
                    LinksFragment linksFragment = new LinksFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.activity_resource_frame, linksFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                    //Intent intent = new Intent(getActivity(), Links.class);
                    //startActivity(intent);
                }
                else if (position == 1) {
                    //DIRECTORY
                    DirectoryFragment directoryFragment = new DirectoryFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.activity_resource_frame, directoryFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                    //Intent intent = new Intent(getActivity(), Directory.class);
                    //startActivity(intent);
                }
                else if (position == 2) {
                    //DIARY
                    if (((ResourcesActivity)getActivity()).isStoragePermissionGranted()) {
                        download.download();
                    }
                }
                else if (position == 3) {
                    //PAPERS
                    QuestionBankFragment questionBankFragment = new QuestionBankFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.activity_resource_frame, questionBankFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                    //Intent intent = new Intent(getActivity(), QuestionBank.class);
                    //startActivity(intent);
                }
                else {
                    //Nothing
                }
            }
        });

        return rootView;
    }


    @Override
    public void onDestroyView() {
        Log.i(TAG, "fragment-onDestroyView");
        resourcesObjectList.clear();
        resourcesAdapter.notifyDataSetChanged();
        super.onDestroyView();
    }


    //    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.resources_list);
//
//        requestQueue = Volley.newRequestQueue(this);
//        resourcesListRecyclerView = (RecyclerView) findViewById(R.id.resources_list_recyclerView);
//        resourcesAdapter = new ResourcesAdapter(resourcesObjectList, this);
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
//        resourcesListRecyclerView.setLayoutManager(layoutManager);
//        resourcesListRecyclerView.setAdapter(resourcesAdapter);
//        resourcesListRecyclerView.setItemAnimator(new DefaultItemAnimator());
//
//        prepareResourcesData();
//
//        resourcesAdapter.setOnItemClickListener(new ResourcesAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                if (position == 0) { //LINKS
//                    Intent intent = new Intent(getActivity(), Links.class);
//                    startActivity(intent);
//                }
//                else if (position == 1) { //DIRECTORY
//                    Intent intent = new Intent(getActivity(), Directory.class);
//                    startActivity(intent);
//                }
//                else if (position == 2) { //DIARY
//                    download = new DownloadPDFFromURL(getActivity(), URL, "Student_Diary.pdf",
//                            "", resourcesListRecyclerView);
//                    download.download();
//                }
//                else if (position == 3) { //PAPERS
//                    Intent intent = new Intent(getActivity(), QuestionBank.class);
//                    startActivity(intent);
//                }
//                else {
//                    //Nothing
//                }
//            }
//        });
//    }

    private void prepareResourcesData(){
        resourcesObjectList.add(new ResourcesObject(LINKS));
        resourcesObjectList.add(new ResourcesObject(DIRECTORY));
        resourcesObjectList.add(new ResourcesObject(DIARY));
        resourcesObjectList.add(new ResourcesObject(PAPERS));

        resourcesAdapter.notifyDataSetChanged();
    }

//    public boolean isStoragePermissionGranted() {
//        if (Build.VERSION.SDK_INT >= 23) {
//            if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    == PackageManager.PERMISSION_GRANTED) {
//                Log.v(TAG, "Permission is granted");
//                return true;
//            }
//            else {
//
//                Log.v(TAG, "Permission is revoked");
//                //ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//                return false;
//            }
//        } else { //permission is automatically granted on sdk<23 upon installation
//            Log.v(TAG, "Permission is granted");
//            return true;
//        }
//
//
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
//            //resume tasks needing this permission
//        }
//    }
//
//    @Override
//    protected void onNewIntent(Intent intent) {
//        Bundle extras = intent.getExtras();
//        if (extras != null) {
//            if (extras.containsKey("test")) {
//                Log.i(TAG, extras.getString("test"));
//
//                if (extras.getString("test").equals("download-complete")) {
//
//                    download.view(extras.getString("file_dir"));
//                }
//            }
//        }
//    }
}