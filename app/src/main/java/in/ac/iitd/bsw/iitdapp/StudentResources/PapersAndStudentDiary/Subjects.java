package in.ac.iitd.bsw.iitdapp.StudentResources.PapersAndStudentDiary;

public class Subjects {
    private String subjectName;

    public Subjects(String subjectName){
        this.subjectName = subjectName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }
}
