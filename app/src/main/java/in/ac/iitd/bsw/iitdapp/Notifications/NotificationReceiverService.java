package in.ac.iitd.bsw.iitdapp.Notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import in.ac.iitd.bsw.iitdapp.Dashboard.DashBoardActivity;
import in.ac.iitd.bsw.iitdapp.Profile.Profile;
import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.Startup.SignIn.HttpsTrustManager;

public class NotificationReceiverService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        HttpsTrustManager.allowAllSSL();

        if (remoteMessage.getData().size() > 0) {
            Map<String, String> data = remoteMessage.getData();
            Log.d("notif", "Message data payload: " + remoteMessage.getData());
            String hasPic = data.get("hasPic");
            final String title = data.get("title");
            final String desc = data.get("detail");
            final String clubName = data.get("clubName");
            final String clubID = data.get("clubID");

            if (hasPic.equals("true")) {
                final String uuid = data.get("uuid");
                // Get a handler that can be used to post to the main thread
                Handler mainHandler = new Handler(this.getMainLooper());
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        sendNotification notification = new sendNotification(NotificationReceiverService.this);
                        notification.execute(title, desc, clubName, clubID, uuid);
                    }
                };
                mainHandler.post(myRunnable);

            } else {
                Intent intent = new Intent(this, DashBoardActivity.class);
                intent.putExtra("key", "value");
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 100, intent, PendingIntent.FLAG_ONE_SHOT);

                NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
                Notification notif = new Notification.Builder(this)
                        .setContentIntent(pendingIntent)
                        .setContentTitle(data.get("title"))
                        .setContentText(data.get("detail"))
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .build();
                notif.flags |= Notification.FLAG_AUTO_CANCEL;
                notificationManager.notify(1, notif);
            }
        }
    }

    private class sendNotification extends AsyncTask<String, Void, Bitmap> {
        Context ctx;
        String title, message, clubName, clubID, uuid ;

        public sendNotification(Context context) {
            super();
            this.ctx = context;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            InputStream in;
            title = params[0];
            message = params[1];
            clubName = params[2];
            clubID = params[3];
            uuid = params[4];

            try {
                URL url = new URL(Profile.URL_DOWNLOAD_KERBEROS_DP + clubID + "_" + uuid);
                Log.i("notif", String.valueOf(url));
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                in = connection.getInputStream();
                return BitmapFactory.decodeStream(in);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {

            super.onPostExecute(result);
            try {
                NotificationManager notificationManager = (NotificationManager) ctx
                        .getSystemService(Context.NOTIFICATION_SERVICE);

                Intent intent = new Intent(ctx, DashBoardActivity.class);
                intent.putExtra("key", "value");
                PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 100, intent, PendingIntent.FLAG_ONE_SHOT);

                Notification notification = new Notification.Builder(ctx)
                        .setContentIntent(pendingIntent)
                        .setContentTitle(clubName + "  : " + title)
                        .setContentText(message)
                        .setSmallIcon(R.drawable.ic_account_circle_black_24dp)
                        .setStyle(new Notification.BigPictureStyle().bigPicture(result))
                        .build();

                // hide the notification after its selected
                notification.flags |= Notification.FLAG_AUTO_CANCEL;

                notificationManager.notify(1, notification);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
