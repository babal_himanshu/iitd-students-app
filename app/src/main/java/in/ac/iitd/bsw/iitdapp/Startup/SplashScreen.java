package in.ac.iitd.bsw.iitdapp.Startup;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import in.ac.iitd.bsw.iitdapp.BaseActivity;
import in.ac.iitd.bsw.iitdapp.Dashboard.DashBoardActivity;
import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.Startup.SignIn.LoginActivity;
import in.ac.iitd.bsw.iitdapp.Startup.SignIn.LoginFragment;

public class SplashScreen extends BaseActivity {
    private static int SPLASH_TIME_OUT = 200;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences preferences = getApplicationContext().getSharedPreferences(LoginFragment.LOGIN_SHARED_PREF, Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = preferences.edit();
//                editor.clear();
//                editor.commit();
                Boolean kerberos = preferences.getBoolean(LoginFragment.IS_KERBEROS_LOGIN, false);
                Boolean google = preferences.getBoolean(LoginFragment.IS_GOOGLE_LOGIN, false);

                Intent intent;
                if (!kerberos && !google) {
                    intent = new Intent(SplashScreen.this, LoginActivity.class);

                } else {
                    intent = new Intent(SplashScreen.this, DashBoardActivity.class);
                }

//                intent = new Intent(SplashScreen.this, LoginActivity.class);

                startActivity(intent);

                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
