package in.ac.iitd.bsw.iitdapp.StudentResources;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.ac.iitd.bsw.iitdapp.R;

public class ResourcesAdapter extends RecyclerView.Adapter<ResourcesAdapter.ResourcesViewHolder>{
    private List<ResourcesObject> resourcesObjectList;
    private Context context;
    OnItemClickListener onItemClickListener;

    public class ResourcesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;

        public ResourcesViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            textView =  (TextView) itemView.findViewById(R.id.resources_list_row_textView);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null){
                onItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public ResourcesAdapter (List<ResourcesObject> resourcesObjectList, Context context) {
        this.resourcesObjectList = resourcesObjectList;
        this.context = context;
    }

    public interface OnItemClickListener {
        void onItemClick (View view, int position);
    }

    public void setOnItemClickListener (final OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ResourcesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.resources_list_row, parent, false);
        return new ResourcesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ResourcesViewHolder holder, int position) {
        ResourcesObject object = resourcesObjectList.get(position);
        holder.textView.setText(object.getName());
    }

    @Override
    public int getItemCount() {
        return resourcesObjectList.size();
    }
}
