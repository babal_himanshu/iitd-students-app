package in.ac.iitd.bsw.iitdapp.Map;

public class IITDplace {
    private String name,description;
    private double lat,lng;
    private int drawableID;


    public IITDplace(String name, String description, double lat, double lng){
        this.name=name;
        this.description=description;
        this.lat=lat;
        this.lng=lng;
    }

    public IITDplace(String name, double lat, double lng, int drawableID){
        this.name=name;
        this.lat=lat;
        this.lng=lng;
        this.drawableID=drawableID;
    }

    public String getName() {
        return name;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public int getDrawableID() { return drawableID;}
}
