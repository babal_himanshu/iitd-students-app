package in.ac.iitd.bsw.iitdapp.FirstTimeSetup;

import android.graphics.Bitmap;

public class ClubObject {
    private Bitmap image;
    private String code;
    private String name;
    private Boolean isSelected = false;

    public ClubObject (Bitmap image, String text, String name) {
        this.image = image;
        this.code = text;
        this.name = name;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }
}
