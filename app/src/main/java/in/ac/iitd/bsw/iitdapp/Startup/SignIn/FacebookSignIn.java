//package in.ac.iitd.bsw.iitdapp.Startup.SignIn;
//
//import android.app.ProgressDialog;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.facebook.AccessToken;
//import com.facebook.CallbackManager;
//import com.facebook.FacebookCallback;
//import com.facebook.FacebookException;
//import com.facebook.FacebookSdk;
//import com.facebook.appevents.AppEventsLogger;
//import com.facebook.login.LoginManager;
//import com.facebook.login.LoginResult;
//import com.facebook.login.widget.LoginButton;
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.auth.AuthCredential;
//import com.google.firebase.auth.AuthResult;
//import com.google.firebase.auth.FacebookAuthProvider;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseUser;
//
//import in.ac.iitd.bsw.iitdapp.R;
//
//public class FacebookSignIn extends MainActivity implements View.OnClickListener{
//
//    private static final String TAG = "FacebookLogin";
//
//    private TextView statusTextView;
//    private TextView detailTextView;
//    private LoginButton facebookLoginButton;
//    private Button facebookLogOutButton;
//
//    private FirebaseAuth firebaseAuth;
//    private FirebaseAuth.AuthStateListener authStateListener;
//
//    private CallbackManager callbackManager;
//
//    private ProgressDialog progressDialog;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        setContentView(R.layout.activity_facebook_signin);
//
//
//        AppEventsLogger.activateApp(this);
//
//        firebaseAuth = FirebaseAuth.getInstance();
//
//        statusTextView = (TextView) findViewById(R.id.facebook_status_textView);
//        detailTextView = (TextView) findViewById(R.id.facebook_detail_textView);
//
//        authStateListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                FirebaseUser user = firebaseAuth.getCurrentUser();
//                if (user != null) {
//                    // User is signed in
//                    Log.i(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
//                    statusTextView.setText("Signed In");
//                    detailTextView.setText(user.getDisplayName() + " " + user.getEmail() + " " + user.getUid()
//                     + " " + user.getPhotoUrl());
//                    Log.d(TAG, String.valueOf(user.getPhotoUrl()));
//                } else {
//                    // User is signed out
//                    Log.i(TAG, "onAuthStateChanged:signed_out");
//                }
//
//
//            }
//        };
//
//        callbackManager = CallbackManager.Factory.create();
//
//        facebookLoginButton = (LoginButton) findViewById(R.id.facebook_signIn_button);
//        facebookLogOutButton = (Button) findViewById(R.id.facebook_signOut_button);
//        facebookLogOutButton.setOnClickListener(this);
//
//        facebookLoginButton.setReadPermissions("email", "public_profile");
//        facebookLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                Log.i(TAG, "facebook:onSuccess:" + loginResult);
//                handleFacebookAccessToken(loginResult.getAccessToken());
//            }
//
//            @Override
//            public void onCancel() {
//                Log.i(TAG, "facebook:onCancel");
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//                Log.i(TAG, "facebook:onError");
//            }
//        });
//
//    }
//
//    private void handleFacebookAccessToken(AccessToken token) {
//        Log.d(TAG, "handleFacebookAccessToken:" + token);
//        // [START_EXCLUDE silent]
//        showProgressDialog();
//        // [END_EXCLUDE]
//
//        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
//        firebaseAuth.signInWithCredential(credential)
//                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
//
//                        // If sign in fails, display a message to the user. If sign in succeeds
//                        // the auth state listener will be notified and logic to handle the
//                        // signed in user can be handled in the listener.
//                        if (!task.isSuccessful()) {
//                            Log.w(TAG, "signInWithCredential", task.getException());
//                            Toast.makeText(FacebookSignIn.this, "Authentication failed.",
//                                    Toast.LENGTH_SHORT).show();
//                        }
//
//                        // [START_EXCLUDE]
//                        hideProgressDialog();
//                        // [END_EXCLUDE]
//                    }
//                });
//    }
//
//    // [START on_start_add_listener]
//    @Override
//    public void onStart() {
//        super.onStart();
//        firebaseAuth.addAuthStateListener(authStateListener);
//    }
//    // [END on_start_add_listener]
//
//    // [START on_stop_remove_listener]
//    @Override
//    public void onStop() {
//        super.onStop();
//        if (authStateListener != null) {
//            firebaseAuth.removeAuthStateListener(authStateListener);
//        }
//    }
//    // [END on_stop_remove_listener]
//
//    private void showProgressDialog() {
//        if (progressDialog == null) {
//            progressDialog = new ProgressDialog(this);
//            progressDialog.setMessage("Loading");
//            progressDialog.setIndeterminate(true);
//        }
//
//        progressDialog.show();
//    }
//
//    private void hideProgressDialog() {
//        if (progressDialog != null && progressDialog.isShowing()) {
//            progressDialog.hide();
//        }
//    }
//
//    @Override
//    public void onClick(View v) {
//        signOut();
//    }
//
//    public void signOut() {
//        firebaseAuth.signOut();
//        LoginManager.getInstance().logOut();
//        statusTextView.setText("Signed Out");
//        detailTextView.setText("Facebook Details Text View");
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        callbackManager.onActivityResult(requestCode, resultCode, data);
//    }
//
//}
