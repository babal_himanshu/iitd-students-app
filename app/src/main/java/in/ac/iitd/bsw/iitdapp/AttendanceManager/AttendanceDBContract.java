package in.ac.iitd.bsw.iitdapp.AttendanceManager;

import android.provider.BaseColumns;

public class AttendanceDBContract {

    public AttendanceDBContract() { }

    public static abstract class AttendanceDBEntry implements BaseColumns {
        public static final String TABLE_NAME = "attendance_records";
        public static final String COLUMN_NAME_COURSE_NO = "course_no";
        public static final String COLUMN_NAME_ROOM_NO = "room_no";
        public static final String COLUMN_NAME_DAY = "day";
        public static final String COLUMN_NAME_START_TIME = "start_time";
        public static final String COLUMN_NAME_END_TIME = "end_time";
        public static final String COLUMN_NAME_FACULTY = "user_name";
        public static final String COLUMN_NAME_USER_ID = "user_id";
        public static final String COLUMN_NAME_COURSE_NAME = "course_name";
        public static final String COLUMN_NAME_ATTENDANCE_CRITERIA = "attendance_criteria";
        public static final String COLUMN_NAME_CLASS_TOTAL = "class_total";
        public static final String COLUMN_NAME_CLASS_TOTAL_DATE = "date_class_total";
        //Class status will be
        //A -> Attended
        //B -> Bunked (By Default)
        //C -> Cancelled
        public static final String COLUMN_NAME_CLASS_STATUS = "class_status";
    }
}
