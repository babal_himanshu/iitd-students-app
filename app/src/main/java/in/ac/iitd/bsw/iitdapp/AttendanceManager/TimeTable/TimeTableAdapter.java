package in.ac.iitd.bsw.iitdapp.AttendanceManager.TimeTable;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.ac.iitd.bsw.iitdapp.R;

public class TimeTableAdapter extends RecyclerView.Adapter<TimeTableAdapter.TimeTableViewHolder> {
    List<CourseTimeTableObject> list;

    public class TimeTableViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public TimeTableViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.resources_list_row_textView);
        }
    }

    public TimeTableAdapter (List<CourseTimeTableObject> list) {
        this.list = list;
    }

    @Override
    public TimeTableAdapter.TimeTableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.resources_list_row, parent, false);
        return new TimeTableViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TimeTableAdapter.TimeTableViewHolder holder, int position) {
        CourseTimeTableObject object = list.get(position);
        holder.textView.setText(object.getCourse_no());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
