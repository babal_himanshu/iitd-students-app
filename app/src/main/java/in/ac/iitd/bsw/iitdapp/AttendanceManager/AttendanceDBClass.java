package in.ac.iitd.bsw.iitdapp.AttendanceManager;

import java.util.Date;
import java.util.List;

public class AttendanceDBClass {
    private List<String[]> a;
    private List<Date[]> b;

    public AttendanceDBClass (List<String[]> a, List<Date[]> b) {
        this.a = a;
        this.b = b;
    }

    public List<String[]> getA() {
        return a;
    }

    public void setA(List<String[]> a) {
        this.a = a;
    }

    public List<Date[]> getB() {
        return b;
    }

    public void setB(List<Date[]> b) {
        this.b = b;
    }
}
