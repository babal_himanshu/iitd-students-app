package in.ac.iitd.bsw.iitdapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URI;
import java.util.List;

import in.ac.iitd.bsw.iitdapp.Startup.SignIn.HttpsTrustManager;

public class BaseActivity extends AppCompatActivity {
    public static Context contextOfApplication;

    public static final String BASE_URL = "https://hostelcomp.iitd.ac.in/";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contextOfApplication = getApplicationContext();
        HttpsTrustManager.allowAllSSL();
        setProxy();
//        MultiDex.install(this);

//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//        Log.d("firebase-token", "Refreshed token: " + refreshedToken);
    }

    public static Context getContextOfApplication(){
        return contextOfApplication;
    }

    private void setProxy() {
        //Device proxy settings
        ProxySelector defaultProxySelector = ProxySelector.getDefault();
        Proxy proxy = null;
        List<Proxy> proxyList = defaultProxySelector.select(URI.create("http://www.google.in"));
        if (proxyList.size() > 0) {
            proxy = proxyList.get(0);

            Log.d("proxy", String.valueOf(proxy));

            try {
                String proxyType = String.valueOf(proxy.type());

                //setting HTTP Proxy
                if (proxyType.equals("HTTP")) {
                    String proxyAddress = String.valueOf(proxy.address());
                    String[] proxyDetails = proxyAddress.split(":");
                    String proxyHost = proxyDetails[0];
                    String proxyPort = proxyDetails[1];
                    Log.d("proxy", proxyType + " " + proxyHost + " " + proxyPort);

                    System.setProperty("http.proxyHost", proxyHost);
                    System.setProperty("http.proxyPort", proxyPort);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
