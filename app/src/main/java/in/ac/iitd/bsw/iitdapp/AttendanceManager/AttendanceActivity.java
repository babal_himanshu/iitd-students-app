package in.ac.iitd.bsw.iitdapp.AttendanceManager;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import java.util.List;

import in.ac.iitd.bsw.iitdapp.AttendanceManager.TimeTable.CourseTimeTableObject;
import in.ac.iitd.bsw.iitdapp.Dashboard.DrawerWithBottomBar;
import in.ac.iitd.bsw.iitdapp.R;

public class AttendanceActivity extends DrawerWithBottomBar {
    private List<CourseTimeTableObject> list;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attedence_activity);

//        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.drawer_bottom_bar_fragment_container);
//        View contentView = inflater.inflate(R.layout.attedence_activity, frameLayout, false);
//        linearLayoutContainer.addView(contentView, 0);

        toolbar = (Toolbar) findViewById (R.id.toolbar);
        setSupportActionBar (toolbar);

//        bottomBar = BottomBar.attach(findViewById(R.id.drawer_container_layout), savedInstanceState);
//        bottomBar.setMaxFixedTabs(2);
//        bottomBar.setItems(R.menu.bottom_bar);
//        bottomBar.setDefaultTabPosition(2);
//        bottomBar.setOnMenuTabClickListener(new OnMenuTabClickListener() {
//            @Override
//            public void onMenuTabSelected(@IdRes int menuItemId) {
//                if (menuItemId == R.id.bottomBar_feed) {
//                    startFragment(new Feed(), "feed");
//
//                } else if (menuItemId == R.id.bottomBar_profile) {
//                    startFragment(new AttendanceAddDates(), "AttendanceAddDates");
//
//                } else if (menuItemId == R.id.bottomBar_attendance) {
//
//
//                }
//
//            }
//
//            @Override
//            public void onMenuTabReSelected(@IdRes int menuItemId) {
//                if (menuItemId == R.id.bottomBar_feed) {
//                    startFragment(new Feed(), "feed");
//                }
//            }
//        });





        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            if (bundle.containsKey("test")) {
                Log.i("test", "onCreate-pass");
            }
        }

        if (savedInstanceState == null) {
            //startFragment(new AddSubjectFragment(), "AddSubjectFragment");
            startFragment(new AttendanceAddDates(), "AttendanceAddDates");
        }
        else {
            AddSubjectFragment fragment = (AddSubjectFragment) getSupportFragmentManager().findFragmentByTag("AddSubjectFragment");
        }

    }

//    @Subscribe
//    public void onEvent (EventTimeTableObjectList list) {
//        this.list = list.getList();
//        Log.i("Event", "activity-event");
//        for (CourseTimeTableObject object : this.list) {
//            Log.i("Event", object.getCourse_no());
//        }
//    }
//
//    @Subscribe
//    public void onList (ArrayList<CourseTimeTableObject> list) {
//        this.list = list;
//        Log.i("Event", "activity-list");
//        for (CourseTimeTableObject object : this.list) {
//            Log.i("Event", object.getCourse_no());
//        }
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        EventBus.getDefault().register(this);
//    }
//
//    @Override
//    public void onStop() {
//        EventBus.getDefault().unregister(this);
//        super.onStop();
//    }


    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            if (bundle.containsKey("test")) {
                setIntent(intent);
                Log.i("test", "onResume-pass");
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.i("test", "new Intent Bundle Called");
        Bundle extras = intent.getExtras();
        if (extras.containsKey("test")) {
            Log.i("test","passed");
            //startFragmentFromNotification(extras);
        }
        Log.i("test","failed");
    }

    private void startFragmentFromNotification(Bundle extras){
//        if (extras != null) {
//            if (extras.containsKey("notification")) {
//                if (extras.getString("notification").equals("download-complete")) {
//                    String FREGMENT_ID = extras.getString("fragment_id");
//                    if (FREGMENT_ID.equals("diary")){
//                        Bundle bundle = new Bundle();
//                        bundle.putString("diary", FREGMENT_ID);
//                        ResourcesFragment resourcesFragment = new ResourcesFragment();
//                        resourcesFragment.setArguments(bundle);
//                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                        fragmentTransaction.replace(R.id.activity_resource_frame, resourcesFragment);
//                        fragmentTransaction.addToBackStack(null);
//                        fragmentTransaction.commitAllowingStateLoss();
//                    }
//                    else {
//                        Bundle bundle = new Bundle();
//                        bundle.putString("subjectName", FREGMENT_ID);
//                        bundle.putString("paperName", extras.getString("file_dir"));
//                        ExamPapersFragment examPapersFragment = new ExamPapersFragment();
//                        examPapersFragment.setArguments(bundle);
//                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                        fragmentTransaction.replace(R.id.activity_resource_frame, examPapersFragment);
//                        fragmentTransaction.addToBackStack(null);
//                        fragmentTransaction.commitAllowingStateLoss();
//                    }
//                }
//            }
//        }
    }

    private void startFragment (Fragment fragment, String tag) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.attendance_manager_container_fragment, fragment, tag);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
