package in.ac.iitd.bsw.iitdapp.Startup.SignIn;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.StudentResources.PapersAndStudentDiary.DownloadPDFFromURL;

public class MainActivity extends AppCompatActivity {

    String TAG = "Download";
    DownloadPDFFromURL download;
    RequestQueue requestQueue;
    public static final String URL = "http://bsw.iitd.ac.in/app/default.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setProxy();

        requestQueue = Volley.newRequestQueue(this);

    }

    //setting Application-Wide proxy Values
    private void setProxy() {
        //Device proxy settings
        ProxySelector defaultProxySelector = ProxySelector.getDefault();
        Proxy proxy = null;
        List<Proxy> proxyList = defaultProxySelector.select(URI.create("http://www.google.in"));
        if (proxyList.size() > 0) {
            proxy = proxyList.get(0);

            Log.d("proxy", String.valueOf(proxy));

            try {
                String proxyType = String.valueOf(proxy.type());

                //setting HTTP Proxy
                if (proxyType.equals("HTTP")) {
                    String proxyAddress = String.valueOf(proxy.address());
                    String[] proxyDetails = proxyAddress.split(":");
                    String proxyHost = proxyDetails[0];
                    String proxyPort = proxyDetails[1];
                    Log.d("proxy", proxyType + " " + proxyHost + " " + proxyPort);

                    System.setProperty("http.proxyHost", proxyHost);
                    System.setProperty("http.proxyPort", proxyPort);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (this.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                //ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey("test")) {
                Log.i(TAG, extras.getString("test"));

                if (extras.getString("test").equals("download-complete")) {

                    download.view(extras.getString("file_dir"));
                }
            }
        }
    }

    public void sendJSONRequest(final HashMap<String, String> hashMap, String url){
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub

                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String > map = hashMap;
                return map;
            }
        };

        requestQueue.add(jsObjRequest);
    }

}
