package in.ac.iitd.bsw.iitdapp.AttendanceManager.TimeTable;

import android.provider.BaseColumns;

public class TimeTableDBContract {

    public TimeTableDBContract() { }

    public static abstract class TimeTableEntry implements BaseColumns {
        public static final String TABLE_NAME = "timeTable_table";
        public static final String COLUMN_NAME_COURSE_NO = "course_no";
        public static final String COLUMN_NAME_COURSE_NAME = "course_name";
        public static final String COLUMN_NAME_ROOM_NO = "room_no";
        public static final String COLUMN_NAME_DAY = "day";
        public static final String COLUMN_NAME_START_TIME = "start_time";
        public static final String COLUMN_NAME_END_TIME = "end_time";
        public static final String COLUMN_NAME_FACULTY = "user_name";
        public static final String COLUMN_NAME_USER_ID = "user_id";
    }
}
