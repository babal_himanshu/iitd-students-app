package in.ac.iitd.bsw.iitdapp.Utility;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class GeneralUtility {

//    public static void activityToFragment(ActivityCompat activity){
//        FragmentManager fragmentManager = activity.getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.dashboard_fragment_container, fragment, tag);
//        fragmentTransaction.addToBackStack(null);
//        fragmentTransaction.commit();
//    }

    public static StringRequest sendSimplePostRequest(String url, final HashMap<String, String> postMap){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("simple_Post", "POST : " + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return postMap;
            }

        };

        return stringRequest;
    }

    public static StringRequest sendSimpleGetRequest(String url){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("simple_Get", "GET : " + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        return stringRequest;
    }

    public static void quitApp(Activity activity){
        activity.finish();
        System.exit(0);
    }

}
