package in.ac.iitd.bsw.iitdapp.StudentResources.Links;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.StudentResources.ResourcesActivity;

public class LinksFragment extends Fragment{
    private RecyclerView linksListRecyclerView;
    private LinkAdapter linkAdapter;
    private List<LinkObject> linkObjectList = new ArrayList<>();
    private RequestQueue requestQueue;
    private static final String URL = "http://bsw.iitd.ac.in/app/default.php";
    private static final String TAG = "@@-links";

    // TODO: 6/25/16 -> insert .setHasFixedSize() for each recycler view used in project

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.links_list, container, false);
        setRetainInstance(true);

        requestQueue = ((ResourcesActivity)getActivity()).requestQueue;
        linksListRecyclerView = ((ResourcesActivity)getActivity()).resourcesRecyclerView;
        //RecyclerView.LayoutManager layoutManager = ((ResourcesActivity)getActivity()).layoutManager;
        //linksListRecyclerView = (RecyclerView) view.findViewById(R.id.links_list_recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());

        linkAdapter  = new LinkAdapter(linkObjectList, getActivity());
        linksListRecyclerView.setLayoutManager(layoutManager);
        linksListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        linksListRecyclerView.setAdapter(linkAdapter);

        prepareLinks();

        return view;
    }

    @Override
    public void onDestroyView() {
        Log.i(TAG, "fragment-onDestroyView");
        linkObjectList.clear();
        linkAdapter.notifyDataSetChanged();
        super.onDestroyView();
    }

//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.links_list);
//
//        requestQueue = Volley.newRequestQueue(getApplicationContext());
//        linksListRecyclerView = (android.support.v7.widget.RecyclerView) findViewById(R.id.links_list_recyclerView);
//        linkAdapter  = new LinkAdapter(linkObjectList, getApplicationContext());
//
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
//        linksListRecyclerView.setLayoutManager(layoutManager);
//        linksListRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        linksListRecyclerView.setAdapter(linkAdapter);
//
//        prepareLinks();
//    }

    public void prepareLinks(){
        StringRequest stringRequest = new StringRequest
                (Request.Method.POST, URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String  response) {
                        try {
                            parseJSON(response.trim());
                            //Log.i(TAG, response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String > map = new HashMap<>();
                map.put("type", "links");
                return map;
            }
        };

        requestQueue.add(stringRequest);
    }

    private void parseJSON(String request) throws JSONException {
        JSONObject object = new JSONObject(request);
        JSONArray useful_links_array = object.getJSONArray("useful_links");
        JSONArray social_links_array = object.getJSONArray("social_media_links");

        //Log.i(TAG, String.valueOf(useful_links_array.length()));
        //Log.i(TAG, String.valueOf(useful_links_array));
        //Log.i(TAG, String.valueOf(social_links_array.length()));
        //Log.i(TAG, String.valueOf(social_links_array));

        for (int i = 0; i < useful_links_array.length(); i ++){
            JSONObject jObject = useful_links_array.getJSONObject(i);
            String linkName = jObject.getString("name");
            String linkURL = jObject.getString("url");
            LinkObject linkObject = new LinkObject(linkName, linkURL);
            linkObjectList.add(linkObject);
        }
        linkAdapter.notifyDataSetChanged();

        for (int i = 0; i < social_links_array.length(); i ++){
            JSONObject jObject = social_links_array.getJSONObject(i);
            String linkName = jObject.getString("name");
            String linkURL = jObject.getString("url");
            LinkObject linkObject = new LinkObject(linkName, linkURL);
            linkObjectList.add(linkObject);
        }
        linkAdapter.notifyDataSetChanged();
    }


}
