package in.ac.iitd.bsw.iitdapp.AttendanceManager;

import java.util.List;

import in.ac.iitd.bsw.iitdapp.AttendanceManager.TimeTable.CourseTimeTableObject;

public class EventTimeTableObjectList {
    private List<CourseTimeTableObject> list;

    public EventTimeTableObjectList (List<CourseTimeTableObject> list) {
        this.list = list;
    }

    public List<CourseTimeTableObject> getList() {
        return list;
    }
}
