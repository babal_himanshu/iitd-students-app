package in.ac.iitd.bsw.iitdapp.Dashboard;

import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.roughike.bottombar.BottomBar;

import in.ac.iitd.bsw.iitdapp.R;

public class DrawerWithBottomBar extends DrawerActivity {
    public Toolbar toolbar;

    public BottomBar bottomBar;
    public LinearLayout linearLayoutContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.drawer_frame_container);
        View contentView = inflater.inflate(R.layout.activity_drawer_with_bottom_bar, frameLayout, false);
        drawerLayout.addView(contentView, 0);

        toolbar = (Toolbar) findViewById (R.id.toolbar_bottom_bar);
        setSupportActionBar (toolbar);

        linearLayoutContainer = (LinearLayout) findViewById(R.id.drawer_bottom_bar_layout);

        bottomBar = BottomBar.attach(findViewById(R.id.drawer_container_layout), savedInstanceState);
        bottomBar.setMaxFixedTabs(2);
//        bottomBar.useFixedMode();
        bottomBar.setItems(R.menu.bottom_bar);
//        bottomBar.setDefaultTabPosition(1);

        //0 -> News Feed
        //1 -> Attendance
        //2 -> profile
        bottomBar.mapColorForTab(0, "#FF5252");
        bottomBar.mapColorForTab(1, "#FF9800");
        bottomBar.mapColorForTab(2, "#7B1FA2");
//        bottomBar.setOnMenuTabClickListener(new OnMenuTabClickListener() {
//            @Override
//            public void onMenuTabSelected(@IdRes int menuItemId) {
//                if (menuItemId == R.id.bottomBar_feed) {
//                    startFragment(new Feed(), "feed");
//
//                } else if (menuItemId == R.id.bottomBar_profile) {
//
//
//                } else if (menuItemId == R.id.bottomBar_attendance) {
//
//
//                }
//
//            }
//
//            @Override
//            public void onMenuTabReSelected(@IdRes int menuItemId) {
//                if (menuItemId == R.id.bottomBar_feed) {
//                    startFragment(new Feed(), "feed");
//                }
//            }
//        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        super.onSaveInstanceState(outState, outPersistentState);
        bottomBar.onSaveInstanceState(outState);
    }

//    private void startFragment (Fragment fragment, String tag) {
//        FragmentManager fragmentManager = this.getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.dashboard_fragment_container, fragment, tag);
//        fragmentTransaction.addToBackStack(null);
//        fragmentTransaction.commit();
//    }
}
