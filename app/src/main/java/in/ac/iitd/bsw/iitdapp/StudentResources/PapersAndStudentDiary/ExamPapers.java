//package in.ac.iitd.bsw.iitdapp.StudentResources.PapersAndStudentDiary;
//
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.v4.app.ActivityCompat;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.DefaultItemAnimator;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.View;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import in.ac.iitd.bsw.iitdapp.R;
//
//public class ExamPapers extends AppCompatActivity {
//    private RecyclerView paperListRecyclerView;
//    private SubjectPapersAdapter papersAdapter;
//    private List<SubjectPapers> papersList = new ArrayList<>();
//    private RequestQueue requestQueue;
//    private static final String URL = "http://bsw.iitd.ac.in/app/default.php";
//    private static final String TAG = "response";
//    private String subjectName;
//    private DownloadPDFFromURL downloadPDF;
//
//
//    // TODO: 6/25/16 -> Edit 'Unsorted' section APIs
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.papers_list);
//
//        paperListRecyclerView = (RecyclerView) findViewById(R.id.papers_list_recyclerView);
//        papersAdapter = new SubjectPapersAdapter(papersList, getApplicationContext());
//
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
//        paperListRecyclerView.setLayoutManager(layoutManager);
//        paperListRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        paperListRecyclerView.setAdapter(papersAdapter);
//
//        requestQueue = Volley.newRequestQueue(this);
//
//        Intent i = getIntent();
//        subjectName = i.getStringExtra("subjectName");
//
////        if (isStoragePermissionGranted()) {
////            download = new DownloadPDFFromURL(this, "http://bsw.iitd.ac.in/QuestionPapers/AM/AML110_Major_2006-07_Sem2.pdf",
////                    "AML110_Major_2006-07_Sem2" + ".pdf", "Question-ExamPapers/" + "AM/");
////            download.download();
////        }
//
//        prepareSubjectsList();
//
//        papersAdapter.setOnItemClickListener(new SubjectPapersAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                Log.i(TAG, papersList.get(position).getPaperName());
//                Log.i(TAG, papersList.get(position).getPaperURL());
//
//                if (isStoragePermissionGranted()) {
//                    downloadPDF = new DownloadPDFFromURL(getApplication(), papersList.get(position).getPaperURL(),
//                            papersList.get(position).getPaperName() + ".pdf", "Question-Papers/" + subjectName + "/"
//                            , paperListRecyclerView);
//
//                    downloadPDF.download();
//                }
//
//            }
//        });
//
//    }
//
//    private void prepareSubjectsList() {
//        HashMap<String, String > map = new HashMap<>();
//        map.put("type", "qbank");
//        map.put("subject", subjectName);
//
//        sendJSONRequest(map, URL);
//    }
//
//    public boolean isStoragePermissionGranted() {
//        if (Build.VERSION.SDK_INT >= 23) {
//            if (this.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    == PackageManager.PERMISSION_GRANTED) {
//                Log.v(TAG, "Permission is granted");
//                return true;
//            }
//            else {
//
//                Log.v(TAG, "Permission is revoked");
//                //ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//                return false;
//            }
//        } else { //permission is automatically granted on sdk<23 upon installation
//            Log.v(TAG, "Permission is granted");
//            return true;
//        }
//
//
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
//            //resume tasks needing this permission
//        }
//    }
//
//    @Override
//    protected void onNewIntent(Intent intent) {
//        Bundle extras = intent.getExtras();
//        if (extras != null) {
//            if (extras.containsKey("test")) {
//                Log.i(TAG, extras.getString("test"));
//
//                if (extras.getString("test").equals("download-complete")) {
//
//                    downloadPDF.view(extras.getString("file_dir"));
//                }
//            }
//        }
//    }
//
//    private void sendJSONRequest(final HashMap<String, String> hashMap, String url){
//        StringRequest stringRequest = new StringRequest
//                (Request.Method.POST, url, new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String  response) {
//                        try {
//                            parseJSON(response.trim());
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        error.printStackTrace();
//                    }
//                }){
//            @Override
//            public String getBodyContentType() {
//                return "application/x-www-form-urlencoded; charset=UTF-8";
//            }
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                return hashMap;
//            }
//        };
//
//        requestQueue.add(stringRequest);
//    }
//
//    private void parseJSON(String request) throws JSONException {
//        JSONObject object = new JSONObject(request);
//        JSONArray array = object.getJSONArray("qpapers");
//
//        for (int i = 0; i < array.length(); i ++){
//            JSONObject paperObject = array.getJSONObject(i);
//            String paperName = paperObject.getString("name");
//            String paperURL = paperObject.getString("url");
//            SubjectPapers paper = new SubjectPapers(paperName, paperURL);
//            Log.i(TAG, paperName);
//            Log.i(TAG, paperURL);
//            papersList.add(paper);
//        }
//        papersAdapter.notifyDataSetChanged();
//    }
//
//}
