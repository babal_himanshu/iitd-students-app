package in.ac.iitd.bsw.iitdapp.StudentResources.Directory;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.StudentResources.ResourcesActivity;

public class DirectoryFragment extends Fragment {
    private static final String URL = "http://bsw.iitd.ac.in/app/default.php";
    private static final String TAG = "@@-directory";
    private DirectoryAdapter directoryAdapter;
    private List<DirectoryObject> directoryObjectList = new ArrayList<>();
    private RequestQueue requestQueue;
    private LinearLayout linearLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.directory_list, container, false);

        //Getting reference to ResourceActivity's objects (To save Android Resources, we re-use items)
        requestQueue = ((ResourcesActivity) getActivity()).requestQueue;
        linearLayout = ((ResourcesActivity)getActivity()).linearLayout;
        RecyclerView directoryListRecyclerView = ((ResourcesActivity) getActivity()).resourcesRecyclerView;
        //RecyclerView.LayoutManager layoutManager = ((ResourcesActivity) getActivity()).layoutManager; l
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());

        directoryAdapter = new DirectoryAdapter(directoryObjectList, getActivity());
        directoryListRecyclerView.setLayoutManager(layoutManager);
        directoryListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        directoryListRecyclerView.setAdapter(directoryAdapter);

        prepareDirectory();
        ((ResourcesActivity) getActivity()).isCallPermissionGranted();

        return view;
    }

    //See Fragment Lifecycle for more details
    //On Destroy View is called when the fragment is stopped.
    /*
    We use this method here because suppose we make a transition to next Fragment and then
    come back to this fragment, onCreateView will be called again the and List containing
    the objects to be displayed will be populated with the same data, twice.
    So, before we Create the Fragment Again, we empty the list to prevent reproduction
    of the data.
     */
    @Override
    public void onDestroyView() {
        Log.i(TAG, "fragment-onDestroyView");
        directoryObjectList.clear();
        directoryAdapter.notifyDataSetChanged();
        super.onDestroyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void prepareDirectory() {
        StringRequest stringRequest = new StringRequest
                (Request.Method.POST, URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            parseJSON(response.trim());
                            //Log.i(TAG, response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("type", "directory");
                return map;
            }
        };

        requestQueue.add(stringRequest);
    }

    private void parseJSON(String request) throws JSONException {
        JSONObject object = new JSONObject(request);
        JSONArray directory_array = object.getJSONArray("directory");

        JSONObject impNumbers = directory_array.getJSONObject(0);
        JSONObject services = directory_array.getJSONObject(2);

        JSONArray impNumArray = impNumbers.getJSONArray("nums");
        JSONArray servicesArray = services.getJSONArray("nums");

        for (int i = 0; i < impNumArray.length(); i++) {
            JSONObject jObject = impNumArray.getJSONObject(i);
            String dirName = jObject.getString("name");
            String dirNo = jObject.getString("no");
            //removing '-' and ' ' from contact number
            dirNo = dirNo.replace(" ", "");
            dirNo = dirNo.replace("-", "");
            if (dirNo.contains("/")) {
                int index = dirNo.indexOf("/");
                String dirNo1 = dirNo.substring(0, index);
                String dirNo2 = dirNo.substring(0, index - 4) + dirNo.substring(index + 1);
                dirNo = dirNo1;
                String dirName2 = dirName;
                dirName = dirName + " - 2";

                directoryObjectList.add(new DirectoryObject(dirName2, dirNo2));
            }

            DirectoryObject directoryObject = new DirectoryObject(dirName, dirNo);
            directoryObjectList.add(directoryObject);
        }
        directoryAdapter.notifyDataSetChanged();

        for (int i = 0; i < servicesArray.length(); i++) {
            JSONObject jObject = servicesArray.getJSONObject(i);
            String dirName = jObject.getString("name");
            String dirNo = jObject.getString("no");
            dirNo = dirNo.replace(" ", "");
            dirNo = dirNo.replace("-", "");
            if (dirNo.contains("/")) {
                int index = dirNo.indexOf("/");
                String dirNo1 = dirNo.substring(0, index);
                String dirNo2 = dirNo.substring(0, index - 4) + dirNo.substring(index + 1);
                dirNo = dirNo1;
                String dirName2 = dirName;
                dirName = dirName + " - 2";

                directoryObjectList.add(new DirectoryObject(dirName2, dirNo2));
            }

            DirectoryObject directoryObject = new DirectoryObject(dirName, dirNo);
            directoryObjectList.add(directoryObject);
        }
        directoryAdapter.notifyDataSetChanged();

    }
}
