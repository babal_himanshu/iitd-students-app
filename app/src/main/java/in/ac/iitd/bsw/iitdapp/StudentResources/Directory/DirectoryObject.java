package in.ac.iitd.bsw.iitdapp.StudentResources.Directory;

/*
And Object is required of type of the data you want to store
For more info, visit links given in DirectoryAdapter.java
 */
public class DirectoryObject {
    private String name;
    private String number;

    public DirectoryObject (String name, String number){
        this.name = name;
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
