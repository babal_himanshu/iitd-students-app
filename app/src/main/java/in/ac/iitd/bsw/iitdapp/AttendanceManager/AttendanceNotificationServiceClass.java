package in.ac.iitd.bsw.iitdapp.AttendanceManager;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

public class AttendanceNotificationServiceClass extends IntentService {
    public AttendanceNotificationServiceClass(String name) {
        super(name);
    }
    public AttendanceNotificationServiceClass () {
        super("AttendanceNotificationServiceClass");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i("time", "onHandleEvent");
        if (intent.getAction().equals("action")) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                if (bundle.containsKey("class")) {
                    String course_code = bundle.getString("courseCode");
                    AttendanceAlarmReciever.manager.cancel(0);
                    if (bundle.getString("class").equals("attended")){
                        AttendanceAddDates.addClassAttended(course_code, "09/07/2016");

                    } else if (bundle.getString("class").equals("bunked")) {
                        AttendanceAddDates.addClassBunked(course_code, "09/07/2016");

                    } else if (bundle.getString("class").equals("off")) {
                        AttendanceAddDates.addClassCancelled(course_code, "09/07/2016");

                    } else {

                    }
                    Log.i("test", bundle.getString("class"));
                }
            }
        }
    }

    public void showToast(String message) {
        final String msg = message;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
