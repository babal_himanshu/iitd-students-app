//package in.ac.iitd.bsw.iitdapp.AttendanceManager;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.view.View;
//import android.widget.ArrayAdapter;
//import android.widget.AutoCompleteTextView;
//import android.widget.Button;
//import android.widget.TextView;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.List;
//
//import in.ac.iitd.bsw.iitdapp.R;
//import in.ac.iitd.bsw.iitdapp.AttendanceManager.TimeTable.CourseTimeTableObject;
//import in.ac.iitd.bsw.iitdapp.AttendanceManager.TimeTable.TimeTable;
//
//public class AttendanceManager extends AppCompatActivity{
//    private List<CourseTimeTableObject> coursesList = new ArrayList<>();
//
//    private AutoCompleteTextView autoCompleteTextView;
//    private Button button, addClassButton, bunkClassButton, offClassButton, attendClassButton;
//    private TextView textView, addClassTextView, bunkClassTextview, offClassTextView, attendClassTextView;
//    private ArrayAdapter<String> arrayAdapter;
//    private AttendanceDBHelper attendanceDBHelper;
//    private long _id;
//    private CourseTimeTableObject object;
//    String date;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.attendance_manager_activity);
//
//        //TimeTableDBHelper timeTableDBHelper = new TimeTableDBHelper(this);
//        //coursesList = timeTableDBHelper.getAllCourses();
//        //TimeTableDBHelper timeTableDBHelper = new TimeTableDBHelper(this);
//        //coursesList =  timeTableDBHelper.getAllCourses();
//        coursesList = TimeTable.courseObjectsList;
//
////        for (CourseTimeTableObject object : coursesList) {
////            Log.i("debug", String.valueOf(object.get_id()));
////            Log.i("debug", object.getCourse_name());
////            Log.i("debug", object.getCourse_no());
////            Log.i("debug", object.getDay());
////            Log.i("debug", object.getEnd_time());
////            Log.i("debug", object.getStart_time());
////            Log.i("debug", object.getUser_id());
////            Log.i("debug", object.getUser_name());
////            Log.i("debug", " ");
////        }
//
//        attendanceDBHelper = new AttendanceDBHelper(this);
//
//        button = (Button) findViewById(R.id.attandance_manager_button);
//        addClassButton = (Button) findViewById(R.id.attandance_manager_addClass_button);
//        bunkClassButton = (Button) findViewById(R.id.attandance_manager_bunkClass_button);
//        offClassButton = (Button) findViewById(R.id.attandance_manager_classOff_button);
//        attendClassButton = (Button) findViewById(R.id.attandance_manager_attendClass_button);
//
//        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.attandance_manager_autoComplete);
//        textView = (TextView) findViewById(R.id.attandance_manager_textView);
//        addClassTextView = (TextView) findViewById(R.id.attandance_manager_addClass_textView);
//        bunkClassTextview = (TextView) findViewById(R.id.attandance_manager_bunkClass_textView);
//        offClassTextView = (TextView) findViewById(R.id.attandance_manager_classOff_textView);
//        attendClassTextView = (TextView) findViewById(R.id.attandance_manager_attendClass_textView);
//
//        String[] array = new String[coursesList.size()];
//        attendanceDBHelper.onUpdate();
//        for (int i = 0; i < coursesList.size(); i ++){
//           CourseTimeTableObject object = coursesList.get(i);
//           attendanceDBHelper.insertClass(object.getCourse_no(), object.getRoom_no(), object.getDay(),
//                   object.getStart_time(), object.getEnd_time(), object.getUser_name(), object.getUser_id(),
//                   75, object.getCourse_name());
//        }
//
//        for (int i = 0; i < coursesList.size(); i ++){
//            array[i] = coursesList.get(i).getCourse_no();
//        }
//        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, array);
//        autoCompleteTextView.setAdapter(arrayAdapter);
//
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String course_no = autoCompleteTextView.getText().toString();
//                CourseTimeTableObject object = attendanceDBHelper.findClassRow(course_no);
//
//                textView.setText(object.getCourse_no() + object.getDay() +  object.getRoom_no() + " " + object.get_id());
//                Log.i("object-id", String.valueOf(object.get_id()));
//                Log.i("object-id", String.valueOf(attendanceDBHelper.findClassID(course_no)));
//                _id = attendanceDBHelper.findClassID(autoCompleteTextView.getText().toString());
//                object = attendanceDBHelper.findClassRow(autoCompleteTextView.getText().toString());
//            }
//        });
//
//        Calendar c = Calendar.getInstance();
//        System.out.println("Current time => " + c.getTime());
//
//        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
//        String formattedDate = df.format(c.getTime());
//
//        date = formattedDate;
//
//
//        Log.i("date", date);
//
//
//
//
//    }
//}
