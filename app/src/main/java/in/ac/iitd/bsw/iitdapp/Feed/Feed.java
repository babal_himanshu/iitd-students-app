package in.ac.iitd.bsw.iitdapp.Feed;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.ac.iitd.bsw.iitdapp.FirstTimeSetup.ClubSelectSetup;
import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.Startup.SignIn.LoginFragment;

public class Feed extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.feed_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.feed_swipe_refresh)
    SwipeRefreshLayout refreshLayout;

    private static final String URL = "https://hostelcomp.iitd.ac.in/notif_retrieve.php";

    private FeedAdapter adapter;
    private List<FeedObject> list = new ArrayList<>();
    private RequestQueue requestQueue;
    private LinearLayoutManager layoutManager;
    private SharedPreferences preferences;
    private String selectedClubs;

    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    int pageCount = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feed_fragment, container, false);
        ButterKnife.bind(this, view);
        refreshLayout.setOnRefreshListener(this);
        setRetainInstance(true);
        preferences = this.getActivity().getSharedPreferences(LoginFragment.LOGIN_SHARED_PREF, Context.MODE_PRIVATE);
        selectedClubs = preferences.getString(ClubSelectSetup.CLUB_LIST_PREF, "");

        adapter = new FeedAdapter(list, getContext());
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        requestQueue = Volley.newRequestQueue(getContext());

        prepareList(1);

        recyclerView.addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    pageCount += 1;
                    prepareList(pageCount);

                    loading = true;
                }
            }
        });

        return view;
    }

    private void prepareList(int pageC){
        final int pageCounter;
        if (pageC == -1) {
            if (!list.isEmpty()) {
                list.clear();
            }
            pageCounter = 1;
        } else {
            pageCounter = pageC;
        }

        String url = URL + "?page_number=" + pageCounter;
        Log.i("feed", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.i("feed", response);
                            parseJSON(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        requestQueue.add(stringRequest);
    }

    private void parseJSON(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        String success = jsonObject.getString("success");
        if (success.equals("True")) {
            JSONArray array = jsonObject.getJSONArray("notifs");
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                String timeStamp = obj.getString("time_stamp");
                String clubid = obj.getString("user_id");
                String clubName = obj.getString("user_name");
                String postTitle = obj.getString("topic");
                String postDesc = obj.getString("desc");
                Boolean hasPic = obj.getBoolean("hasPic");
                Log.i("feed", String.valueOf(obj.get("width_height")));
                float imageRatio = Float.parseFloat(obj.getString("width_height"));

                Log.i("feed", selectedClubs);
                Log.i("feed", clubid);

//                if (selectedClubs.contains(clubid)) {
//
//                    FeedObject feedObj = new FeedObject(R.drawable.pfc_iitd, timeStamp, clubName, postTitle, postDesc, clubid);
//                    list.add(feedObj);
//                }

                FeedObject feedObj = new FeedObject(R.drawable.pfc_iitd, timeStamp, clubName, postTitle, postDesc, clubid, hasPic, imageRatio);
                list.add(feedObj);

            }

            Log.i("feed", String.valueOf(list.size()));
            adapter.notifyDataSetChanged();
            refreshLayout.setRefreshing(false);
        }
    }

//    @Override
//    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
//        super.onViewStateRestored(savedInstanceState);
//        Log.i("feed", "onViewRestored");
//         SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(FEED_PREF, 0);
//         SharedPreferences.Editor editor = sharedPreferences.edit();
//        long currentVisiblePosition = 0;
//        currentVisiblePosition = sharedPreferences.getLong("position", 0);
//        layoutManager.scrollToPosition((int) currentVisiblePosition);
//        Log.i("feed", String.valueOf(currentVisiblePosition));
//        editor.clear();
//
////        if(savedInstanceState != null)
////        {
////            Parcelable savedRecyclerLayoutState = savedInstanceState.getParcelable(BUNDLE_RECYCLER_LAYOUT);
////            recyclerView.getLayoutManager().onRestoreInstanceState(savedRecyclerLayoutState);
////        }
//
//    }
//
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        Log.i("feed", "onSaveInstState");
////        outState.putParcelable(BUNDLE_RECYCLER_LAYOUT, recyclerView.getLayoutManager().onSaveInstanceState());
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        Log.i("feed", "onPause");
//        long currentVisiblePosition = 0;
//         SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(FEED_PREF, 0);
//         SharedPreferences.Editor editor = sharedPreferences.edit();
//        currentVisiblePosition = ((LinearLayoutManager)recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
//        editor.putLong("position", currentVisiblePosition);
//        Log.i("feed", String.valueOf(currentVisiblePosition));
//        editor.commit();
//    }

    @Override
    public void onRefresh() {
        prepareList(-1);
    }
}
