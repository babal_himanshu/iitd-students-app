package in.ac.iitd.bsw.iitdapp.Profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import in.ac.iitd.bsw.iitdapp.BaseActivity;
import in.ac.iitd.bsw.iitdapp.Dashboard.DashBoardActivity;
import in.ac.iitd.bsw.iitdapp.FirstTimeSetup.ClubListAdapter;
import in.ac.iitd.bsw.iitdapp.FirstTimeSetup.ClubObject;
import in.ac.iitd.bsw.iitdapp.FirstTimeSetup.ClubSelectSetup;
import in.ac.iitd.bsw.iitdapp.FirstTimeSetup.FirstSetupActivity;
import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.Startup.SignIn.LoginFragment;
import in.ac.iitd.bsw.iitdapp.Utility.DBUtility;
import in.ac.iitd.bsw.iitdapp.Utility.GeneralUtility;

public class EditSubscriptions extends Fragment {
    private RecyclerView recyclerView;
    private ClubListAdapter adapter;
    private List<ClubObject> clubsList = new ArrayList<>();
    private List<String> selectedClubs = new ArrayList<>();
    private RequestQueue requestQueue;
    private FloatingActionButton nextFab;

    private static final String URL_CLUB_LIST = BaseActivity.BASE_URL + "clubs.php";

    private SharedPreferences preferences;
    private String provider;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setup_club_select_list, container, false);

        nextFab = FirstSetupActivity.nextFAB;
        nextFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextFabClick();
            }
        });

        preferences = this.getActivity().getSharedPreferences(LoginFragment.LOGIN_SHARED_PREF, Context.MODE_PRIVATE);
        selectedClubs = Arrays.asList(DBUtility.convertStringToArray(preferences.getString(ClubSelectSetup.CLUB_LIST_PREF, "def")));
        requestQueue = Volley.newRequestQueue(getContext());

        recyclerView = (RecyclerView) view.findViewById(R.id.setup_club_select_recyclerView);
        Log.i("club-1", String.valueOf(selectedClubs));
        adapter = new ClubListAdapter(clubsList, getContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new ClubListAdapter.OnItemClickListener() {
            @Override
            public void onSelectButtonClick(View view, int position, Boolean isSelected) {
                ImageButton selectButton = (ImageButton) view;
                if (! isSelected){
                    selectButton.setImageResource(R.drawable.ic_clear_black_24dp);
                    Toast.makeText(getContext(), "Subscribed to " + clubsList.get(position).getName(), Toast.LENGTH_SHORT).show();
//                    selectButton.setBackgroundColor(Color.parseColor("#FF8000"));
//                    selectButton.setText("SELECT");
//                    selectButton.setTextColor(Color.parseColor("#000000"));
                    clubsList.get(position).setSelected(true);
                    adapter.notifyDataSetChanged();

                } else {
                    selectButton.setImageResource(R.drawable.ic_done_black_24dp);
                    Toast.makeText(getContext(), "Unsubscribed from " + clubsList.get(position).getName(), Toast.LENGTH_SHORT).show();
//                    selectButton.setBackgroundColor(Color.parseColor("#00ffffff"));
//                    selectButton.setText("SELECTED");
//                    selectButton.setTextColor(Color.parseColor("#FFFFFF"));
                    clubsList.get(position).setSelected(false);
                    adapter.notifyDataSetChanged();
                }
            }
        });

        populateList();

        return view;
    }

    private void nextFabClick(){

        int count = 0;
        ArrayList<String> clubList = new ArrayList<>();
        for (ClubObject object : clubsList) {
            if (object.getSelected()) {
                Log.i("club", object.getCode());
                clubList.add(object.getCode());
                count ++;
            }
        }
        String[] clubArray = new String[count];
        for (int i = 0; i < count; i++) {
            clubArray[i] = clubList.get(i);
        }

        subscribeFirebaseTopics(clubList);

        String clubString = DBUtility.convertArrayToString(clubArray);
        Log.i("club", clubString);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("Subs", clubString);

        String url = "";
        provider = ((FirstSetupActivity)getActivity()).provider;
        if (! provider.equals("")) {
            if (provider.equals("kerberos")) {
                url = BaseActivity.BASE_URL + "setSubscriptions_k.php";
                hashMap.put("Kerebros_uid", preferences.getString(LoginFragment.KERBEROS_USERID, ""));
            } else if (provider.equals("google")) {
                url = BaseActivity.BASE_URL + "setSubscriptions_g.php";
                hashMap.put("Google_uid", preferences.getString(LoginFragment.GOOGLE_ID, ""));
            }
        }

        requestQueue.add(GeneralUtility.sendSimplePostRequest(url, hashMap));
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ClubSelectSetup.CLUB_LIST_PREF, clubString);
        editor.apply();

        Intent intent = new Intent(getActivity(), DashBoardActivity.class);
        startActivity(intent);

    }

    private void subscribeFirebaseTopics(ArrayList<String> clubList){
        for (ClubObject object : clubsList) {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(object.getCode());
        }
        for (String code : clubList) {
            FirebaseMessaging.getInstance().subscribeToTopic(code);
        }
    }

    private void populateList() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_CLUB_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            parseJSON(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
        requestQueue.add(stringRequest);
    }

    private void parseJSON(String response) throws JSONException {
        Bitmap icon = BitmapFactory.decodeResource(getContext().getResources(),
                R.drawable.pfc_iitd);

        JSONObject object = new JSONObject(response);
        if (object.getInt("success") > 0) {
            JSONArray array = object.getJSONArray("result");
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                String clubName = obj.getString("name");
                String clubCode = obj.getString("code");
                ClubObject clubObject = new ClubObject(icon, clubCode, clubName);
                if (selectedClubs.contains(clubCode)){
                    clubObject.setSelected(true);
                }
                clubsList.add(clubObject);
            }
            adapter.notifyDataSetChanged();
        }
    }
}
