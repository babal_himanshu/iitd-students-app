package in.ac.iitd.bsw.iitdapp.StudentResources.PapersAndStudentDiary;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.StudentResources.ResourcesActivity;

public class QuestionBankFragment extends Fragment {
    private RecyclerView subjectListRecyclerView;
    private SubjectsAdapter subjectsAdapter;
    private List<Subjects> subjectsList = new ArrayList<>();
    private RequestQueue requestQueue;
    public static final String URL = "http://bsw.iitd.ac.in/app/default.php";
    public static final String TAG = "@@-question-bank";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.subjects_list, container, false);

        requestQueue = ((ResourcesActivity)getActivity()).requestQueue;
        subjectListRecyclerView = ((ResourcesActivity)getActivity()).resourcesRecyclerView;
        //RecyclerView.LayoutManager layoutManager = ((ResourcesActivity)getActivity()).layoutManager;
        //subjectListRecyclerView = (RecyclerView) view.findViewById(R.id.subject_list_recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());

        subjectsAdapter = new SubjectsAdapter(subjectsList, getActivity());
        subjectListRecyclerView.setLayoutManager(layoutManager);
        subjectListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        subjectListRecyclerView.setAdapter(subjectsAdapter);

        prepareSubjectsList();

        subjectsAdapter.setOnItemClickListener(new SubjectsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putString("subjectName", subjectsList.get(position).getSubjectName());

                ExamPapersFragment examPapersFragment = new ExamPapersFragment();
                examPapersFragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.activity_resource_frame, examPapersFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                //int itemPosition = getAdapterPosition();
                //Intent i = new Intent(context, ExamPapers.class);
                //i.putExtra("subjectName", subjects.get(itemPosition).getSubjectName());
                //itemView.getContext().startActivity(i);
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        Log.i(TAG, "fragment-onDestroyView");
        subjectsList.clear();
        subjectsAdapter.notifyDataSetChanged();
        super.onDestroyView();
    }


//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.subjects_list);
//
//        subjectListRecyclerView = (RecyclerView) findViewById(R.id.subject_list_recyclerView);
//        subjectsAdapter = new SubjectsAdapter(subjectsList, getApplicationContext());
//
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
//        subjectListRecyclerView.setLayoutManager(layoutManager);
//        subjectListRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        subjectListRecyclerView.setAdapter(subjectsAdapter);
//
//        requestQueue = Volley.newRequestQueue(this);
//
//        if (isStoragePermissionGranted()) {
//            download = new DownloadPDFFromURL(this, "http://bsw.iitd.ac.in/QuestionPapers/AM/AML110_Major_2006-07_Sem2.pdf",
//                    "AML110_Major_2006-07_Sem2" + ".pdf", "Question-ExamPapers/" + "AM/");
//            download.download();
//        }
//
//        prepareSubjectsList();
//    }

    private void prepareSubjectsList() {
        HashMap<String, String > map = new HashMap<>();
        map.put("type", "qbank");
        map.put("subject", "list_subjects");

        sendJSONRequest(map, URL);
    }

//    public boolean isStoragePermissionGranted() {
//        if (Build.VERSION.SDK_INT >= 23) {
//            if (this.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    == PackageManager.PERMISSION_GRANTED) {
//                Log.v(TAG, "Permission is granted");
//                return true;
//            }
//            else {
//
//                Log.v(TAG, "Permission is revoked");
//                //ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//                return false;
//            }
//        } else { //permission is automatically granted on sdk<23 upon installation
//            Log.v(TAG, "Permission is granted");
//            return true;
//        }
//
//
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
//            //resume tasks needing this permission
//        }
//    }
//
//    @Override
//    protected void onNewIntent(Intent intent) {
//        Bundle extras = intent.getExtras();
//        if (extras != null) {
//            if (extras.containsKey("test")) {
//                Log.i(TAG, extras.getString("test"));
//
//                if (extras.getString("test").equals("download-complete")) {
//
//                    download.view(extras.getString("file_dir"));
//                }
//            }
//        }
//    }

    private void sendJSONRequest(final HashMap<String, String> hashMap, String url){
        StringRequest stringRequest = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String  response) {
                        try {
                            parseJSON(response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return hashMap;
            }
        };

        requestQueue.add(stringRequest);
    }

    private void parseJSON(String request) throws JSONException {
        JSONObject object = new JSONObject(request);
        JSONArray array = object.getJSONArray("subjects");

        for (int i = 0; i < array.length(); i ++){
            String name = array.getString(i);
            Subjects subject = new Subjects(name);
            //Log.i(TAG, name);
            subjectsList.add(subject);
        }
        subjectsAdapter.notifyDataSetChanged();
    }
}
