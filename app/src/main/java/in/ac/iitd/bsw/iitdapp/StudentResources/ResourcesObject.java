package in.ac.iitd.bsw.iitdapp.StudentResources;

public class ResourcesObject {
    private String name;

    public ResourcesObject (String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
