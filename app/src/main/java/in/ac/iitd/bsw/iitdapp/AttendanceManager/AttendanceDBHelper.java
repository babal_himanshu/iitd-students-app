package in.ac.iitd.bsw.iitdapp.AttendanceManager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import in.ac.iitd.bsw.iitdapp.AttendanceManager.TimeTable.CourseTimeTableObject;
import in.ac.iitd.bsw.iitdapp.Utility.DBUtility;

//todo -> call cursor.close();

public class AttendanceDBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "attendance.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TEXT_TYPE = " TEXT ";
    private static final String COMMA_SEP = " , ";
    private static final String NOT_NULL = " NOT NULL ";
    private static final String TYPE_INT = " INTEGER ";
    private static final String DEFAULT = " DEFAULT ";
    private static final String CLASS_HAPPENED = "B";
    private static final String CLASS_ATTENDED = "A";
    private static final String CLASS_CANCELLED = "C";

    //class_status
    //A -> Attended
    //B -> Bunked (By Default)
    //C -> Cancelled
    private static final String CREATE_ENTRIES =
            "CREATE TABLE " + AttendanceDBContract.AttendanceDBEntry.TABLE_NAME + " (" +
                    AttendanceDBContract.AttendanceDBEntry._ID + " INTEGER PRIMARY KEY," +
                    AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_COURSE_NO + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_ROOM_NO + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_DAY + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_START_TIME + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_END_TIME + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_FACULTY + TEXT_TYPE + COMMA_SEP +
                    AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_USER_ID + TEXT_TYPE + COMMA_SEP +
                    AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_ATTENDANCE_CRITERIA + TYPE_INT + DEFAULT + 0 + COMMA_SEP +
                    AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_CLASS_STATUS + TEXT_TYPE + NOT_NULL + DEFAULT + CLASS_HAPPENED + COMMA_SEP +
                    AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_CLASS_TOTAL + TYPE_INT + DEFAULT + 0 + COMMA_SEP +
                    AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_CLASS_TOTAL_DATE + TEXT_TYPE + COMMA_SEP +
                    AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_COURSE_NAME + TEXT_TYPE + NOT_NULL +
                    " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + AttendanceDBContract.AttendanceDBEntry.TABLE_NAME;


    public AttendanceDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onUpdate() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public boolean insertClass(String course_no, String room_no, String day, String start_time,
                               String end_time, String faculty, String user_id, int attendance_criteria,
                               String course_name) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_ATTENDANCE_CRITERIA, attendance_criteria);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_USER_ID, user_id);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_START_TIME, start_time);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_ROOM_NO, room_no);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_FACULTY, faculty);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_END_TIME, end_time);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_COURSE_NO, course_no);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_DAY, day);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_COURSE_NAME, course_name);

        long newRowID;
        newRowID = database.insert(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME, null, contentValues);

        return (newRowID != -1);
    }

    public boolean insertCourse(CourseTimeTableObject object) {
        String course_no = object.getCourse_no();
        String room_no = object.getRoom_no();
        String day = object.getDay();
        String start_time = object.getStart_time();
        String end_time = object.getEnd_time();
        String faculty = object.getUser_name();
        String user_id = object.getUser_id();
        int attendance_criteria = object.getAttendance_criteria();
        String course_name = object.getCourse_name();

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_ATTENDANCE_CRITERIA, attendance_criteria);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_USER_ID, user_id);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_START_TIME, start_time);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_ROOM_NO, room_no);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_FACULTY, faculty);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_END_TIME, end_time);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_COURSE_NO, course_no);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_DAY, day);
        contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_COURSE_NAME, course_name);

        long newRowID;
        newRowID = database.insert(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME, null, contentValues);

        return (newRowID != -1);
    }

    public void printTableData(){
        SQLiteDatabase db = getReadableDatabase();

        Cursor cur = db.rawQuery("SELECT * FROM " + AttendanceDBContract.AttendanceDBEntry.TABLE_NAME, null);

        String[] arr = cur.getColumnNames();

        String rows = "";

        for(int i = 0 ; i < arr.length; i++){
            rows = rows + " || " + arr[i];
        }

        Log.d("alldata", rows);

        if(cur.getCount() != 0){
            cur.moveToFirst();

            do{
                String row_values = "";

                for(int i = 0 ; i < cur.getColumnCount(); i++){
                    row_values = row_values + " || " + cur.getString(i);
                }

                Log.d("alldata", row_values);

            }while (cur.moveToNext());
        }
    }

    public AttendanceDBClass getDayWiseDetails (String course_no){
        CourseTimeTableObject object = findClassRow(course_no);
        String[] days = object.getDay().split(",");
        String[] room = object.getRoom_no().split(",");
        String[] start_time = object.getStart_time().split(",");
        String[] end_time = object.getEnd_time().split(",");
        String course_code = object.getCourse_no();

        DateFormat dateFormat = new SimpleDateFormat("hh:mm");

        List<String[]> dayWiseDetails = new ArrayList<>();
        List<Date[]> dayWiseTime = new ArrayList<>();

        for (int i = 0; i < days.length; i ++) {
            String[] aDay = new String[5];
            //it will contain start time and end time for each subject
            Date[] dates = new Date[2];

            aDay[0] = course_code;
            aDay[1] = days[i];
            aDay[2] = room[i];
            aDay[3] = start_time[i];
            aDay[4] = end_time[i];

            try {
                dates[0] = dateFormat.parse(start_time[i]);
                dates[1] = dateFormat.parse(end_time[i]);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            dayWiseDetails.add(aDay);
            dayWiseTime.add(dates);
        }
        AttendanceDBClass dbClass = new AttendanceDBClass(dayWiseDetails, dayWiseTime);

        return dbClass;
    }

    public List<AttendanceDBClass> getDayWiseDetailsForAllCourses (){
        List<AttendanceDBClass> classList = new ArrayList<>();
        List<CourseTimeTableObject> list = getAllCourses();

        for (CourseTimeTableObject object : list) {
            String[] days = object.getDay().split(",");
            String[] room = object.getRoom_no().split(",");
            String[] start_time = object.getStart_time().split(",");
            String[] end_time = object.getEnd_time().split(",");
            String course_code = object.getCourse_no();

            Log.i("date", String.valueOf(Arrays.asList(days)));
            Log.i("date", String.valueOf(Arrays.asList(room)));
            Log.i("date", String.valueOf(Arrays.asList(start_time)));
            Log.i("date", String.valueOf(Arrays.asList(end_time)));

            DateFormat dateFormat = new SimpleDateFormat("HH:mm");

            List<String[]> dayWiseDetails = new ArrayList<>();
            List<Date[]> dayWiseTime = new ArrayList<>();

            for (int i = 0; i < days.length; i++) {
                String[] aDay = new String[5];
                //it will contain start time and end time for each subject
                Date[] dates = new Date[2];

                aDay[0] = course_code;
                aDay[1] = days[i];
                aDay[2] = room[i];
                aDay[3] = start_time[i];
                aDay[4] = end_time[i];

                try {
                    dates[0] = dateFormat.parse(start_time[i]);
                    dates[1] = dateFormat.parse(end_time[i]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                dayWiseDetails.add(aDay);
                dayWiseTime.add(dates);
            }

            classList.add(new AttendanceDBClass(dayWiseDetails, dayWiseTime));
        }

        return classList;
    }

    public CourseTimeTableObject findClassRow (String course_no) {
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.query(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME,
                null,
                AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_COURSE_NO + " = " + "'" + course_no + "'",
                null, null, null, null);

        cursor.moveToFirst();
        CourseTimeTableObject object = cursorToCourseObject(cursor);
        cursor.close();
        return object;
    }

    public long findClassID (String course_no) {
        CourseTimeTableObject object = findClassRow(course_no);
        return object.get_id();
    }

    //Date format ->  DD/MM/YYYY
    public boolean addClassTotal (String course_no, String date) {
        long _id = findClassID(course_no);
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.query(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME,
                null, AttendanceDBContract.AttendanceDBEntry._ID + "=" + _id,
                null, null, null, null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();

            int classCount = cursor.getInt(10);
            String classDate = cursor.getString(11);
            String classStatus = cursor.getString(9);

            if (classDate != null) {
                String[] dates = DBUtility.convertStringToArray(classDate);
                String[] status = DBUtility.convertStringToArray(classStatus);
                if (! Arrays.asList(dates).contains(date)) {
                    classCount = dates.length + 1;
                    if (status.length != dates.length) {
                        String[] equalStatus = new String[dates.length];
                        for (int i = 0; i < status.length; i++) {
                            equalStatus[i] = status[i];
                        }
                        int count = dates.length - status.length;
                        for (int i = 0; i < count; i++) {
                            equalStatus[status.length + i] = CLASS_HAPPENED;
                        }
                        status = equalStatus;
                    }
                    String[] newDates = new String[dates.length + 1];
                    String[] newStatus = new String[dates.length + 1];
                    for (int i = 0; i < dates.length; i++) {
                        newDates[i] = dates[i];
                        newStatus[i] = status[i];
                    }
                    newDates[dates.length] = date;
                    newStatus[dates.length] = CLASS_HAPPENED;
                    classDate = DBUtility.convertArrayToString(newDates);
                    classStatus = DBUtility.convertArrayToString(newStatus);
//                    if (!classDate.contains(date)) {
//                        classDate = classDate + "," + date;
//                    }
                }
            } else {
                classCount = 1;
                classDate = date;
                classStatus = CLASS_HAPPENED;
            }

            ContentValues contentValues = new ContentValues();
            contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_CLASS_TOTAL, classCount);
            contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_CLASS_STATUS,  classStatus);
            contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_CLASS_TOTAL_DATE, classDate);

            int res = database.update(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME, contentValues,
                    AttendanceDBContract.AttendanceDBEntry._ID + " = " + _id, null);


            cursor.close();
            Cursor c = database.query(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME,
                    null, AttendanceDBContract.AttendanceDBEntry._ID + "=" + _id,
                    null, null, null, null);
            c.moveToFirst();
            Log.i("attendance", String.valueOf(c.getInt(10)));
            Log.i("attendance", String.valueOf(c.getString(11)));
            Log.i("attendance", String.valueOf(c.getString(9)));
            c.close();
            database.close();
            return res != -1;
        }
        else {
            return false;
        }
    }

    public boolean clearClassData (String course_no, String date) {
        long _id = findClassID(course_no);
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.query(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME,
                null, AttendanceDBContract.AttendanceDBEntry._ID + "=" + _id,
                null, null, null, null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();

            int classCount = cursor.getInt(10);
            String classDate = cursor.getString(11);
            String classStatus = cursor.getString(9);

            classCount = 1;
            classDate = date;
            classStatus = CLASS_HAPPENED;


            ContentValues contentValues = new ContentValues();
            contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_CLASS_TOTAL, classCount);
            contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_CLASS_STATUS,  classStatus);
            contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_CLASS_TOTAL_DATE, classDate);

            int res = database.update(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME, contentValues,
                    AttendanceDBContract.AttendanceDBEntry._ID + " = " + _id, null);


            cursor.close();
            Cursor c = database.query(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME,
                    null, AttendanceDBContract.AttendanceDBEntry._ID + "=" + _id,
                    null, null, null, null);
            c.moveToFirst();
            Log.i("attendance", String.valueOf(c.getInt(10)));
            Log.i("attendance", String.valueOf(c.getString(11)));
            Log.i("attendance", String.valueOf(c.getString(9)));
            c.close();
            database.close();
            return res != -1;
        }
        else {
            return false;
        }
    }

    public boolean addClassAttended (String  course_no, String date) {
        addClassTotal(course_no, date);
        long _id = findClassID(course_no);
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.query(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME,
                null, AttendanceDBContract.AttendanceDBEntry._ID + "=" + _id,
                null, null, null, null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();

            String classDate = cursor.getString(11);
            String classStatus = cursor.getString(9);
            String[] dates = DBUtility.convertStringToArray(classDate);
            String[] status = DBUtility.convertStringToArray(classStatus);

            int index = Arrays.asList(dates).indexOf(date);
            if (index != -1) {
                status[index] = CLASS_ATTENDED;
            }
            classStatus = DBUtility.convertArrayToString(status);

            ContentValues contentValues = new ContentValues();
            contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_CLASS_STATUS,  classStatus);

            int res = database.update(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME, contentValues,
                    AttendanceDBContract.AttendanceDBEntry._ID + " = " + _id, null);


            cursor.close();
            Cursor c = database.query(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME,
                    null, AttendanceDBContract.AttendanceDBEntry._ID + "=" + _id,
                    null, null, null, null);
            c.moveToFirst();
            Log.i("attendance", String.valueOf(c.getString(11)));
            Log.i("attendance", String.valueOf(c.getString(9)));
            c.close();
            database.close();
            return res != -1;
        }
        else {
            return false;
        }
    }

    public boolean addClassBunked (String  course_no, String date) {
        addClassTotal(course_no, date);
        long _id = findClassID(course_no);
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.query(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME,
                null, AttendanceDBContract.AttendanceDBEntry._ID + "=" + _id,
                null, null, null, null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();

            String classDate = cursor.getString(11);
            String classStatus = cursor.getString(9);
            String[] dates = DBUtility.convertStringToArray(classDate);
            String[] status = DBUtility.convertStringToArray(classStatus);

            int index = Arrays.asList(dates).indexOf(date);
            if (index != -1) {
                status[index] = CLASS_HAPPENED;
            }
            classStatus = DBUtility.convertArrayToString(status);

            ContentValues contentValues = new ContentValues();
            contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_CLASS_STATUS,  classStatus);

            int res = database.update(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME, contentValues,
                    AttendanceDBContract.AttendanceDBEntry._ID + " = " + _id, null);


            cursor.close();
            Cursor c = database.query(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME,
                    null, AttendanceDBContract.AttendanceDBEntry._ID + "=" + _id,
                    null, null, null, null);
            c.moveToFirst();
            Log.i("attendance", String.valueOf(c.getString(11)));
            Log.i("attendance", String.valueOf(c.getString(9)));
            c.close();
            database.close();
            return res != -1;
        }
        else {
            return false;
        }
    }

    public boolean addClassCancelled (String  course_no, String date) {
        addClassTotal(course_no, date);
        long _id = findClassID(course_no);
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.query(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME,
                null, AttendanceDBContract.AttendanceDBEntry._ID + "=" + _id,
                null, null, null, null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();

            String classDate = cursor.getString(11);
            String classStatus = cursor.getString(9);
            String[] dates = DBUtility.convertStringToArray(classDate);
            String[] status = DBUtility.convertStringToArray(classStatus);

            int index = Arrays.asList(dates).indexOf(date);
            if (index != -1) {
                status[index] = CLASS_CANCELLED;
            }
            classStatus = DBUtility.convertArrayToString(status);

            ContentValues contentValues = new ContentValues();
            contentValues.put(AttendanceDBContract.AttendanceDBEntry.COLUMN_NAME_CLASS_STATUS,  classStatus);

            int res = database.update(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME, contentValues,
                    AttendanceDBContract.AttendanceDBEntry._ID + " = " + _id, null);


            cursor.close();
            Cursor c = database.query(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME,
                    null, AttendanceDBContract.AttendanceDBEntry._ID + "=" + _id,
                    null, null, null, null);
            c.moveToFirst();
            Log.i("attendance", String.valueOf(c.getString(11)));
            Log.i("attendance", String.valueOf(c.getString(9)));
            c.close();
            database.close();
            return res != -1;
        }
        else {
            return false;
        }
    }

    public List<CourseTimeTableObject> getAllCourses() {
        List<CourseTimeTableObject> list = new ArrayList<>();

        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(AttendanceDBContract.AttendanceDBEntry.TABLE_NAME, null,
                null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            CourseTimeTableObject object = cursorToCourseObject(cursor);
            list.add(object);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    private CourseTimeTableObject cursorToCourseObject(Cursor cursor) {
        CourseTimeTableObject object = new CourseTimeTableObject(cursor.getString(1), cursor.getString(2),
                cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7),
                cursor.getString(12));
        object.set_id(cursor.getLong(0));
        object.setAttendance_criteria(8);

        return object;
    }
}

