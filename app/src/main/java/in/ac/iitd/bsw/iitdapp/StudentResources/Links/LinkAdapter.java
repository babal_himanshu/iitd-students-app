package in.ac.iitd.bsw.iitdapp.StudentResources.Links;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.ac.iitd.bsw.iitdapp.R;

public class LinkAdapter extends RecyclerView.Adapter<LinkAdapter.LinkViewHolder>{
    List<LinkObject> linkObjectList;
    Context context;

    public class LinkViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView linkTextView;

        public LinkViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            linkTextView = (TextView) itemView.findViewById(R.id.links_list_row_textView);
        }

        @Override
        public void onClick(View v) {
            int itemPosition = getAdapterPosition();
            String url = linkObjectList.get(itemPosition).getUrl();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(browserIntent);

//            Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
//            String facebookUrl = getFacebookPageURL(context);
//            facebookIntent.setData(Uri.parse(facebookUrl));
//            context.startActivity(facebookIntent);
        }
    }

    public LinkAdapter (List<LinkObject> linkObjectList, Context context){
        this.linkObjectList = linkObjectList;
        this.context = context;
    }

    //method to get the right URL to use in the intent
    public String getFacebookPageURL(Context context, String FACEBOOK_URL, String FACEBOOK_PAGE_ID) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
            } else { //older versions of fb app
                return "fb://page/" + FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL; //normal web url
        }
    }

    @Override
    public LinkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.links_list_row, parent, false);
        return new LinkViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LinkViewHolder holder, int position) {
        LinkObject object = linkObjectList.get(position);
        holder.linkTextView.setText(object.getName());
    }

    @Override
    public int getItemCount() {
        return linkObjectList.size();
    }
}
