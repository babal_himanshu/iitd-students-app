package in.ac.iitd.bsw.iitdapp.AttendanceManager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.Calendar;
import java.util.List;

import butterknife.ButterKnife;
import in.ac.iitd.bsw.iitdapp.AttendanceManager.TimeTable.CourseTimeTableObject;
import in.ac.iitd.bsw.iitdapp.R;

public class AttendanceAddDates extends Fragment{
    private List<CourseTimeTableObject> selectedCoursesList;
    private AttendanceDBHelper attendanceDBHelper;
    public static AttendanceDBHelper helper;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.attendance_fragment_add_dates, container, false);
        ButterKnife.bind(this, view);
        //getList();
        //alarmMethod();
        attendanceDBHelper = new AttendanceDBHelper(getContext());
        helper = attendanceDBHelper;
        List<AttendanceDBClass> allCourses = attendanceDBHelper.getDayWiseDetailsForAllCourses();

        //alarmMethod("TEST", "test");
        //testAlarmA("A", "test");
        //testAlarmB("B", "test");
        Log.i("attendance", String.valueOf(allCourses.size()));

//        for (int i = 0; i < allCourses.size(); i++) {
//            AttendanceDBClass c = allCourses.get(i);
//            /*
//            aDay[0] = course_code;
//            aDay[1] = days[i];
//            aDay[2] = room[i];
//            aDay[3] = start_time[i];
//            aDay[4] = end_time[i];
//             */
//            List<String[]> str = c.getA();
//            for (String[] s: str) {
//                classStartAlarm(s[0], s[1], s[3], s[2]);
//                classEndAlarm(s[0], s[1], s[4], s[2]);
//            }
//        }

        //Log.i("TIME", String.valueOf(System.currentTimeMillis()));
        for (int i = 0; i < allCourses.size(); i++) {
            AttendanceDBClass c = allCourses.get(i);
            AttendanceDBClass object = allCourses.get(i);
            List<String[]> str = object.getA();
            for (String[] s: str) {
                //classStartAlarm(s[0], s[1], s[3], s[2]);
                //classEndAlarm(s[0], s[1], s[4], s[2]);
                Log.i("attendance", s[0] + " " + s[1]);
                attendanceDBHelper.addClassBunked(s[0], "09/07/2016");
                attendanceDBHelper.addClassAttended(s[0], "10/07/2016");
                attendanceDBHelper.addClassCancelled(s[0], "11/07/2016");
                attendanceDBHelper.addClassBunked(s[0], "12/07/2016");
                attendanceDBHelper.addClassAttended(s[0], "13/07/2016");
                attendanceDBHelper.addClassCancelled(s[0], "14/07/2016");
                attendanceDBHelper.addClassAttended(s[0], "15/07/2016");
                attendanceDBHelper.addClassBunked(s[0], "16/07/2016");
                Log.i("attendance", "-----------------------------");

                //alarmMethod(s[0], s[2]);
                testAlarmA(s[0], s[2]);
                testAlarmB(s[0], s[2]);
//            Log.i("array", s[0]);
//            Log.i("array", s[1]);
//            Log.i("array", s[2]);
//            Log.i("array", s[3]);
//            Log.i("array", s[4]);
            }
        }

        return view;
    }

    private void testAlarmA (String courseCode, String room) {
        Log.i("attendance", "class-start");
        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent("android.media.action.DISPLAY_ATTENDANCE_NOTIFICATION");
        notificationIntent.addCategory("android.intent.category.DEFAULT");
        notificationIntent.putExtra("type", "start");
        notificationIntent.putExtra("courseCode", courseCode);
        notificationIntent.putExtra("courseRoom", room);
        notificationIntent.setAction(Long.toString(System.currentTimeMillis()));

        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, 1);
        //c.add(Calendar.SECOND, 30);
        Log.i("TIME", String.valueOf(c.getTimeInMillis()));

        PendingIntent broadcast = PendingIntent.getBroadcast(getContext(), 111, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), (2 * 60 * 1000), broadcast);
    }

    private void testAlarmB (String courseCode, String room){
        Log.i("attendance", "class-end");
        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent("android.media.action.DISPLAY_ATTENDANCE_NOTIFICATION");
        notificationIntent.addCategory("android.intent.category.DEFAULT");
        notificationIntent.putExtra("type", "end");
        notificationIntent.putExtra("courseCode", courseCode);
        notificationIntent.putExtra("courseRoom", room);
        notificationIntent.setAction(Long.toString(System.currentTimeMillis()));

        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, 2);
        //c.add(Calendar.SECOND, 30);
        Log.i("TIME", String.valueOf(c.getTimeInMillis()));

        PendingIntent broadcast = PendingIntent.getBroadcast(getContext(), 222, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), (2 * 60 * 1000), broadcast);

    }

    private void alarmMethod(String courseCode, String room) {
        Log.i("time", "alarmMethod");
        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent("android.media.action.DISPLAY_ATTENDANCE_NOTIFICATION");
        notificationIntent.addCategory("android.intent.category.DEFAULT");
        notificationIntent.putExtra("type", "test");
        notificationIntent.putExtra("courseCode", courseCode);
        notificationIntent.putExtra("courseRoom", room);
        notificationIntent.setAction(Long.toString(System.currentTimeMillis()));

        PendingIntent broadcast = PendingIntent.getBroadcast(getContext(), 333, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, 1);
        cal.add(Calendar.SECOND, 30);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 2 * 60 * 1000, broadcast);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
//        } else {
//            alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
//        }

    }

    public static void addClassAttended(String course_code, String date){
        helper.addClassAttended(course_code, date);
    }
    public static void addClassBunked(String course_code, String date){
        helper.addClassBunked(course_code, date);
    }
    public static void addClassCancelled(String course_code, String date){
        helper.addClassCancelled(course_code, date);
    }

    private void classStartAlarm (String courseCode, String day, String startTime, String room) {
        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent("android.media.action.DISPLAY_ATTENDANCE_NOTIFICATION");
        notificationIntent.addCategory("android.intent.category.DEFAULT");
        notificationIntent.putExtra("type", "start");
        notificationIntent.putExtra("courseCode", courseCode);
        notificationIntent.putExtra("courseRoom", room);
        notificationIntent.setAction(Long.toString(System.currentTimeMillis()));

        PendingIntent broadcast = PendingIntent.getBroadcast(getContext(), 444, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        int hour = Integer.parseInt(startTime.substring(0, 2));
        int minute = Integer.parseInt(startTime.substring(3));

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_WEEK, dayOfWeek(day));
        cal.add(Calendar.HOUR, hour);
        cal.add(Calendar.MINUTE, minute);

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis() - 8 * 60 * 1000, (AlarmManager.INTERVAL_DAY) * 7, broadcast);
    }

    private void classEndAlarm (String courseCode, String day, String endTime, String room) {
        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent("android.media.action.DISPLAY_ATTENDANCE_NOTIFICATION");
        notificationIntent.addCategory("android.intent.category.DEFAULT");
        notificationIntent.putExtra("type", "end");
        notificationIntent.putExtra("courseCode", courseCode);
        notificationIntent.putExtra("courseRoom", room);
        notificationIntent.setAction(Long.toString(System.currentTimeMillis()));

        PendingIntent broadcast = PendingIntent.getBroadcast(getContext(), 555, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        int hour = Integer.parseInt(endTime.substring(0, 2));
        int minute = Integer.parseInt(endTime.substring(3));

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_WEEK, dayOfWeek(day));
        cal.add(Calendar.HOUR, hour);
        cal.add(Calendar.MINUTE, minute);

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis() + 3 * 60 * 1000, (AlarmManager.INTERVAL_DAY) * 7, broadcast);

    }

    private void getList() {
        Bundle bundle = getArguments();
        selectedCoursesList = Parcels.unwrap(bundle.getParcelable("selectedCourses"));

        for (CourseTimeTableObject o : selectedCoursesList) {
            Log.i("parcel", o.getCourse_no());
        }

    }

//    @Subscribe(sticky = true)
//    public void onEvent (EventTimeTableObjectList list) {
//        this.list = list.getList();
//        EventBus.getDefault().removeStickyEvent(list);
//        Log.i("Event", "frag-event");
//        for (CourseTimeTableObject object : this.list) {
//            Log.i("Event", object.getCourse_no());
//        }
//    }
//
//    @Subscribe(sticky = true)
//    public void onList (ArrayList<CourseTimeTableObject> list) {
//        this.list = list;
//        EventBus.getDefault().removeStickyEvent(list);
//        Log.i("Event", "frag-list");
//        for (CourseTimeTableObject object : this.list) {
//            Log.i("Event", object.getCourse_no());
//        }
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        EventBus.getDefault().register(this);
//    }
//
//    @Override
//    public void onDestroy() {
//        EventBus.getDefault().unregister(this);
//        super.onDestroy();
//    }

    private int dayOfWeek (String day) {
        int value = 0;
        if (day.equals("Monday")) {
            value = 0;

        } else if (day.equals("Tuesday")) {
            value = 1;

        } else if (day.equals("Wednesday")) {
            value = 2;

        } else if (day.equals("Thursday")) {
            value = 3;

        } else if (day.equals("Friday")) {
            value = 4;

        } else if (day.equals("Saturday")) {
            value = 5;

        } else if (day.equals("Sunday")) {
            value = 6;

        } else {
            value = -1;
        }
        return value;
    }

}
