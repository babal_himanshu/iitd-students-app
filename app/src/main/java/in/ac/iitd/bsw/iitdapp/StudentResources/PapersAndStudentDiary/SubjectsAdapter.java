package in.ac.iitd.bsw.iitdapp.StudentResources.PapersAndStudentDiary;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.ac.iitd.bsw.iitdapp.R;

public class SubjectsAdapter extends RecyclerView.Adapter<SubjectsAdapter.SubjectViewHolder>{
    List<Subjects> subjects;
    Context context;
    OnItemClickListener onItemClickListener;

    public class SubjectViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView subjectNameTextView;

        public SubjectViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            subjectNameTextView = (TextView) itemView.findViewById(R.id.subjects_list_row_textView);
        }

        //Implementing RecyclerView OnItem Click Listener
        @Override
        public void onClick(View v) {
            if (onItemClickListener != null){
                onItemClickListener.onItemClick(v, getAdapterPosition());
            }

            //int itemPosition = getAdapterPosition();
            //Intent i = new Intent(context, ExamPapers.class);
            //i.putExtra("subjectName", subjects.get(itemPosition).getSubjectName());
            //itemView.getContext().startActivity(i);

        }
    }

    public SubjectsAdapter(List<Subjects> subjects, Context context){
        this.subjects = subjects;
        this.context = context;
    }

    public interface OnItemClickListener {
        public void onItemClick (View view, int position);
    }

    public void setOnItemClickListener (final OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }


    @Override
    public SubjectsAdapter.SubjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subjects_list_row, parent, false);

        return new SubjectViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SubjectsAdapter.SubjectViewHolder holder, int position) {
        Subjects subject = subjects.get(position);
        holder.subjectNameTextView.setText(subject.getSubjectName());
    }

    @Override
    public int getItemCount() {
        return subjects.size();
    }
}
