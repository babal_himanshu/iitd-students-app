package in.ac.iitd.bsw.iitdapp.AttendanceManager;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.ac.iitd.bsw.iitdapp.AttendanceManager.TimeTable.CourseTimeTableObject;
import in.ac.iitd.bsw.iitdapp.AttendanceManager.TimeTable.TimeTableDBHelper;
import in.ac.iitd.bsw.iitdapp.R;

import static java.util.Arrays.asList;

public class AddSubjectFragment extends Fragment implements View.OnClickListener {
    @BindView(R.id.attendance_manager_add_courses_fab)
    FloatingActionButton addCourseFAB;
    @BindView(R.id.attendance_manager_add_courses_custom_linearLayout)
    LinearLayout customLinearLayout;
    @BindView(R.id.attendance_manager_add_courses_submit_button)
    Button submitButton;
    @BindView(R.id.attendance_manager_add_courses_custom_button)
    Button addCustomCourse;
    @BindView(R.id.swipe_recyclerView)
    RecyclerView recyclerView;

    private SwipeRecyclerViewAdapter adapter;
    private ArrayAdapter<String> autoCompleteAdapter;
    private List<CourseTimeTableObject> courseList;
    private String[] courseArray;
    private List<CourseTimeTableObject> selectedCourses = new ArrayList<>();
    private int hour, minute;
    private AttendanceDBHelper attendanceDBHelper;

    // TODO: 7/9/16 -> Automatically add Tutorials (if exists) for the chosen course 
    // TODO: 7/9/16 -> Merge Courses whose names are changed, eg. MTLxxx/MALxxx
    // TODO: 7/9/16 -> Tutorials for custom courses
    // TODO: 7/9/16 -> Auto Batch recognize for 1st year students
    // TODO: 7/9/16 -> Subject containing a day multiple times
    // TODO: 7/9/16 -> set keyBoard hide at proper places
    
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.attendance_fragment_add_subjects, container, false);
        //setRetainInstance(true);
        ButterKnife.bind(this, view);

        submitButton.setOnClickListener(this);
        addCustomCourse.setOnClickListener(this);
        addCourseFAB.setOnClickListener(this);

        //ProgressDialog progressDialog = new ProgressDialog(getContext());
        //progressDialog.show();
        TimeTableDBHelper timeTableDBHelper = new TimeTableDBHelper(getContext());
        courseList = timeTableDBHelper.getAllCourses();
        attendanceDBHelper = new AttendanceDBHelper(getContext());

        courseArray = new String[courseList.size()];
        for (int i = 0; i < courseList.size(); i++) {
            courseArray[i] = courseList.get(i).getCourse_no();
        }

        autoCompleteAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, courseArray);

        adapter = new SwipeRecyclerViewAdapter(getContext(), selectedCourses);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        adapter.setClickListener(new SwipeRecyclerViewAdapter.ClickListener() {
            @Override
            public void onItemClick(final int position, View v) {
                if (v.getId() == R.id.swipe_edit) {
                    CourseTimeTableObject object = selectedCourses.get(position);
                    editCourseCustomDialog(object.getCourse_no(), object.getCourse_name(), object.getUser_name(), object.getRoom_no(),
                            object.getAttendance_criteria(), object.getStart_time(), object.getEnd_time(), object.getDay());
                    Toast.makeText(v.getContext(), "Clicked on EDIT", Toast.LENGTH_SHORT).show();

                } else if (v.getId() == R.id.swipe_delete) {
                    new AlertDialog.Builder(getContext())
                            .setTitle("Are You Sure?")
                            .setMessage("Do you want to delete " + selectedCourses.get(position).getCourse_no()
                                    + " from Selected Course List")
                            .setNegativeButton("No", null)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    selectedCourses.remove(position);
                                    adapter.notifyDataSetChanged();
                                }
                            })
                            .show();
                } else {
                    Toast.makeText(v.getContext(), "Clicked", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (savedInstanceState != null) {
            selectedCourses = Parcels.unwrap(savedInstanceState.getParcelable("selectedCourses"));
            adapter.notifyDataSetChanged();
        }
        Log.i("selectedCourses-create", String.valueOf(selectedCourses.size()));


        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle state) {

        Log.i("selectedCourses-onSave", String.valueOf(selectedCourses.size()));
        state.putParcelable("selectedCourses", Parcels.wrap(selectedCourses));
        super.onSaveInstanceState(state);
    }

    @Override
    public void onPause() {

        Bundle state = new Bundle();
        Log.i("selectedCourses-pause", String.valueOf(selectedCourses.size()));
        state.putParcelable("selectedCourses", Parcels.wrap(selectedCourses));
        super.onPause();
    }

    @Override
    public void onResume() {

        Log.i("selectedCourses-resume", "onResume-called");
        Bundle savedInstanceState = getArguments();
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("selectedCourses")) {
                selectedCourses = Parcels.unwrap(savedInstanceState.getParcelable("selectedCourses"));
                adapter.notifyDataSetChanged();
            }
            Log.i("selectedCourses-resume", String.valueOf(selectedCourses.size()));
        }
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("selectedCourses-start", "-called");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("selectedCourses-stop", "-called");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i("selCourses-destView", "-called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("selectedCourses-destroy", "-called");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.attendance_manager_add_courses_fab) {
            addCourseCustomDialog();

        } else if (v.getId() == R.id.attendance_manager_add_courses_submit_button) {
            if (selectedCourses.size() != 0) {
                //EventBus.getDefault().postSticky(new EventTimeTableObjectList(selectedCourses));
                //EventBus.getDefault().postSticky(selectedCourses);

                attendanceDBHelper.onUpdate();
                for (CourseTimeTableObject object : selectedCourses) {
                    attendanceDBHelper.insertCourse(object);
                }

                Bundle bundle = new Bundle();
                //bundle.putParcelable("selectedCourses", Parcels.wrap(selectedCourses));
                startFragmentWithBundle(new AttendanceAddDates(), bundle);

            } else {
                new AlertDialog.Builder(getContext())
                        .setTitle("Choose at least one Course")
                        .setMessage("Please choose/add at least one course to proceed")
                        .setPositiveButton("Okay", null)
                        .show();
            }

        } else if (v.getId() == R.id.attendance_manager_add_courses_custom_button) {
            getTime();
            customDialog();
        }

    }

    // TODO: 7/9/16 -> Add button to edit that course on alert dialog when an already existing course is chosen
    // TODO: 7/9/16 -> allow 100% attendance
    private void addCourseCustomDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.aleart_dialog_add_course);
        dialog.setCanceledOnTouchOutside(false);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;

        dialog.getWindow().setLayout((int)(0.95 * width), WindowManager.LayoutParams.WRAP_CONTENT);

        TextView title = (TextView) dialog.findViewById(R.id.alert_dialog_add_course_title);
        title.setText("Add New Subject");
        Button addButton = (Button) dialog.findViewById(R.id.alert_dialog_add_course_add_button);
        Button cancelButton = (Button) dialog.findViewById(R.id.alert_dialog_add_course_cancel_button);

        final AutoCompleteTextView addCourseAutoComplete = (AutoCompleteTextView) dialog.findViewById(R.id.alert_dialog_add_course_auto_complete_textView);
        final EditText addCourseAttendanceEditText = (EditText) dialog.findViewById(R.id.alert_dialog_add_course_attendance_editText);
        TextView addCustomCourseText = (TextView) dialog.findViewById(R.id.alert_dialog_add_course_newCourse_textView);
        addCourseAttendanceEditText.setEnabled(false);
        addCourseAttendanceEditText.setFocusable(false);

        addCourseAutoComplete.setAdapter(autoCompleteAdapter);
        dialog.show();

        addCourseAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                addCourseAttendanceEditText.setEnabled(true);
                addCourseAttendanceEditText.setFocusable(true);
                addCourseAttendanceEditText.setFocusableInTouchMode(true);
            }
        });

        addCustomCourseText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                customDialog();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedCourse = addCourseAutoComplete.getText().toString();
                if (!selectedCourse.equals("")) {
                    if (asList(courseArray).contains(selectedCourse)) {
                        String percent = addCourseAttendanceEditText.getText().toString();
                        if (!percent.equals("") && percent.length() < 3){
                            String[] selectedCoursesArray = selectedCourseCodeArray();
                            if (! Arrays.asList(selectedCoursesArray).contains(selectedCourse)) {

                                int index = Arrays.asList(courseArray).indexOf(selectedCourse);

                                dialog.dismiss();
                                selectDaysCustomDialog(index, (int) Float.parseFloat(percent));
                                for (CourseTimeTableObject o : selectedCourses) {
                                }
                            } else {
                                new AlertDialog.Builder(getContext())
                                        .setTitle("Course Already Selected")
                                        .setMessage("Course " + selectedCourse + " is Already in your list."
                                        + "\nChoose a different course or edit existing")
                                        .setPositiveButton("Okay", null)
                                        .show();
                            }

                        } else {
                            new AlertDialog.Builder(getContext())
                                    .setTitle("Enter Attendance Criteria")
                                    .setMessage("Please enter course attendance criteria \nin the range 1-100")
                                    .setPositiveButton("Okay", null)
                                    .show();
                        }
                    } else {
                        new AlertDialog.Builder(getContext())
                                .setTitle("Choose a Valid Course")
                                .setMessage("Please choose a course from the list. If course is not" +
                                        " given in this list, opt to add a custom course")
                                .setPositiveButton("Okay", null)
                                .show();
                    }
                } else {
                    new AlertDialog.Builder(getContext())
                            .setTitle("Please Choose a Course")
                            .setMessage("Type in the Box to see course choices")
                            .setPositiveButton("Okay", null)
                            .show();
                }
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void selectDaysCustomDialog(int index, final int attendance){
        final Dialog dialog = new Dialog(getContext());
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.alert_dialog_select_days);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        dialog.getWindow().setLayout((int)(0.95 * width), WindowManager.LayoutParams.WRAP_CONTENT);

        TextView title = (TextView) dialog.findViewById(R.id.alert_dialog_select_days_title);
        title.setText("Select days");
        Button addButton = (Button) dialog.findViewById(R.id.alert_dialog_select_days_add_button);
        Button cancelButton = (Button) dialog.findViewById(R.id.alert_dialog_select_days_cancel_button);
        LinearLayout linearLayout = (LinearLayout) dialog.findViewById(R.id.alert_dialog_select_days_linear_layout);

        final CourseTimeTableObject object = courseList.get(index);
        final String[] room = object.getRoom_no().split(",");
        String[] start = object.getStart_time().split(",");
        String[] end = object.getEnd_time().split(",");
        String[] day = object.getDay().split(",");

        //String[] eachDay = new String[room.length];
        final List<CheckBox> checkBoxList = new ArrayList<>();
        for (int i = 0; i < room.length; i++) {
            String d = day[i] + " , " + room[i] + " , " + start[i] + " , " + end[i];
            //eachDay[i] = d;

            CheckBox checkBox = new CheckBox(getContext());
            checkBox.setText(d);
            linearLayout.addView(checkBox);
            checkBoxList.add(checkBox);
        }

        dialog.show();



        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int checkCounter = 0;
                String selDay = "";
                String selRoom = "";
                String selStart = "";
                String selEnd = "";

                for (int i = 0; i < checkBoxList.size(); i++) {
                    CheckBox check = checkBoxList.get(i);
                    if (check.isChecked()) {
                        checkCounter ++;
                        String checkText = check.getText().toString();

                        String[] checkTextArray = checkText.split(" , ");
                        selDay += checkTextArray[0] + ",";
                        selRoom += checkTextArray[1] + ",";
                        selStart += checkTextArray[2] + ",";
                        selEnd += checkTextArray[3] + ",";
                    }
                }
                if (selRoom.length() != 0){
                    selRoom = selRoom.substring(0, selRoom.length() - 1);
                }
                if (selDay.length() != 0) {
                    selDay = selDay.substring(0, selDay.length() - 1);
                }
                if (selEnd.length() != 0) {
                    selEnd = selEnd.substring(0, selEnd.length() - 1);
                }
                if (selStart.length() != 0) {
                    selStart = selStart.substring(0, selStart.length() - 1);
                }

                if (checkCounter != 0) {
                    object.setDay(selDay);
                    object.setRoom_no(selRoom);
                    object.setEnd_time(selEnd);
                    object.setStart_time(selStart);

                    object.setAttendance_criteria(attendance);

                    selectedCourses.add(object);
                    adapter.notifyDataSetChanged();

                    dialog.dismiss();
                }
                else {
                    new AlertDialog.Builder(getContext())
                            .setTitle("Choose at Least One Day")
                            .setMessage("Please Choose at Least One Day to Proceed")
                            .setPositiveButton("Okay", null)
                            .show();
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private String[] selectedCourseCodeArray() {
        String[] array = new String[selectedCourses.size()];
        for (int i = 0; i < selectedCourses.size(); i++) {
            array[i] = selectedCourses.get(i).getCourse_no();
        }
        return array;
    }

    // TODO: 7/7/16 -> replace checkboxes by list selection
    // TODO: 7/9/16 -> Add button to edit that course on alert dialog when an already existing course is chosen
    private void customDialog(){
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.attendance_fragment_add_custom_subject);
        dialog.setCanceledOnTouchOutside(false);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        dialog.getWindow().setLayout((int)(0.95 * width), (int)(0.85 * height));

        TextView title = (TextView) dialog.findViewById(R.id.attendance_manager_addCustomClass_title);
        title.setText("Add New Subject");
        Button addButton = (Button) dialog.findViewById(R.id.attendance_manager_addCustomClass_add_button);
        Button cancelButton = (Button) dialog.findViewById(R.id.attendance_manager_addCustomClass_cancel_button);

        final EditText courseCodeEditText = (EditText) dialog.findViewById(R.id.attendance_manager_addCustomClass_course_code_editText);
        final EditText courseNameEditText = (EditText) dialog.findViewById(R.id.attendance_manager_addCustomClass_course_name_editText);
        final EditText facultyNameEditText = (EditText) dialog.findViewById(R.id.attendance_manager_addCustomClass_course_faculty_editText);
        final EditText roomNoEditText = (EditText) dialog.findViewById(R.id.attendance_manager_addCustomClass_course_room_no_editText);
        final EditText attendanceEditText = (EditText) dialog.findViewById(R.id.attendance_manager_addCustomClass_attendance__editText);
        final TextView startTimeTextView = (TextView) dialog.findViewById(R.id.attendance_manager_addCustomClass_start_time_textView);
        Button startTimeButton = (Button) dialog.findViewById(R.id.attendance_manager_addCustomClass_start_time_button);
        final TextView endTimeTextView = (TextView) dialog.findViewById(R.id.attendance_manager_addCustomClass_end_time_textView);
        Button endTimeButton = (Button) dialog.findViewById(R.id.attendance_manager_addCustomClass_end_time_button);
        final CheckBox monday = (CheckBox) dialog.findViewById(R.id.attendance_manager_addCustomClass_checkbox_monday);
        final CheckBox tuesday = (CheckBox) dialog.findViewById(R.id.attendance_manager_addCustomClass_checkbox_tuesday);
        final CheckBox wednesday = (CheckBox) dialog.findViewById(R.id.attendance_manager_addCustomClass_checkbox_wednesday);
        final CheckBox thursday = (CheckBox) dialog.findViewById(R.id.attendance_manager_addCustomClass_checkbox_thursday);
        final CheckBox friday = (CheckBox) dialog.findViewById(R.id.attendance_manager_addCustomClass_checkbox_friday);
        final CheckBox saturday = (CheckBox) dialog.findViewById(R.id.attendance_manager_addCustomClass_checkbox_saturday);
        final CheckBox sunday = (CheckBox) dialog.findViewById(R.id.attendance_manager_addCustomClass_checkbox_sunday);
        final List<CheckBox> checkBoxList = new ArrayList<>();
        checkBoxList.add(monday);
        checkBoxList.add(tuesday);
        checkBoxList.add(wednesday);
        checkBoxList.add(thursday);
        checkBoxList.add(friday);
        checkBoxList.add(saturday);
        checkBoxList.add(sunday);

        //removing ',' at the end
        //final String days =  res.substring(0, res.length() - 1);
        startTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String hour;
                        if (hourOfDay < 10) {
                            hour = "0" + hourOfDay;
                        } else {
                            hour = String.valueOf(hourOfDay);
                        }
                        String min;
                        if (minute < 10) {
                            min = "0" + minute;
                        } else {
                            min = String.valueOf(minute);
                        }

                        String finalChosenTime = hour + ":" + min;
                        startTimeTextView.setText(finalChosenTime);
                    }
                }, hour, minute, false);
                timePickerDialog.show();
            }
        });
        endTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String hour;
                        if (hourOfDay < 10) {
                            hour = "0" + hourOfDay;
                        } else {
                            hour = String.valueOf(hourOfDay);
                        }
                        String min;
                        if (minute < 10) {
                            min = "0" + minute;
                        }
                        else {
                            min = String.valueOf(minute);
                        }

                        String finalChosenTime = hour + ":" + min;

                        endTimeTextView.setText(finalChosenTime);

                    }
                }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean allFieldsFilled;

                if (courseCodeEditText.getText().length() != 0) {

                    if (courseNameEditText.getText().length() != 0) {

                        if (roomNoEditText.getText().length() != 0) {

                            if (facultyNameEditText.getText().length() != 0) {

                                if (attendanceEditText.getText().length() != 0 && attendanceEditText.getText().length() < 3){

                                    if (!startTimeTextView.getText().equals("start time")){

                                        if (! endTimeTextView.getText().equals("end time")) {
                                            allFieldsFilled = true;
                                        } else {
                                            allFieldsFilled = false;
                                            new AlertDialog.Builder(getContext())
                                                    .setTitle("Choose Ending Time")
                                                    .setMessage("Please Choose the time at which " + courseCodeEditText.getText().toString() +
                                                    " ends")
                                                    .setPositiveButton("Okay", null)
                                                    .show();
                                        }
                                    } else {
                                        allFieldsFilled = false;
                                        new AlertDialog.Builder(getContext())
                                                .setTitle("Choose Starting Time")
                                                .setMessage("Please Choose the time at which " + courseCodeEditText.getText().toString() +
                                                        " starts")
                                                .setPositiveButton("Okay", null)
                                                .show();
                                    }
                                } else {
                                    allFieldsFilled = false;
                                    attendanceEditText.setError("Enter a Valid Number (0-100)");
                                }
                            } else {
                                allFieldsFilled = false;
                                facultyNameEditText.setError("Field can not be empty");
                            }
                        } else {
                            allFieldsFilled = false;
                            roomNoEditText.setError("Field can not be empty");
                        }
                    } else {
                        allFieldsFilled = false;
                        courseNameEditText.setError("Field can not be empty");
                    }
                } else {
                    allFieldsFilled = false;
                    courseCodeEditText.setError("Field can not be empty");
                }
                final Boolean finalAllFieldsFilled = allFieldsFilled;

                int checkCounter = 0;
                for (CheckBox checkBox : checkBoxList) {
                    if (checkBox.isChecked()) {
                        checkCounter ++;
                    }
                }

                //final String finalRes = res;
                String res = "";
                if (monday.isChecked()) {
                    res += "Monday,";
                }
                if (tuesday.isChecked()) {
                    res += "Tuesday,";
                }
                if (wednesday.isChecked()) {
                    res += "Wednesday,";
                }
                if (thursday.isChecked()) {
                    res += "Thursday,";
                }
                if (friday.isChecked()) {
                    res += "Friday,";
                }
                if (saturday.isChecked()) {
                    res += "Saturday,";
                }
                if (sunday.isChecked()) {
                    res +="Sunday,";
                }

                if (finalAllFieldsFilled ) {
                    if (res.length() != 0) {
                        String[] selectedCourseCodeArray = selectedCourseCodeArray();
                        if (! Arrays.asList(selectedCourseCodeArray).contains(courseCodeEditText.getText().toString())) {
                            String start_time = startTimeTextView.getText().toString();
                            String end_time = endTimeTextView.getText().toString();
                            String  room = roomNoEditText.getText().toString();
                            String a = start_time + ",";
                            String b = end_time + ",";
                            String c = room + ",";
                            for (int i = 0; i < checkCounter - 1; i ++) {
                                a += start_time +  ",";
                                b += end_time + ",";
                                c += roomNoEditText.getText().toString() + ",";
                            }
                            start_time = a;
                            end_time = b;
                            room = c;
                            start_time = start_time.substring(0, start_time.length()-1);
                            end_time = end_time.substring(0, end_time.length()-1);
                            room = room.substring(0, room.length()-1);

                            CourseTimeTableObject newCourse = new CourseTimeTableObject(courseCodeEditText.getText().toString(),
                                    res.substring(0, res.length() - 1), start_time, end_time);

                            newCourse.setCourse_name(courseNameEditText.getText().toString());
                            newCourse.setRoom_no(room);
                            newCourse.setUser_name(facultyNameEditText.getText().toString());
                            newCourse.setAttendance_criteria((int) Float.parseFloat(attendanceEditText.getText().toString()));

                            selectedCourses.add(newCourse);
                            adapter.notifyDataSetChanged();
                            dialog.dismiss();

                        } else {
                            new AlertDialog.Builder(getContext())
                                    .setTitle("Course Already Selected")
                                    .setMessage("Course " + courseCodeEditText.getText().toString() + " is Already in your list."
                                            + "\nChoose a different course or edit existing")
                                    .setPositiveButton("Okay", null)
                                    .show();
                        }

                    } else {
                        new AlertDialog.Builder(getContext())
                                .setTitle("Choose days for " + courseCodeEditText.getText().toString())
                                .setMessage("Please Choose the days on which " + courseCodeEditText.getText().toString() +
                                        " takes place")
                                .setPositiveButton("Okay", null)
                                .show();
                    }
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void editCourseCustomDialog(String courseCode, String courseName, String facultyName, String roomNo,
                                        int attendance, String startTime, String endTime, String days) {
        final Dialog dialog = new Dialog(getContext());
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.attendance_fragment_add_custom_subject);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        dialog.getWindow().setLayout((int)(0.95 * width), (int)(0.85 * height));

        TextView title = (TextView) dialog.findViewById(R.id.attendance_manager_addCustomClass_title);
        title.setText("Add New Subject");
        Button addButton = (Button) dialog.findViewById(R.id.attendance_manager_addCustomClass_add_button);
        Button cancelButton = (Button) dialog.findViewById(R.id.attendance_manager_addCustomClass_cancel_button);

        final EditText courseCodeEditText = (EditText) dialog.findViewById(R.id.attendance_manager_addCustomClass_course_code_editText);
        courseCodeEditText.setText(courseCode);
        final EditText courseNameEditText = (EditText) dialog.findViewById(R.id.attendance_manager_addCustomClass_course_name_editText);
        courseNameEditText.setText(courseName);
        final EditText facultyNameEditText = (EditText) dialog.findViewById(R.id.attendance_manager_addCustomClass_course_faculty_editText);
        facultyNameEditText.setText(facultyName);
        final EditText roomNoEditText = (EditText) dialog.findViewById(R.id.attendance_manager_addCustomClass_course_room_no_editText);
        roomNoEditText.setText(roomNo);
        final EditText attendanceEditText = (EditText) dialog.findViewById(R.id.attendance_manager_addCustomClass_attendance__editText);
        attendanceEditText.setText(String.valueOf(attendance));
        final TextView startTimeTextView = (TextView) dialog.findViewById(R.id.attendance_manager_addCustomClass_start_time_textView);
        startTimeTextView.setText(startTime);
        Button startTimeButton = (Button) dialog.findViewById(R.id.attendance_manager_addCustomClass_start_time_button);
        final TextView endTimeTextView = (TextView) dialog.findViewById(R.id.attendance_manager_addCustomClass_end_time_textView);
        endTimeTextView.setText(endTime);
        Button endTimeButton = (Button) dialog.findViewById(R.id.attendance_manager_addCustomClass_end_time_button);
        final CheckBox monday = (CheckBox) dialog.findViewById(R.id.attendance_manager_addCustomClass_checkbox_monday);
        if (days.contains("Monday")) { monday.setChecked(true); }
        final CheckBox tuesday = (CheckBox) dialog.findViewById(R.id.attendance_manager_addCustomClass_checkbox_tuesday);
        if (days.contains("Tuesday")) { tuesday.setChecked(true); }
        final CheckBox wednesday = (CheckBox) dialog.findViewById(R.id.attendance_manager_addCustomClass_checkbox_wednesday);
        if (days.contains("Wednesday")) { wednesday.setChecked(true); }
        final CheckBox thursday = (CheckBox) dialog.findViewById(R.id.attendance_manager_addCustomClass_checkbox_thursday);
        if (days.contains("Thursday")) { thursday.setChecked(true); }
        final CheckBox friday = (CheckBox) dialog.findViewById(R.id.attendance_manager_addCustomClass_checkbox_friday);
        if (days.contains("Friday")) { friday.setChecked(true); }
        final CheckBox saturday = (CheckBox) dialog.findViewById(R.id.attendance_manager_addCustomClass_checkbox_saturday);
        if (days.contains("Saturday")) { saturday.setChecked(true); }
        final CheckBox sunday = (CheckBox) dialog.findViewById(R.id.attendance_manager_addCustomClass_checkbox_sunday);
        if (days.contains("Sunday")) { sunday.setChecked(true); }


        //removing ',' at the end
        //final String days =  res.substring(0, res.length() - 1);

        startTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String hour;
                        if (hourOfDay < 10) {
                            hour = "0" + hourOfDay;
                        } else {
                            hour = String.valueOf(hourOfDay);
                        }
                        String min;
                        if (minute == 0) {
                            min = "00";
                        } else {
                            min = String.valueOf(minute);
                        }

                        String finalChosenTime = hour + ":" + min;
                        startTimeTextView.setText(finalChosenTime);
                    }
                }, hour, minute, false);
                timePickerDialog.show();
            }
        });
        endTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String hour;
                        if (hourOfDay < 10) {
                            hour = "0" + hourOfDay;
                        } else {
                            hour = String.valueOf(hourOfDay);
                        }

                        String finalChosenTime = hour + ":" + minute;

                        endTimeTextView.setText(finalChosenTime);

                    }
                }, hour, minute, false);
                timePickerDialog.show();
            }
        });
        //final String finalRes = res;
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String res = "";
                if (monday.isChecked()) {
                    res += "Monday,";
                }
                if (tuesday.isChecked()) {
                    res += "Tuesday,";
                }
                if (wednesday.isChecked()) {
                    res += "Wednesday,";
                }
                if (thursday.isChecked()) {
                    res += "Thursday,";
                }
                if (friday.isChecked()) {
                    res += "Friday,";
                }
                if (saturday.isChecked()) {
                    res += "Saturday,";
                }
                if (sunday.isChecked()) {
                    res +="Sunday,";
                }

                CourseTimeTableObject newCourse = new CourseTimeTableObject(courseCodeEditText.getText().toString(),
                        res.substring(0, res.length() - 1), startTimeTextView.getText().toString(), endTimeTextView.getText().toString());

                newCourse.setCourse_name(courseNameEditText.getText().toString());
                newCourse.setRoom_no(roomNoEditText.getText().toString());
                newCourse.setUser_name(facultyNameEditText.getText().toString());
                newCourse.setAttendance_criteria((int) Float.parseFloat(attendanceEditText.getText().toString()));

                selectedCourses.add(newCourse);
                adapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void getTime() {
        Calendar calendar = Calendar.getInstance();

        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
    }

    private void startFragmentWithBundle (Fragment fragment, Bundle bundle) {
        //String backStateName = fragment.getClass().getName();
        //Log.i("debug", backStateName);
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.attendance_manager_container_fragment, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

//    private void addCourseDialog(){
//        MaterialDialog dialog = new MaterialDialog.Builder(getContext())
//                .customView(R.layout.aleart_dialog_add_course, true)
//                .title("Add New Subject")
//                .positiveText("Add")
//                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        String selectedCourse = addCourseAutoComplete.getText().toString();
//                        if (!selectedCourse.equals("")) {
//                            if (Arrays.asList(courseArray).contains(selectedCourse)) {
//                                String percent = addCourseAttendanceEditText.getText().toString();
//                                if (!percent.equals("")){
//                                    Log.i("multi", selectedCourse + " " + percent);
//                                    dialog.dismiss();
//                                } else {
//                                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
//
//
//                                    alertDialogBuilder.setTitle("Enter Attendance Criteria")
//                                    .setMessage("Please enter course attendance criteria");
//                                    final AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                    alertDialogBuilder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//
//                                         }
//                                    });
//
//                                    alertDialog.show();
//                                }
//                            } else {
//                                new AlertDialog.Builder(getContext())
//                                        .setTitle("Choose a Valid Course")
//                                        .setMessage("Please choose a course from the list. If course is not" +
//                                                " given in this list, opt to add a custom course")
//                                        .setPositiveButton("Okay", null)
//                                        .show();
//                            }
//                        } else {
//                            new AlertDialog.Builder(getContext())
//                                    .setTitle("Please Choose a Course")
//                                    .setMessage("Type in the Box to see course choices")
//                                    .setPositiveButton("Okay", null)
//                                    .show();
//                        }
//                    }
//                })
//                .negativeText("Cancel")
//                .onNegative(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        dialog.dismiss();
//                    }
//                })
//                .build();
//
//        addCourseAutoComplete = (AutoCompleteTextView) dialog.getCustomView().findViewById(R.id.alert_dialog_add_course_auto_complete_textView);
//        addCourseAttendanceEditText = (EditText) dialog.getCustomView().findViewById(R.id.alert_dialog_add_course_attendance_editText);
//        addCourseAttendanceEditText.setEnabled(false);
//        addCourseAttendanceEditText.setFocusable(false);
//
//        addCourseAutoComplete.setAdapter(autoCompleteAdapter);
//
//        addCourseAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Log.i("multi-select", (String) parent.getItemAtPosition(position));
//
//                addCourseAttendanceEditText.setEnabled(true);
//                addCourseAttendanceEditText.setFocusable(true);
//                addCourseAttendanceEditText.setFocusableInTouchMode(true);
//            }
//        });
//
////        new AdapterView.OnItemSelectedListener() {
////            @Override
////            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
////                Log.i("multi-select", courseArray[position]);
////
////                addCourseAttendanceEditText.setEnabled(true);
////                addCourseAttendanceEditText.setFocusable(true);
////                addCourseAttendanceEditText.setFocusableInTouchMode(true);
////            }
////
////            @Override
////            public void onNothingSelected(AdapterView<?> parent) {
////                Log.i("multi-select", "nothingSelected");
////            }
////        });
//
//        dialog.show();
//    }
//
//    private void customMaterialDialog() {
//        dialog = new MaterialDialog.Builder(getContext())
//                .customView(R.layout.attendance_fragment_add_custom_subject, true)
//                .title("Add New Subject")
//                .positiveText("Add")
//                .negativeText("Cancel")
//                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        String res = "";
//                        if (monday.isChecked()) {
//                            res += "Monday,";
//                        }
//                        if (tuesday.isChecked()) {
//                            res += "Tuesday,";
//                        }
//                        if (wednesday.isChecked()) {
//                            res += "Wednesday,";
//                        }
//                        if (thursday.isChecked()) {
//                            res += "Thursday,";
//                        }
//                        if (friday.isChecked()) {
//                            res += "Friday,";
//                        }
//                        if (saturday.isChecked()) {
//                            res += "Saturday,";
//                        }
//                        if (sunday.isChecked()) {
//                            res +="Sunday,";
//                        }
//
//                        Log.i("custom", courseCodeEditText.getText().toString());
//                        Log.i("custom", courseNameEditText.getText().toString());
//                        Log.i("custom", roomNoEditText.getText().toString());
//                        Log.i("custom", facultyNameEditText.getText().toString());
//                        Log.i("custom", attendanceEditText.getText().toString());
//                        Log.i("custom", startTimeTextView.getText().toString());
//                        Log.i("custom", endTimeTextView.getText().toString());
//                        Log.i("custom", res);
//
//                        CourseTimeTableObject newCourse = new CourseTimeTableObject(courseCodeEditText.getText().toString(),
//                                res.substring(0, res.length() - 1), startTimeTextView.getText().toString(), endTimeTextView.getText().toString());
//
//                        newCourse.setCourse_name(courseNameEditText.getText().toString());
//                        newCourse.setRoom_no(roomNoEditText.getText().toString());
//                        newCourse.setUser_name(facultyNameEditText.getText().toString());
//                        newCourse.setAttendance_criteria((int) Float.parseFloat(attendanceEditText.getText().toString()));
//
//                        selectedCourses.add(newCourse);
//                        dialog.dismiss();
//                    }
//                })
//                .build();
////        dialog.setTitle("Add New Subject");
//
////        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
////        lp.copyFrom(dialog.getWindow().getAttributes());
////        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
////        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
////        dialog.getWindow().setAttributes(lp);
//
//
//        courseCodeEditText = (EditText) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_course_code_editText);
//        courseNameEditText = (EditText) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_course_name_editText);
//        facultyNameEditText = (EditText) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_course_faculty_editText);
//        roomNoEditText = (EditText) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_course_room_no_editText);
//        attendanceEditText = (EditText) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_attendance__editText);
//        startTimeTextView = (TextView) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_start_time_textView);
//        Button startTimeButton = (Button) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_start_time_button);
//        endTimeTextView = (TextView) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_end_time_textView);
//        Button endTimeButton = (Button) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_end_time_button);
//        monday = (CheckBox) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_checkbox_monday);
//        tuesday = (CheckBox) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_checkbox_tuesday);
//        wednesday = (CheckBox) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_checkbox_wednesday);
//        thursday = (CheckBox) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_checkbox_thursday);
//        friday = (CheckBox) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_checkbox_friday);
//        saturday = (CheckBox) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_checkbox_saturday);
//        sunday = (CheckBox) dialog.getCustomView().findViewById(R.id.attendance_manager_addCustomClass_checkbox_sunday);
//
//
//        //removing ',' at the end
//        //final String days =  res.substring(0, res.length() - 1);
//
//        startTimeButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                        String hour;
//                        if (hourOfDay < 10) {
//                            hour = "0" + hourOfDay;
//                        } else {
//                            hour = String.valueOf(hourOfDay);
//                        }
//                        String min;
//                        if (minute == 0) {
//                            min = "00";
//                        } else {
//                            min = String.valueOf(minute);
//                        }
//
//                        String finalChosenTime = hour + ":" + min;
//                        Log.i("custom", finalChosenTime);
//                        startTimeTextView.setText(finalChosenTime);
//                    }
//                }, hour, minute, false);
//                timePickerDialog.show();
//            }
//        });
//        endTimeButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                        String hour;
//                        if (hourOfDay < 10) {
//                            hour = "0" + hourOfDay;
//                        } else {
//                            hour = String.valueOf(hourOfDay);
//                        }
//
//                        String finalChosenTime = hour + ":" + minute;
//                        Log.i("custom", finalChosenTime);
//
//                        endTimeTextView.setText(finalChosenTime);
//
//                    }
//                }, hour, minute, false);
//                timePickerDialog.show();
//            }
//        });
//        //final String finalRes = res;
//        submitButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String res = "";
//                if (monday.isChecked()) {
//                    res += "Monday,";
//                }
//                if (tuesday.isChecked()) {
//                    res += "Tuesday,";
//                }
//                if (wednesday.isChecked()) {
//                    res += "Wednesday,";
//                }
//                if (thursday.isChecked()) {
//                    res += "Thursday,";
//                }
//                if (friday.isChecked()) {
//                    res += "Friday,";
//                }
//                if (saturday.isChecked()) {
//                    res += "Saturday,";
//                }
//                if (sunday.isChecked()) {
//                    res +="Sunday,";
//                }
//
//                Log.i("custom", courseCodeEditText.getText().toString());
//                Log.i("custom", courseNameEditText.getText().toString());
//                Log.i("custom", roomNoEditText.getText().toString());
//                Log.i("custom", facultyNameEditText.getText().toString());
//                Log.i("custom", attendanceEditText.getText().toString());
//                Log.i("custom", startTimeTextView.getText().toString());
//                Log.i("custom", endTimeTextView.getText().toString());
//                Log.i("custom", res);
//
//                CourseTimeTableObject newCourse = new CourseTimeTableObject(courseCodeEditText.getText().toString(),
//                        res.substring(0, res.length() - 1), startTimeTextView.getText().toString(), endTimeTextView.getText().toString());
//
//                newCourse.setCourse_name(courseNameEditText.getText().toString());
//                newCourse.setRoom_no(roomNoEditText.getText().toString());
//                newCourse.setUser_name(facultyNameEditText.getText().toString());
//                newCourse.setAttendance_criteria((int) Float.parseFloat(attendanceEditText.getText().toString()));
//
//                selectedCourses.add(newCourse);
//                dialog.dismiss();
//            }
//        });
//
//        dialog.show();
//
//    }
//
//
//
//    private void addEditText(){
//        AutoCompleteTextView textView = new AutoCompleteTextView(getContext());
//        textView.setOnItemSelectedListener(this);
//        textView.setAdapter(autoCompleteAdapter);
//        autoCompleteViewList.add(textView);
//        textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
//        linearLayout.addView(textView);
//    }
}
