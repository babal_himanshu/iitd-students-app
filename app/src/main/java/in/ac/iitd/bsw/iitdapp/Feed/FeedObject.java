package in.ac.iitd.bsw.iitdapp.Feed;

public class FeedObject {
    private int clubIcon;
    private String postDate;
    private String clubName;
    private String clubID;
    private String postTitle;
    private String postDesc;
    private Boolean hasImage;
    private String imageUUID;
    private float imageRatio;

    public FeedObject(int clubIcon, String postDate, String clubName, String postTitle, String postDesc, String clubID, Boolean hasImage, float imageRatio) {
        this.clubIcon = clubIcon;
        this.postDate = postDate;
        this.clubName = clubName;
        this.postTitle = postTitle;
        this.postDesc = postDesc;
        this.clubID = clubID;
        this.hasImage = hasImage;
        this.imageRatio = imageRatio;
    }

    public float getImageRatio() {
        return imageRatio;
    }

    public Boolean getHasImage() {
        return hasImage;
    }

    public String getImageUUID() {
        return imageUUID;
    }

    public void setImageUUID(String imageUUID) {
        this.imageUUID = imageUUID;
    }

    public String getClubID() {
        return clubID;
    }

    public int getClubIcon() {
        return clubIcon;
    }

    public String getPostDate() {
        return postDate;
    }

    public String getClubName() {
        return clubName;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public String getPostDesc() {
        return postDesc;
    }
}
