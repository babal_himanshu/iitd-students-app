package in.ac.iitd.bsw.iitdapp.StudentResources;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import in.ac.iitd.bsw.iitdapp.Dashboard.DashBoardActivity;
import in.ac.iitd.bsw.iitdapp.Dashboard.DrawerActivity;
import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.StudentResources.Directory.DirectoryFragment;
import in.ac.iitd.bsw.iitdapp.StudentResources.Links.LinksFragment;
import in.ac.iitd.bsw.iitdapp.StudentResources.PapersAndStudentDiary.ExamPapersFragment;
import in.ac.iitd.bsw.iitdapp.StudentResources.PapersAndStudentDiary.QuestionBankFragment;

public class ResourcesActivity extends DrawerActivity {
    private static final String TAG = "@@-resource-activity";
    public RecyclerView resourcesRecyclerView;
    public RequestQueue requestQueue;
    public RecyclerView.LayoutManager layoutManager;
    public LinearLayout linearLayout;
    private FragmentManager fragmentManager;
    //private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.drawer_frame_container);
        View contentView = inflater.inflate(R.layout.activity_resources, frameLayout, false);
        drawerLayout.addView(contentView, 0);

        toolbar.setVisibility(View.GONE);
        toolbar = (Toolbar) findViewById (R.id.toolbar_resources);
        toolbar.setTitle("Resources");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar (toolbar);
        getSupportActionBar().setTitle("Resources");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        result.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);

        requestQueue = Volley.newRequestQueue(this);
        linearLayout = (LinearLayout) findViewById(R.id.activity_resource_linearLayout_viewGroup);
        resourcesRecyclerView = (RecyclerView) findViewById(R.id.activity_resources_recyclerView);
        assert resourcesRecyclerView != null;
        resourcesRecyclerView.hasFixedSize();
        layoutManager = new LinearLayoutManager(this);

        fragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            Bundle extra = getIntent().getExtras();
            if (extra != null && extra.containsKey("notification")){
                startFragmentFromNotification(extra);
            }
            else {
                Bundle bundle = new Bundle();
                bundle.putString("diary", null);
                ResourcesFragment resourcesFragment = new ResourcesFragment();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                resourcesFragment.setArguments(bundle);
                transaction.replace(R.id.activity_resource_frame, resourcesFragment);
                transaction.commit();
            }
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (this.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }


    }

    public boolean isCallPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (this.checkSelfPermission(Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 2);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras.containsKey("notification")) {
            startFragmentFromNotification(extras);
        }
    }

    private void startFragmentFromNotification(Bundle extras){
        if (extras != null) {
            if (extras.containsKey("notification")) {
                if (extras.getString("notification").equals("download-complete")) {
                    String FREGMENT_ID = extras.getString("fragment_id");
                    if (FREGMENT_ID.equals("diary")){
                        Bundle bundle = new Bundle();
                        bundle.putString("diary", FREGMENT_ID);
                        ResourcesFragment resourcesFragment = new ResourcesFragment();
                        resourcesFragment.setArguments(bundle);
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.activity_resource_frame, resourcesFragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commitAllowingStateLoss();
                    }
                    else {
                        Bundle bundle = new Bundle();
                        bundle.putString("subjectName", FREGMENT_ID);
                        bundle.putString("paperName", extras.getString("file_dir"));
                        ExamPapersFragment examPapersFragment = new ExamPapersFragment();
                        examPapersFragment.setArguments(bundle);
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.activity_resource_frame, examPapersFragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commitAllowingStateLoss();
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        Log.i("debug", "a");

        Fragment fragment = this.getSupportFragmentManager().findFragmentById(R.id.activity_resource_frame);

        if (fragment instanceof ExamPapersFragment) {
            Log.i("debug", "b");
            startFragment(new QuestionBankFragment());
        }
        else if (fragment instanceof QuestionBankFragment) {
            Log.i("debug", "c");
            startFragment(new ResourcesFragment());
        }
        else if (fragment instanceof LinksFragment) {
            Log.i("debug", "d");
            startFragment(new ResourcesFragment());
        }
        else if (fragment instanceof DirectoryFragment) {
            Log.i("debug", "e");
            startFragment(new ResourcesFragment());
        }
        // TODO: 7/6/16 -> modify this part
        else if (fragment instanceof ResourcesFragment) {
            Log.i("debug", "f");
            //startFragment(new ResourcesFragment());
            Intent intent = new Intent(ResourcesActivity.this, DashBoardActivity.class);
            startActivity(intent);
        }
        else {
            Log.i("debug", "g");
            super.onBackPressed();
        }
    }

    private void startFragment (Fragment fragment) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.activity_resource_frame, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}
