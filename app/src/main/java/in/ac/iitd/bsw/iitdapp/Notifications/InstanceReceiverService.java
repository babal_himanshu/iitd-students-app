package in.ac.iitd.bsw.iitdapp.Notifications;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import in.ac.iitd.bsw.iitdapp.BaseActivity;
import in.ac.iitd.bsw.iitdapp.Startup.SignIn.LoginFragment;

public class InstanceReceiverService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";
    private Context applicationContext = BaseActivity.getContextOfApplication();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        try {
            SharedPreferences preferences = applicationContext.getSharedPreferences(LoginFragment.LOGIN_SHARED_PREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.remove(LoginFragment.FIREBASE_TOKEN);
            editor.putString(LoginFragment.FIREBASE_TOKEN, refreshedToken);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
    }
}
