package in.ac.iitd.bsw.iitdapp.Profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.SignInButton;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.ac.iitd.bsw.iitdapp.FirstTimeSetup.FirstSetupActivity;
import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.Startup.SignIn.LoginFragment;
import in.ac.iitd.bsw.iitdapp.Utility.GeneralUtility;

public class LinkAccounts extends Fragment implements View.OnClickListener{
    @BindView(R.id.kerebros_login_userName_editText)
    EditText userNameEditText;
    @BindView(R.id.kerebros_login_password_editText)
    EditText passEditText;
    @BindView(R.id.kerebros_login_submitButton)
    Button submitButton;
    @BindView(R.id.kerebros_login_google_signIn_button)
    SignInButton googleSignInButton;
    @BindView(R.id.login_useAsGuest_textView)
    TextView useAsGuestTextView;

    private static final String URL_LOGIN = "https://hostelcomp.iitd.ac.in/authe.php?";
    private static final String URL_LINK = "https://hostelcomp.iitd.ac.in/link.php?";
    private static final String URL_ADD_KERBEROS_USER = "https://hostelcomp.iitd.ac.in/adduser.php?";
    private static final String TAG = "LinkAccounts";

    private RequestQueue requestQueue;
    private SharedPreferences preferences;
    private String refreshedToken;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.kerebros_login, container, false);
        ButterKnife.bind(this, view);
        submitButton.setOnClickListener(this);
        googleSignInButton.setVisibility(View.GONE);
        useAsGuestTextView.setVisibility(View.GONE);
        requestQueue = Volley.newRequestQueue(getContext());
        preferences = getActivity().getApplicationContext().getSharedPreferences(LoginFragment.LOGIN_SHARED_PREF, Context.MODE_PRIVATE);

        SharedPreferences.Editor sharedEditor = preferences.edit();
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sharedEditor.putString(LoginFragment.FIREBASE_TOKEN, refreshedToken);
        sharedEditor.apply();

        return view;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.kerebros_login_submitButton) {
            authUser(userNameEditText.getText().toString(), passEditText.getText().toString());
        }
    }

    private void authUser(final String userID, String password) {
        String url = URL_LOGIN + "uid=" + userID + "&pwd=" + password;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    parseJSON(response, userID);
                    Log.i(TAG, response);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        requestQueue.add(stringRequest);
    }

    private void parseJSON(String response, String userID) throws JSONException, UnsupportedEncodingException {
        SharedPreferences.Editor sharedEditor = preferences.edit();
        JSONObject object = new JSONObject(response);
        int success = object.getInt("success");
        Boolean isAuth = false;
        if (success > 0) {
            isAuth = true;
            sharedEditor.putBoolean(LoginFragment.IS_KERBEROS_LOGIN, true);
        }

        if (isAuth) {
            sharedEditor.putString(LoginFragment.KERBEROS_USERID, userID);
            sharedEditor.putString(LoginFragment.KERBEROS_USERNAME, object.getString("name"));
            sharedEditor.putString(LoginFragment.KERBEROS_DEPARTMENT, object.getString("dep"));
            sharedEditor.putString(LoginFragment.KERBEROS_CATEGORY, object.getString("category"));
            sharedEditor.putString(LoginFragment.KERBEROS_ENTRYNO, object.getString("entry"));
            sharedEditor.apply();

            Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
            addKerberosUser(object.getString("name"), userID, object.getString("entry"));

        } else {
            Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
        }
    }

    private void firstTimeSetup(String provider){
        Intent intent = new Intent(getActivity(), FirstSetupActivity.class);
        intent.putExtra("provider", provider);
        startActivity(intent);
    }

    private void addKerberosUser(String name,String ker_uid, String entryNo) throws UnsupportedEncodingException {
        Log.i(TAG, name);
        Log.i(TAG, ker_uid);
        Log.i(TAG, entryNo);
        Log.i(TAG, refreshedToken);
        String url = encodeURL(URL_ADD_KERBEROS_USER + "Name=" + name + "&Kerebros_uid=" + ker_uid + "&Entry_No=" + entryNo + "&FireBaseToken_id=" + refreshedToken) ;
        Log.i(TAG, url);
        StringRequest stringRequest = GeneralUtility.sendSimpleGetRequest(url);
        requestQueue.add(stringRequest);

        linkAccount(ker_uid);
    }

    private void linkAccount(String ker_uid) throws UnsupportedEncodingException {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("Google_uid", preferences.getString(LoginFragment.GOOGLE_ID, ""));
        hashMap.put("Kerebros_uid", ker_uid);
        hashMap.put("FireBaseToken_id", refreshedToken);

        StringRequest stringRequest = GeneralUtility.sendSimplePostRequest(URL_LINK, hashMap);
        requestQueue.add(stringRequest);

        firstTimeSetup("kerberos");
    }

    private String encodeURL(String url) {
        return url.replace(" ", "%20");
    }
}
