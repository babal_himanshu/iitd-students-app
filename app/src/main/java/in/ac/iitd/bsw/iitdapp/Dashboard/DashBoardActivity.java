package in.ac.iitd.bsw.iitdapp.Dashboard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.roughike.bottombar.OnMenuTabClickListener;

import in.ac.iitd.bsw.iitdapp.AttendanceManager.AddSubjectFragment;
import in.ac.iitd.bsw.iitdapp.Feed.Feed;
import in.ac.iitd.bsw.iitdapp.Profile.Profile;
import in.ac.iitd.bsw.iitdapp.R;

public class DashBoardActivity extends DrawerWithBottomBar {
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.drawer_bottom_bar_fragment_container);
        View contentView = inflater.inflate(R.layout.activity_dashboard, frameLayout, false);
        linearLayoutContainer.addView(contentView, 0);

        bottomBar.setOnMenuTabClickListener(new OnMenuTabClickListener() {
            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.bottomBar_feed) {
                    startFragment(new Feed(), "feed");

                } else if (menuItemId == R.id.bottomBar_profile) {
                    startFragment(new Profile(), "profile");

                } else if (menuItemId == R.id.bottomBar_attendance) {
//                    Intent intent = new Intent(DashBoardActivity.this, AttendanceActivity.class);
//                    startActivity(intent);
                    //startFragment(new AttendanceAddDates(), "attendance");
                    startFragment(new AddSubjectFragment(), "attendance");

                }

            }

            @Override
            public void onMenuTabReSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.bottomBar_feed) {
                    startFragment(new Feed(), "feed");
                }
            }

        });

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("Dashboard", "Refreshed token: " + refreshedToken);

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        bottomBar.onSaveInstanceState(outState);
    }

    private void startFragment (Fragment fragment, String tag) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.dashboard_fragment_container, fragment, tag);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Log.i("debug", "a");

        Fragment fragment = this.getSupportFragmentManager().findFragmentById(R.id.dashboard_fragment_container);

        if (fragment instanceof Feed || fragment instanceof Profile || fragment instanceof AddSubjectFragment) {
            Log.i("debug", "b");
            //startFragment(new QuestionBankFragment());
            if (doubleBackToExitPressedOnce) {
                finishApp();
            }
            else {
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Press the back button once again to quit", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        doubleBackToExitPressedOnce = false;
                    }
                }, 2500);
            }
        } else {
            Log.i("debug", "g");
            super.onBackPressed();
        }
    }

    private void finishApp(){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
