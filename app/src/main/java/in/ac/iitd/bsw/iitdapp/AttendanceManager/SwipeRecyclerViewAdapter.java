package in.ac.iitd.bsw.iitdapp.AttendanceManager;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.ac.iitd.bsw.iitdapp.AttendanceManager.TimeTable.CourseTimeTableObject;
import in.ac.iitd.bsw.iitdapp.R;

public class SwipeRecyclerViewAdapter extends RecyclerSwipeAdapter<SwipeRecyclerViewAdapter.SwipeViewHolder> {
    private Context context;
    private List<CourseTimeTableObject> list;
    ClickListener clickListener;

    public class SwipeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.swipe)
        SwipeLayout swipeLayout;
        @BindView(R.id.swipe_course_code_editText)
        TextView courseCodeTextView;
        @BindView(R.id.swipe_course_name_editText)
        TextView courseNameTextView;
        @BindView(R.id.swipe_room_no_editText)
        TextView roomNoTextView;
        @BindView(R.id.swipe_attendance_criteria_editText)
        TextView attendanceCriteriaTextView;
        @BindView(R.id.swipe_faculty_name_editText)
        TextView facultyTextView;
        @BindView(R.id.swipe_time_textView)
        TextView timeTextView;
        @BindView(R.id.swipe_days_textView)
        TextView daysTextView;
        @BindView(R.id.swipe_edit)
        ImageView swipeEditImage;
        @BindView(R.id.swipe_delete)
        ImageView swipeDeleteImage;
//        @BindView(R.id.swipe_extra_button)
//        ImageButton swipeExtraButton;
//        @BindView(R.id.swipe_extra_button2)
//        ImageButton swipeExtraButton2;
//        @BindView(R.id.swipe_extra_button3)
//        ImageButton swipeExtraButton3;


        public SwipeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            swipeEditImage.setOnClickListener(this);
            swipeDeleteImage.setOnClickListener(this);
//            swipeExtraButton.setOnClickListener(this);
//            swipeExtraButton2.setOnClickListener(this);
//            swipeExtraButton3.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.onItemClick(getAdapterPosition(), v);
            }
//            if (v.getId() == R.id.swipe_edit) {
//                Toast.makeText(v.getContext(), "Clicked on EDIT", Toast.LENGTH_SHORT).show();
//
//            } else if (v.getId() == R.id.swipe_delete) {
//                Toast.makeText(v.getContext(), "Clicked on DELETE", Toast.LENGTH_SHORT).show();
//
//            } else if (v.getId() == R.id.swipe_extra_button) {
//                Toast.makeText(v.getContext(), "Clicked on EXTRA", Toast.LENGTH_SHORT).show();
//
//            } else if (v.getId() == R.id.swipe_extra_button2) {
//                Toast.makeText(v.getContext(), "Clicked on EXTRA - 1", Toast.LENGTH_SHORT).show();
//
//            } else if (v.getId() == R.id.swipe_extra_button3) {
//                Toast.makeText(v.getContext(), "Clicked on EXTRA - 2", Toast.LENGTH_SHORT).show();
//
//            } else if (v.getId() == R.id.swipe) {
//                Toast.makeText(v.getContext(), "CLICKED", Toast.LENGTH_SHORT).show();
//
//            } else {
//                Toast.makeText(v.getContext(), "Clicked", Toast.LENGTH_SHORT).show();
//            }
        }
    }

    public SwipeRecyclerViewAdapter (Context context, List<CourseTimeTableObject> list) {
        this.context = context;
        this.list = list;
    }

    public interface ClickListener {
        void onItemClick (int position, View v);
    }

    public void setClickListener (ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public SwipeRecyclerViewAdapter.SwipeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.attendance_adapter_row, parent, false);
        return new SwipeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SwipeRecyclerViewAdapter.SwipeViewHolder viewHolder, int position) {
        mItemManger.bindView(viewHolder.itemView, position);


        CourseTimeTableObject object = list.get(position);
        viewHolder.courseCodeTextView.setText(object.getCourse_no());
        viewHolder.courseNameTextView.setText(object.getCourse_name());
        viewHolder.roomNoTextView.setText(object.getRoom_no());
        viewHolder.daysTextView.setText(object.getDay());
        viewHolder.facultyTextView.setText(object.getUser_name());
        String att;
        if (object.getAttendance_criteria() != 0){
            att = String.valueOf(object.getAttendance_criteria());
        } else {
            att = "Not Filled";
        }
        viewHolder.attendanceCriteriaTextView.setText(att);
        viewHolder.timeTextView.setText(object.getStart_time());

        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        viewHolder.swipeLayout.close();

        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeLayout.findViewById(R.id.swipe_right_to_left));

        viewHolder.swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Clicked on " + viewHolder.courseNameTextView.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }
}
