package in.ac.iitd.bsw.iitdapp.StudentResources.PapersAndStudentDiary;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import in.ac.iitd.bsw.iitdapp.R;
import in.ac.iitd.bsw.iitdapp.StudentResources.ResourcesActivity;

import static com.facebook.FacebookSdk.getApplicationContext;

/*
This class was created to Download  .pdf files from BSW servers
A notification is shown while file is being downloaded
 */
public class DownloadPDFFromURL {
    private static final int MEGABYTE = 1024 * 1024;
    private final int ID = 1;
    String url;
    String TAG = "Download";
    Context context;
    String fileName;
    String filePath;
    Intent intent;
    PendingIntent pendingIntent;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder builder;
    private String fileDir;
    private View snackView;
    private String FRAGMENT_ID;


    // TODO: 6/25/16 -> Replace toast with snackbar and add View() on its(Snackbar's)  on click listener.

    public DownloadPDFFromURL(Context c, String url, String nameOfFile, String filePath, View snackView, String FRAGMENT_ID) {
        this.context = c;
        this.url = url;
        this.fileName = nameOfFile;
        this.filePath = filePath;
        this.snackView = snackView;
        this.FRAGMENT_ID = FRAGMENT_ID;
        this.fileDir = "/BSW-IITD/" + filePath + fileName;
    }

    public void download() {

        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        builder = new NotificationCompat.Builder(context);

        builder.setContentTitle("Downloading")
                .setContentText("Download in Process")
                .setSmallIcon(R.drawable.ic_file_download_black_24dp)
                .setOngoing(true);

        //todo -> change it from MainActivity.class to appropriate
        intent = new Intent(context, ResourcesActivity.class);
        intent.putExtra("file_dir", fileDir);
        intent.putExtra("fragment_id", FRAGMENT_ID);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        new DownloadFile().execute(url, fileName, filePath);
    }

    public void view(String dir) {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + dir);
        Uri path = Uri.fromFile(pdfFile);
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        try {
            context.startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(), "No Application available to view PDF", Toast.LENGTH_SHORT).show();
        }
    }

    private void showSnackBar(String message) {
        Snackbar.make(snackView, message, Snackbar.LENGTH_SHORT).show();
//        Snackbar.with(context)
//                .text(message)
//                .show(ResourcesActivity.class);
//        SnackbarManager.show(
//                Snackbar.with(getApplicationContext())
//                        .text(message), ResourcesActivity);
    }

    private class DownloadFile extends AsyncTask<String, Integer, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            intent.putExtra("notification", "download-toBeExecuted");
            pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setProgress(100, 0, false)
                    .setContentIntent(pendingIntent);
            notificationManager.notify(ID, builder.build());
        }

        @Override
        protected Integer doInBackground(String... strings) {
            String fileUrl = strings[0];
            String fileName = strings[1];
            String path = strings[2];

            Log.i(TAG, fileName);
            Log.i(TAG, fileUrl);

            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "/BSW-IITD/" + path);
            folder.mkdirs();

            File pdfFile = new File(folder, fileName);

            if (pdfFile.exists()) {

                int check_point = -1;
                publishProgress(check_point);
                return check_point;
//                context.runOnUiThread(new Runnable() {
//                    public void run() {
//
//                        Toast.makeText(context, "File Already Downloaded", Toast.LENGTH_LONG).show();
//                    }
//                });
            } else {

                try {
                    pdfFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {

                    URL url = new URL(fileUrl);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.connect();

                    InputStream inputStream = urlConnection.getInputStream();
                    FileOutputStream fileOutputStream = new FileOutputStream(pdfFile);
                    int totalSize = urlConnection.getContentLength();

                    Log.i(TAG, String.valueOf(totalSize));

                    byte[] buffer = new byte[MEGABYTE];
                    int bufferLength = 0;
                    long total = 0;
                    while ((bufferLength = inputStream.read(buffer)) > 0) {
                        total += bufferLength;
                        publishProgress((int) (total * 100) / totalSize);
                        Log.i(TAG, String.valueOf((int) (total * 100) / totalSize));

                        fileOutputStream.write(buffer, 0, bufferLength);
                    }

                    fileOutputStream.close();
                    Log.i("download", "download-complete");
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i("Download", "cancel");
                    cancel(true);
                }
                return null;
            }


        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            if (values[0] == -1) {
                Toast.makeText(context, "File Already Downloaded", Toast.LENGTH_LONG).show();
                showSnackBar("File Already Downloaded");

                view(fileDir);
            } else {
                Toast.makeText(context, "Downloading...", Toast.LENGTH_SHORT).show();
                showSnackBar("Downloading...");

                intent.putExtra("notification", "download-inProgress");
                pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                builder.setProgress(100, values[0], false)
                        .setContentIntent(pendingIntent);
                notificationManager.notify(ID, builder.build());
            }
        }

        @Override
        protected void onCancelled(Integer aVoid) {
            super.onCancelled(aVoid);

            Toast.makeText(context, "Download Cancelled", Toast.LENGTH_LONG).show();
            showSnackBar("Download Cancelled");

            builder.setProgress(0, 0, false);

            intent.putExtra("notification", "download-cancelled");
            pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentTitle("Download Cancelled")
                    .setContentText("Download Cancelled due to an error")
                    .setSmallIcon(R.drawable.ic_error_outline_black_24dp)
                    .setContentIntent(pendingIntent)
                    .setOngoing(false);
            notificationManager.notify(ID, builder.build());
        }

        @Override
        protected void onPostExecute(Integer aVoid) {
            super.onPostExecute(aVoid);

            // Removes the progress bar
            builder.setProgress(0, 0, false);

            Toast.makeText(context, "Downloading Complete", Toast.LENGTH_SHORT).show();
            showSnackBar("Downloading Complete");

            intent.putExtra("notification", "download-complete");
            pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentTitle("Downloaded")
                    .setContentText("Download complete")
                    .setSmallIcon(R.drawable.ic_done_black_24dp)
                    .setOngoing(false)
                    .setContentIntent(pendingIntent);

            notificationManager.notify(ID, builder.build());

            view(fileDir);
        }
    }
}
