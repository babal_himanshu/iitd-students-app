//package in.ac.iitd.bsw.iitdapp.StudentResources;
//
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.ActivityCompat;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.DefaultItemAnimator;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.View;
//
//import com.android.volley.RequestQueue;
//import com.android.volley.toolbox.Volley;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import in.ac.iitd.bsw.iitdapp.R;
//import in.ac.iitd.bsw.iitdapp.StudentResources.Directory.Directory;
//import in.ac.iitd.bsw.iitdapp.StudentResources.Links.Links;
//import in.ac.iitd.bsw.iitdapp.StudentResources.PapersAndStudentDiary.DownloadPDFFromURL;
//import in.ac.iitd.bsw.iitdapp.StudentResources.PapersAndStudentDiary.QuestionBank;
//
//public class Resources extends AppCompatActivity {
//    private static final String URL = "http://bsw.iitd.ac.in/stuff/diary.pdf";
//    private static final String TAG = "response";
//    private static final String LINKS = "Important Links";
//    private static final String DIRECTORY = "Telephone Directory";
//    private static final String DIARY = "Student Diary";
//    private static final String PAPERS = "Question Papers";
//    private RecyclerView resourcesListRecyclerView;
//    private ResourcesAdapter resourcesAdapter;
//    private List<ResourcesObject> resourcesObjectList = new ArrayList<>();
//    private RequestQueue requestQueue;
//    private DownloadPDFFromURL download;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.resources_list);
//
//        requestQueue = Volley.newRequestQueue(this);
//        resourcesListRecyclerView = (RecyclerView) findViewById(R.id.resources_list_recyclerView);
//        resourcesAdapter = new ResourcesAdapter(resourcesObjectList, this);
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
//        resourcesListRecyclerView.setLayoutManager(layoutManager);
//        resourcesListRecyclerView.setAdapter(resourcesAdapter);
//        resourcesListRecyclerView.setItemAnimator(new DefaultItemAnimator());
//
//        prepareResourcesData();
//
//        resourcesAdapter.setOnItemClickListener(new ResourcesAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                if (position == 0) { //LINKS
//                    Intent intent = new Intent(Resources.this, Links.class);
//                    startActivity(intent);
//                } else if (position == 1) { //DIRECTORY
//                    Intent intent = new Intent(Resources.this, Directory.class);
//                    startActivity(intent);
//                } else if (position == 2) { //DIARY
//                    download = new DownloadPDFFromURL(Resources.this, URL, "Student_Diary.pdf",
//                            "", resourcesListRecyclerView);
//                    download.download();
//                } else if (position == 3) { //PAPERS
//                    Intent intent = new Intent(Resources.this, QuestionBank.class);
//                    startActivity(intent);
//                } else {
//                    //Nothing
//                }
//            }
//        });
//    }
//
//    private void prepareResourcesData() {
//        resourcesObjectList.add(new ResourcesObject(LINKS));
//        resourcesObjectList.add(new ResourcesObject(DIRECTORY));
//        resourcesObjectList.add(new ResourcesObject(DIARY));
//        resourcesObjectList.add(new ResourcesObject(PAPERS));
//
//        resourcesAdapter.notifyDataSetChanged();
//    }
//
//    public boolean isStoragePermissionGranted() {
//        if (Build.VERSION.SDK_INT >= 23) {
//            if (this.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    == PackageManager.PERMISSION_GRANTED) {
//                Log.v(TAG, "Permission is granted");
//                return true;
//            } else {
//
//                Log.v(TAG, "Permission is revoked");
//                //ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//                return false;
//            }
//        } else { //permission is automatically granted on sdk<23 upon installation
//            Log.v(TAG, "Permission is granted");
//            return true;
//        }
//
//
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
//            //resume tasks needing this permission
//        }
//    }
//
//    @Override
//    protected void onNewIntent(Intent intent) {
//        Bundle extras = intent.getExtras();
//        if (extras != null) {
//            if (extras.containsKey("test")) {
//                Log.i(TAG, extras.getString("test"));
//
//                if (extras.getString("test").equals("download-complete")) {
//
//                    download.view(extras.getString("file_dir"));
//                }
//            }
//        }
//    }
//}
